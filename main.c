/**
 * Copyright (c) 2015 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *		list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *		Semiconductor ASA integrated circuit in a product or a software update for
 *		such product, must reproduce the above copyright notice, this list of
 *		conditions and the following disclaimer in the documentation and/or other
 *		materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *		contributors may be used to endorse or promote products derived from this
 *		software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *		Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *		engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
/** @file
* @brief Example template project.
* @defgroup nrf_templates_example Example Template
*
*/


#include <stdio.h>
#include "nrf.h"
//#include "bsp.h"
#include "hardfault.h"
#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ant.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "ant_key_manager.h"
//#include "ant_state_indicator.h"

#include "nrf_delay.h"

#include "bsp.h"
#include "velo_driver.h"
#include "velo_us_timer.h"

#include "velo_config.h"

#include "nrf_gpio.h"

#define SCHED_MAX_EVENT_DATA_SIZE   256
#define SCHED_QUEUE_SIZE            10

//#include "nrf_drv_clock.h"
/**
 * @brief Function for setup all thinks not directly associated with ANT stack/protocol.
 * @desc Initialization of: @n
 *				 - app_tarce for debug.
 *				 - app_timer, pre-setup for bsp.
 *				 - bsp for signaling LEDs and user buttons.
 */
static void utils_setup(void)
{
  // Start microsecond timer:
  us_timer_config();
  ret_code_t err_code = NRF_LOG_INIT(time_us);
  APP_ERROR_CHECK(err_code);

  // Initialize and start a single continuous mode timer, which is used to update the event time
  // on the main data page.
  err_code = app_timer_init();
  APP_ERROR_CHECK(err_code);

  err_code = nrf_pwr_mgmt_init();
  APP_ERROR_CHECK(err_code);

  NRF_LOG_DEFAULT_BACKENDS_INIT();
}


/**
 * @brief Function for ANT stack initialization.
 *
 * @details Initializes the SoftDevice and the ANT event interrupt.
 */
static void softdevice_setup(void)
{
  ret_code_t err_code = nrf_sdh_enable_request();
  APP_ERROR_CHECK(err_code);

  ASSERT(nrf_sdh_is_enabled());

  err_code = nrf_sdh_ant_enable();
  APP_ERROR_CHECK(err_code);

  err_code = ant_public_key_set(ANT_PUBLIC_NETWORK_NUM);
  APP_ERROR_CHECK(err_code);
		
}


/* --------------------------- Setup functions end----------------------------------------------*/
/**
 * @brief Function for application main entry.
 */

int main(void)

{
  // Setup scheduler - this allows us to run things in main context, rather than interrupt context.
  APP_SCHED_INIT(SCHED_MAX_EVENT_DATA_SIZE, SCHED_QUEUE_SIZE);
  //Setup
  utils_setup();
  NRF_LOG_INFO("Booting up.");
  NRF_LOG_FLUSH();
  softdevice_setup();

  velo_init();  


  while (true)
  {
    //NRF_LOG_INFO("About to sched execute")
    app_sched_execute();
    NRF_LOG_FLUSH();
    nrf_pwr_mgmt_run();
  }
}
/** @} */

