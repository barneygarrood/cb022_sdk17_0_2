#ifndef VELO_ERROR_H__
#define VELO_ERROR_H__
#include "velo_status.h"
void typedef (* velo_error_cb_t) (velo_error_status_t error_status);
void velo_error_init(velo_error_cb_t callback);
void app_error_fault_handler(uint32_t id, uint32_t pc, uint32_t info);
void velo_error_check_fpu();

#endif //VELO_ERROR_H__