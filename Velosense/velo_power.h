
#ifndef VELO_BUTTON_H__
#define VELO_BUTTON_H__


#define BUTTON_DETECTION_DELAY          APP_TIMER_TICKS(50) 
#define BUTTON_OFF_MS			2000
#define BOOTLOADER_TAP_TIME_MS          10000
#define BOOTLOADER_TAP_COUNT            7

#define BOOTLOADER_DFU_START 0xB1


void typedef (* velo_power_callback_t) (void);
void velo_power_check_update_status(void);
void velo_power_enter_dfu();
void velo_power_shutdown_all(void);
void velo_power_sleep(void);
void velo_power_up();
void velo_power_init(velo_power_callback_t callback_function);
#endif //VELO_BUTTON_H__



