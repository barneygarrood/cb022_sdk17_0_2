#ifndef VELO_STATUS_H__
#define VELO_STATUS_H__

#include <stdint.h>

typedef enum
{
  VELO_PENDING_ACTION_NONE = 0,
  VELO_PENDING_ACTION_UPDATE_LED = 1,
  VELO_PENDING_ACTION_CHECK_CHARGE_STATUS = 2,
} velo_pending_action_enum_t;

typedef enum 
{
  VELO_PWR_STATE_NONE = 0,
  VELO_PWR_STATE_OFF = 1,
  //VELO_PWR_STATE_SLEEP = 2,
  VELO_PWR_STATE_STANDBY = 4,
  VELO_PWR_STATE_ON = 8,
  VELO_PWR_STATE_DFU = 16, 
} velo_pwr_state_enum_t;


typedef enum
{
  VELO_CHARGE_STATE_NONE = 0,
  VELO_CHARGE_STATE_LOW = 2,
  VELO_CHARGE_STATE_CHARGING = 4,
  VELO_CHARGE_STATE_CHARGED = 8,
} velo_charge_state_enum_t;

typedef enum
{
  VELO_BUTTON_STATE_NONE = 0,
  VELO_BUTTON_STATE_BUTHOLD = 1,
  VELO_BUTTON_STATE_OFF = 2,
} velo_button_state_enum_t;

typedef struct
{
  //uint16_t line_no; // nrf_error has 32 bit, but assume no more than 65k lines in a file!
  //uint32_t err_code;
  uint32_t stack_ptr;
  uint16_t err_code;
  uint8_t ant_context :1;
  uint8_t timer_context :1;
  uint8_t calculating :1;
  uint8_t error :1;
  uint8_t sd_assert :1;
  uint8_t sd_mem_access :1;
  uint8_t sdk_assert :1;
  uint8_t sdk_error :1;
} velo_error_status_t;

// I reckon bitfields make for easiest reading code.
// Tried before with enums, but then can't neatly do switch statements.
typedef struct 
{
  velo_pwr_state_enum_t current_state;
  velo_pwr_state_enum_t new_state;
  velo_charge_state_enum_t charge_state;
  
  velo_pending_action_enum_t pending_actions;

  velo_button_state_enum_t button_state;

  velo_error_status_t error_status;

  uint32_t last_ant_msg_tx_time;

  uint8_t shutdown_warning : 1;
  uint8_t enable_gpio_events :1;
  
  uint8_t pwm_init :1;
  uint8_t twi_init :1;
  uint8_t lidar_init :1;
  uint8_t bat_init :1;
  uint8_t accel_init :1;

  uint8_t ant_zenith_init :1;
  uint8_t gpios_enable : 1;
  uint8_t sampling_timer_init : 1;
  uint8_t lidar_disconnected :1;

  uint8_t inactive_warning :1;
  uint8_t inactive :1;
  uint8_t dirty :1;
}velo_status_t;

extern velo_status_t velo_status;
void velo_status_init();

#endif //VELO_STATUS_H__