#include "velo_led_pwm.h"
#include "nrf_drv_pwm.h"
#include "velo_status.h"
#include "velo_config.h"
#include "nrf_gpio.h"

#define VELO_PWM_SIZE 128
static nrf_drv_pwm_t m_pwm = NRF_DRV_PWM_INSTANCE(0);		//PWM instance:
static nrf_pwm_values_individual_t seq_values[VELO_PWM_SIZE];

// Create PWM arrays to control LEDs.
// Array is 128 values long.  We can divide this up into sub-128 blocks to flash faster.
// On is number of values for which LED is on.  If flash then it ramps up/ramps down.
// Cycle time should be a divisor of 128 so can flash faster etc.
static void _flash_pattern(uint8_t on_time, uint8_t cycle_time, bool flash, uint8_t intensity, uint8_t R, uint8_t G, uint8_t B)
{
  uint8_t value;
  uint8_t ctr = 0;
  uint8_t half_time = on_time/2;
  for (int i=0;i<VELO_PWM_SIZE ;i++)
  {
    if (ctr <= on_time)
    {
      if (flash)
      {
        if (ctr <= half_time)
        {
          value = (uint8_t) (intensity * ctr/ (on_time / 2.0f));
        }
        else
        {
          value = (uint8_t) (intensity*(on_time - ctr) / (on_time / 2.0f));
        }
      }
      else
      {
        value = intensity;
      }
    }
    else
    {
      value = 0;
    }
    ctr++;
    if (ctr == cycle_time){ctr = 0;}
    seq_values[i].channel_0 = R * value / 255;
    seq_values[i].channel_1 = B * value / 255;
    seq_values[i].channel_2 = G * value / 255;
  }
}




static void _flash_pattern_buthold()
{
  // rapid flash pattern red
  _flash_pattern(8,8,true,20,0xEE,0x00,0xEE); // Violet fast flash

}

static void _flash_pattern_on()
{
  _flash_pattern(4,16,true,20,0x00,0xFF,0xFF);  // Cyan flash
}

static void _flash_pattern_on_lidar_disconnected()
{
  _flash_pattern(128,128,false,20,0xFF,0x00,0x00);  // solid red
}


static void _flash_pattern_low_bat()
{
  _flash_pattern(4,16,true,20,0xFF,0x00,0x00);  // Red flash
}

static void _flash_pattern_charging_on()
{
  _flash_pattern(128,128,false,20,0xFF,0xFF,0x00);  // solid amber
}

static void _flash_pattern_charging_stdby()
{
  _flash_pattern(16,128,true,20,0xFF,0xFF,0x00);  // slow flash amber
}

static void _flash_pattern_charged_on()
{
  _flash_pattern(128,128,false,20,0x00,0xFF,0x00);  // solid amber
}

static void _flash_pattern_charged_stdby()
{
  _flash_pattern(16,128,true,20,0x00,0xFF,0x00);  // slow flash amber
}

static void _flash_pattern_off()
{
  _flash_pattern(128,128,false,20,0,0,0); // LED off
}

//Set LED config based on what the current status is:
void velo_led_pwm_set_pattern(void)
{
  // Is standby mode?
  if (velo_status.current_state == VELO_PWR_STATE_STANDBY)
  {
    if (velo_status.charge_state & VELO_CHARGE_STATE_CHARGING)
    {
      if (velo_status.charge_state & VELO_CHARGE_STATE_CHARGED)
      {
        // Charged
        _flash_pattern_charged_stdby();
      }
      else
      {
        // Charging
        _flash_pattern_charging_stdby();
        
      }
    }
  }
  else if(velo_status.button_state & VELO_BUTTON_STATE_OFF)
  {
    _flash_pattern_off();
  }
  else if (velo_status.button_state & VELO_BUTTON_STATE_BUTHOLD)
  {
    _flash_pattern_buthold();
  }
    
  //Starter for 10: is charge plugged in:
  else if (velo_status.charge_state & VELO_CHARGE_STATE_CHARGING)
  {
    if (velo_status.charge_state & VELO_CHARGE_STATE_CHARGED)
    {
        //Charge complete
       _flash_pattern_charged_on();
    }
    else
    {
      //Charging
      _flash_pattern_charging_on();
    }
  }
  else if (velo_status.charge_state == VELO_CHARGE_STATE_LOW)
  {
    _flash_pattern_low_bat();
  }
  else
  {
    // If lidar disconnected?
    if (velo_status.lidar_disconnected)
    {
      _flash_pattern_on_lidar_disconnected();
    }
    else
    {
      _flash_pattern_on();
    }
  }
}


// Initialise LEDs
void velo_led_pwm_init(void)
{
  // To start with create pwm for led off.
  _flash_pattern_off();
  //Configure PWM:
  nrf_drv_pwm_config_t const pwm_cfg =
  {
    .output_pins =
    {
      PIN_LED1 | NRF_DRV_PWM_PIN_INVERTED,		// channel 0
      PIN_LED2 | NRF_DRV_PWM_PIN_INVERTED,          // channel 1
      PIN_LED3 | NRF_DRV_PWM_PIN_INVERTED,          // channel 2
      NRF_DRV_PWM_PIN_NOT_USED,                     // channel 3
    },
    .irq_priority = APP_IRQ_PRIORITY_LOWEST,
    .base_clock   = NRF_PWM_CLK_500kHz,
    .count_mode   = NRF_PWM_MODE_UP,
    .top_value    = 100,
    .load_mode    = NRF_PWM_LOAD_INDIVIDUAL,
    .step_mode    = NRF_PWM_STEP_AUTO
  };
  APP_ERROR_CHECK(nrf_drv_pwm_init(&m_pwm, &pwm_cfg, NULL));
  velo_status.pwm_init = true;
  nrf_pwm_sequence_t const pwm_seq0 =
  {
    .values.p_common = (nrf_pwm_values_common_t *) seq_values,
    .length         = 128*4,
    .repeats         = 200, 
    .end_delay       = 0,
  };
  (void) nrf_drv_pwm_simple_playback(&m_pwm,&pwm_seq0,1,NRF_DRV_PWM_FLAG_LOOP);
}

void velo_led_pwm_uninit(void)
{
  //Unninitialise PWM (which controls LEDs - otherwise if shutdown exactly during a flash they can stay on).
  if (velo_status.pwm_init)
  {
    nrf_drv_pwm_uninit(&m_pwm);
    velo_status.pwm_init = false;
  }
}