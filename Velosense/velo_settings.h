#ifndef VELO_SETTINGS_H__
#define VELO_SETTINGS_H__


#include <stdint.h>
#include "velo_flash.h"
#include "velo_config.h"
#include "crc16.h"

// Settings configuiration:
#define VELO_CONFIG_CRC_LOC         4
#define VELO_SETTINGS_COUNT         3
#define VELO_SETTINGS_START         8 
// Velo settings buffer has to be an integer number of bytes, so pad out to a multiple of 8.
#define VELO_SETTINGS_BUFFER_SIZE   (8 + VELO_SETTINGS_COUNT*3 + ((VELO_SETTINGS_COUNT*3) % 8 ? 8 - (VELO_SETTINGS_COUNT*3) % 8 : 0))    // Buffer size - need to make word aligned.
#define VELO_SETTING_HEADER_SIZE    4


 typedef struct{
  uint16_t range_ref_1;
  uint16_t range_ref_2;
  uint16_t range_ref_3;
 } velo_settings_t;

extern velo_settings_t velo_settings;

// Structure to contain config header:
// Note that this needs to be an entire word of 4 bytes so we can read/write.
typedef struct {

  uint8_t uninitialised :1;
  uint8_t active        :1;
  uint8_t unused        :2;
  uint8_t counter       :4;
  uint8_t unused_1[1];
  uint16_t crc; 
} velo_config_header_t;

// Controller id type:
typedef struct {
    uint32_t serial_number;
    uint8_t cid;
  } velo_controller_id_t;


uint16_t velo_get_deviceID();
uint32_t velo_get_serialNo();
void velo_settings_load();
void velo_settings_get(uint16_t * p_velo_settings);
void velo_settings_set(uint16_t * p_velo_settings);
void velo_settings_save();
uint8_t velo_settings_counter();

#endif //VELO_SETTINGS_H__