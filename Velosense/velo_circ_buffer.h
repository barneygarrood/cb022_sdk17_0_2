#ifndef VELO_CIRC_BUFFER_H__
#define VELO_CIRC_BUFFER_H__
#include <stdint.h>
#include <stdbool.h>

typedef struct {
  uint8_t * buffer;
  uint16_t object_size;
  uint16_t buffer_size;
  uint16_t size;
  uint16_t head;
  uint16_t tail;
  bool full;
}velo_circ_buffer_t;

//void _advance_pointer(velo_circ_buffer_t * p_circ_buffer);
//void _retreat_pointer(velo_circ_buffer_t * p_circ_buffer);
void velo_circ_buffer_push(velo_circ_buffer_t * p_circ_buffer, uint8_t * p_data);
bool velo_circ_buffer_pop(velo_circ_buffer_t * p_circ_buffer, uint8_t * p_data);
void velo_circ_buffer_reset(velo_circ_buffer_t * p_circ_buffer);
void velo_circ_buffer_init(velo_circ_buffer_t * p_circ_buffer, uint8_t * buffer, uint16_t object_size, uint16_t buffer_size);
void velo_circ_buffer_peek(velo_circ_buffer_t * p_circ_buffer, uint8_t * p_data,  uint16_t index);
void velo_circ_buffer_overwrite(velo_circ_buffer_t * p_circ_buffer, uint8_t * p_data,  uint16_t index);

#endif //VELO_CIRC_BUFFER_H__