#include "velo_driver.h"

#include "velo_ant_zenith.h"
#include "velo_config.h"
#include "velo_power.h"
#include "velo_us_timer.h"

#include "velo_sensors.h"
#include "velo_status.h"
#include "velo_settings.h"
#include "ant_zenith.h"

#include "nrf_pwr_mgmt.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "app_scheduler.h"
#include "app_timer.h"

#include "velo_error.h"

#include <math.h>

#include "nrf_log_ctrl.h"
#define NRF_LOG_MODULE_NAME velo_driver
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

//Timers				
APP_TIMER_DEF(m_read_timer);          // Timer for reading data from sensors.
APP_TIMER_DEF(m_reset_timer);               // Timer for resetting device in event of an error.

#define MEAS_TIME 300           // requires about 275/32768s to take measurement, so need to take account of this.

// NOTE Forard declaration of _velo_power_cb:
static void _velo_power_cb();
static uint32_t last_t;

// NOTE Callback from sampling timer - gets data from sensors.
static void _polling_callback(void * p_context)
{
  bool send_ant = false;
  // Check if need to update ant tx packet:
  uint32_t dt = velo_status.last_ant_msg_tx_time + VELO_MSG_PERIOD -NRF_RTC0->COUNTER;
  if ((dt < (POLL_PERIOD + MEAS_TIME)) & (dt > MEAS_TIME))
  {
    send_ant = true;
  }

  velo_status.error_status.timer_context = true;
  
  // Store initial state of lidar_disconnected status flag:
  uint8_t lidar_disconnected = velo_status.lidar_disconnected;
  
  velo_sensors_read();
  if (send_ant)
  {
    velo_ant_zenith_send();
  }

  // If one of the lidar sensors became disconnected then need to update LED.
  if (velo_status.lidar_disconnected!= lidar_disconnected)
  {
    velo_status.pending_actions |= VELO_PENDING_ACTION_UPDATE_LED;
  }
  velo_power_check_update_status();
  velo_status.error_status.timer_context = false;
}


static void _create_start_timers()
{
  uint8_t err_code;
  if(!velo_status.sampling_timer_init)
  {
    err_code = app_timer_create(&m_read_timer, APP_TIMER_MODE_REPEATED, _polling_callback);
    APP_ERROR_CHECK(err_code);
    app_timer_start(m_read_timer, POLL_PERIOD, NULL);
    velo_status.sampling_timer_init = true;
  }
} 

static void _stop_timers()
{
  uint32_t err_code;
  if(velo_status.sampling_timer_init)
  {
    err_code = app_timer_stop(m_read_timer);
    APP_ERROR_CHECK(err_code);
  }
}

static void _turn_off_all_peripherals(void)
{
   // Stop radios
  velo_ant_zenith_close();

  // Stop timer:
  _stop_timers();

  // Uninit sensors
  velo_sensors_uninit_all();
}

// NOTE Turn off everything.
static void _shutdown(void * p_event_data, uint16_t event_size)
{
  NRF_LOG_INFO("Going to shut everything down");
  
  _turn_off_all_peripherals();

  // Put CPU to sleep:
  velo_power_shutdown_all();
}


// NOTE Turn off radios and sensors but leave all else on.
static void _standby(void * p_event_data, uint16_t event_size)
{
   // Stop radios
  velo_ant_zenith_close();
  //Turn off range sensors:
  if (velo_status.lidar_init)
  {
    velo_sensors_uninit_lidar();
  }
  else
  {
    velo_sensors_init(false);
  }
  NRF_LOG_INFO("A");
  NRF_LOG_FLUSH();
  _create_start_timers();
  NRF_LOG_INFO("B");
  NRF_LOG_FLUSH();
}

// -----------------------------------------------------------------------------------------------------------------
// NOTE INITIALISATION
// Load saved data from FLASH memory:
static void _load_saved_info()
{
//Initialise flash storage:
  velo_flash_init();

  // Load config from flash memory:
  velo_settings_load();
}

// NOTE Turn on sensors, timers, and radios.
static void _wake(void * p_event_data, uint16_t event_size)
{
  // Initialise sensors:
  velo_sensors_init(true);
  // Start timers:
  _create_start_timers();
  // Start radios:
  velo_ant_zenith_profile_setup(velo_get_deviceID());
  // Set LED pattern and check power status.
  velo_status.pending_actions |= VELO_PENDING_ACTION_UPDATE_LED;
  velo_power_check_update_status();
}

// NOTE Turn off everything and reboot into DFU mode.
static void _enter_dfu(void * p_event_data, uint16_t event_size)
{
  _turn_off_all_peripherals();
  velo_power_enter_dfu();
}

// NOTE Function to change power mode.  All power mode changes should come through here.  Keeps things tidy.
static void _velo_power_callback(void)
{
  // Only do something if new status is different to old status:
  if (velo_status.new_state == velo_status.current_state) return;
  switch (velo_status.new_state)
  {
    case (VELO_PWR_STATE_OFF):
      // NOTE Everything off.  Lowest power mode.
      velo_status.new_state = VELO_PWR_STATE_NONE;
      velo_status.current_state = VELO_PWR_STATE_OFF;
      // Turn everything off:
      app_sched_event_put(NULL,0,_shutdown);
      break;
    case (VELO_PWR_STATE_STANDBY):
      // NOTE Radios off, sensors off.  Charging 'off' mode.
      velo_status.new_state = VELO_PWR_STATE_NONE;
      velo_status.current_state = VELO_PWR_STATE_STANDBY;
      app_sched_event_put(NULL,0,_standby);
      velo_status.pending_actions |= VELO_PENDING_ACTION_UPDATE_LED;
      break;
  
    case (VELO_PWR_STATE_ON):
      // NOTE Everything on.
      velo_status.new_state = VELO_PWR_STATE_NONE;
      velo_status.current_state = VELO_PWR_STATE_ON;
      app_sched_event_put(NULL,0,_wake);
      velo_status.pending_actions |= VELO_PENDING_ACTION_UPDATE_LED;
      break;
  
    case (VELO_PWR_STATE_DFU):
      // NOTE Everything off, enter DFU.
      velo_status.new_state = VELO_PWR_STATE_NONE;
      velo_status.current_state = VELO_PWR_STATE_DFU;
      app_sched_event_put(NULL,0, _enter_dfu);
      break;
  }
  // Set LED pattern and check power status.
  velo_power_check_update_status();
}

void _reset(void * p_context)
{
  sd_nvic_SystemReset();
}

void _schedule_reset(void * p_event_data, uint16_t event_size)
{
  uint8_t err_code;
  err_code = app_timer_create(&m_reset_timer, APP_TIMER_MODE_SINGLE_SHOT, _reset);
  APP_ERROR_CHECK(err_code);
  app_timer_start(m_reset_timer, 32768, NULL); // Fire after 1 second.
  
}

// NOTE Error handler callback:
void _error_handler_cb(velo_error_status_t error_status)
{
  // Set page 254 with error info:
  //velo_ant_zenith_set_error(error_status);
  app_sched_event_put(NULL,0,_schedule_reset);
}

// NOTE Master initialisation function
void velo_init(void)
{

  // Initialise flags:
  velo_status_init();
  velo_error_init(_error_handler_cb);
  velo_status.enable_gpio_events = false;

  _load_saved_info();

  // Power up device - this keeps lidar off, but switches on regulator so twi, leds work.
  velo_power_up();
  // Initialise twi:
  velo_sensors_init_twi();
  // Initialise battery:
  velo_sensors_init_bat();
  // Init power - this decides what to do based on start-up condition, e.g. do we need to go to standby mode, or turn off compeltely.
  velo_power_init(_velo_power_callback);
  velo_status.enable_gpio_events = true;
  
}




