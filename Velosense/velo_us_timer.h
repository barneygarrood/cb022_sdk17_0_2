#ifndef VELO_US_TIMER_H__
#define VELO_US_TIMER_H__

#include "nrf_drv_timer.h"


void us_timer_config(void);
/**@brief Returns microsecond clock count. */
uint32_t time_us(void);
uint32_t time_ms(void);





#endif //VELO_US_TIMER_H__
