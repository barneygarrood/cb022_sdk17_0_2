#include "velo_settings.h"
#include "velo_config.h"
#include "velo_status.h"
#include "nrf_delay.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"



// ############################ Device ID function: ############################ 

// Serial number parameters.  Only used in velo_driver.

// NOTE return first 16 bits of device serial number:
uint16_t velo_get_deviceID()
{
	return (NRF_FICR->DEVICEADDR[0] & 0xFFFF);
}

// NOTE Return first 32 bits of device serial number
uint32_t velo_get_serialNo() 
{
	return NRF_FICR->DEVICEADDR[0];
}

//uint16_t velo_settings[VELO_SETTINGS_COUNT] = {255};

// Static 1D array containing all 64 bytes of settings:
// Note these have to be word aligned for flash write to work:
static uint8_t s_velo_config[VELO_FLASH_BLOCK_SIZE] __ALIGN(4);
static uint8_t s_header[sizeof(velo_config_header_t)] __ALIGN(4);
static uint8_t s_active_block;
// Since data write operation is scheduled we need to keep distinct data objects for writing.
static velo_flash_data_t header_data_to_write;
static velo_flash_data_t block_data_to_write;

// Object to store settings in engineering units in.  Makes a few things clearer and faster than converting from raw
// 2-byte values in realtime.

velo_settings_t velo_settings;

static void _settings_convert()
{
  uint16_t settings[VELO_SETTINGS_COUNT*2];
  // cast from byte array to array of 16 bit unsigned integers:
  memcpy(&settings[0],&s_velo_config[VELO_SETTINGS_START],VELO_SETTINGS_COUNT*2);
  velo_settings.range_ref_1 = settings[0];
  velo_settings.range_ref_2 = settings[1];
  velo_settings.range_ref_3 = settings[2];
}

static void _get_default_settings(uint16_t * p_velo_settings)
{
  for (int i=0;i<VELO_SETTINGS_COUNT;i++)
  {
    p_velo_settings[i]=0xFFFF;
  }
}

static const uint8_t n_blocks = VELO_FLASH_SIZE/VELO_FLASH_BLOCK_SIZE;

// ############################ Settings: ############################ 

static void _get_crc()
{
  //Calculate crc:
  uint16_t crc = crc16_compute(&s_velo_config[VELO_SETTINGS_START], VELO_FLASH_BLOCK_SIZE-VELO_SETTINGS_START, NULL);
  // Put crc into config array:
  memcpy(&s_velo_config[VELO_CONFIG_CRC_LOC], &crc, 2);
}

static void _initialise_config()
{
  // Initialise array:
  NRF_LOG_INFO("n_blocks = %u",n_blocks);
  memset(s_velo_config,0xFF,VELO_FLASH_BLOCK_SIZE);
  uint16_t p_velo_settings[VELO_SETTINGS_COUNT];

  // Get default values:
  _get_default_settings(p_velo_settings);
  // Copy over to config array:
  memcpy(&s_velo_config[VELO_SETTINGS_START],p_velo_settings,VELO_SETTINGS_COUNT*2);

  //Calculate crc:
  _get_crc();

  //Put in header byte:
  velo_config_header_t * p_header = (velo_config_header_t *) &s_velo_config[0];
  p_header->uninitialised=0;
  p_header->active = 1;
  p_header->counter = 0;
  s_active_block=0;

  //Erase any old data to be sure we start blank:
  velo_flash_erase(false);

  //Write data:
  NRF_LOG_INFO("Source address = 0x%x",(uint32_t)s_velo_config);
  NRF_LOG_FLUSH();  

  //Setup write operation:
  velo_flash_data_t data;

  block_data_to_write.address = VELO_FLASH_START_ADDR + s_active_block * VELO_FLASH_BLOCK_SIZE;
  block_data_to_write.byte_count = VELO_FLASH_BLOCK_SIZE;
  block_data_to_write.p_data = s_velo_config;
  velo_flash_write(&block_data_to_write, false);
  NRF_LOG_INFO("Settings initialised.");
}


static void _settings_save(bool increment_counter)
{

  // Assume that header block hasn't changed, so we just dactivate, and save that 4-byte block.
  velo_config_header_t * p_header = (velo_config_header_t *) &s_velo_config[0];

  // Find new block location.
  s_active_block+=1;
  // Erase page if we are at end of it, and reset active_block variable:
  if (s_active_block>=n_blocks)
  {
    s_active_block=0;
    velo_flash_erase(true);
    NRF_LOG_INFO("Settings page erased.");
  }
  else
  {  
    // Deactivate old settings.
    uint8_t write_count= 1;
    //Deactivate
    p_header->active = 0;
    //Write back the deactivated block
    NRF_LOG_INFO("Setting up write to deactivate old block");
    memcpy(s_header,p_header,sizeof(p_header));
    header_data_to_write.address = VELO_FLASH_START_ADDR + (s_active_block-1) * VELO_FLASH_BLOCK_SIZE;
    header_data_to_write.byte_count= VELO_SETTING_HEADER_SIZE;
    header_data_to_write.p_data = s_header;
    
    NRF_LOG_INFO("Writing header 0x%x to block 0x%x",s_header[0],header_data_to_write.address);
    velo_flash_write(&header_data_to_write, true);
  }

  //Calcualte crc:
  _get_crc();

  // Update rest of header:
  p_header->active = 1;
  if (increment_counter) {p_header->counter += 1;}  // Note that this rolls-over automatically in same way as in aero profile.
  p_header->uninitialised = 0;  // Should already be like this, but just to be sure..
  
  //Write data:
  NRF_LOG_INFO("About to write %u bytes to config address 0x%x from save_config",VELO_FLASH_BLOCK_SIZE,VELO_FLASH_START_ADDR + s_active_block * VELO_FLASH_BLOCK_SIZE);

  block_data_to_write.address = VELO_FLASH_START_ADDR + s_active_block * VELO_FLASH_BLOCK_SIZE;
  block_data_to_write.byte_count = VELO_FLASH_BLOCK_SIZE;
  block_data_to_write.p_data = s_velo_config;
  velo_flash_write(&block_data_to_write, true);
}

void velo_settings_load()
{
  // Read 128 byte settings.
  // Check for active.  If not active, load next set.
  // If we get through all 32 blocks and find no active, then need to initialise first block.
  s_active_block = 255;

  // Create pointer to header block:
  velo_config_header_t * p_header = (velo_config_header_t *) &s_velo_config[0];
  
  for (int i=0;i<n_blocks;i++)
  {
    velo_flash_read(VELO_FLASH_START_ADDR + i * VELO_FLASH_BLOCK_SIZE, s_velo_config, VELO_FLASH_BLOCK_SIZE);
     NRF_LOG_INFO("Block %u header = 0x%x",i,s_velo_config[0]);
    if (!p_header->uninitialised & p_header->active)
    {
      s_active_block=i;
      
      NRF_LOG_INFO("Active config found = %u.",s_active_block);
      break;
    }
  }
  // If active block found, then we're done as array already copied over.
  if (s_active_block >= n_blocks)
  {
    NRF_LOG_INFO("No active config found: Initialising config.");
    _initialise_config();
  }
  _settings_convert();
}
 
void velo_settings_get(uint16_t * p_velo_settings)
{
  
  // Assume settings already loaded into the static 1D array, and just copy across:
  // Convert to 16 bit integers:
  memcpy(p_velo_settings,&s_velo_config[VELO_SETTINGS_START],VELO_SETTINGS_COUNT*2);
}

void velo_settings_save()
{
  uint16_t p_velo_settings[VELO_SETTINGS_COUNT];
  p_velo_settings[0] = velo_settings.range_ref_1;
  p_velo_settings[1] = velo_settings.range_ref_2;
  p_velo_settings[2] = velo_settings.range_ref_3;
  memcpy(&s_velo_config[VELO_SETTINGS_START],p_velo_settings,VELO_SETTINGS_COUNT*2);
  _settings_save(true);
}

uint8_t velo_settings_counter()
{
  velo_config_header_t * p_header = (velo_config_header_t *) &s_velo_config[0];
  return p_header->counter;
}