#ifndef VELO_FLASH_H__
#define VELO_FLASH_H__

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

// Settings location in on-board flash memory:
#define VELO_FLASH_START_ADDR     0xF6000
#define VELO_FLASH_END_ADDR       0xF6FFF
#define VELO_FLASH_SIZE           0x1000
#define VELO_FLASH_BLOCK_SIZE     32

typedef struct 
{
  uint8_t * p_data;
  uint32_t address;
  uint8_t byte_count; // Assume never need to write more than 255 bytes.
}velo_flash_data_t;

void velo_flash_init();
void velo_flash_read(uint32_t first_byte_number, uint8_t * p_data, uint16_t byte_count);
void velo_flash_write(velo_flash_data_t * p_data, bool schedule);
void velo_flash_erase(bool schedule);

#endif //VELO_FLASH_H__
