#include "velo_circ_buffer.h"
#define NRF_LOG_MODULE_NAME velo_circ_buffer
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
NRF_LOG_MODULE_REGISTER();


void _advance_pointer(velo_circ_buffer_t * p_circ_buffer)
{
  if (p_circ_buffer->full)
  {
    p_circ_buffer->tail = (p_circ_buffer->tail + 1) % p_circ_buffer->buffer_size;
    p_circ_buffer->size = p_circ_buffer->buffer_size;
  }
  else
  {
    p_circ_buffer->size++;
  }
  p_circ_buffer->head = (p_circ_buffer->head + 1) % p_circ_buffer->buffer_size;
  p_circ_buffer->full = (p_circ_buffer->head == p_circ_buffer->tail);
  //NRF_LOG_INFO("Advance buffer 0x%x: size = %u",p_circ_buffer, p_circ_buffer->size);
}

void _retreat_pointer(velo_circ_buffer_t * p_circ_buffer)
{
  p_circ_buffer->full = false;
  p_circ_buffer->tail = (p_circ_buffer->tail + 1) % p_circ_buffer->buffer_size;
  p_circ_buffer->size--;
  //NRF_LOG_INFO("Retreat buffer 0x%x: size = %u",p_circ_buffer, p_circ_buffer->size);
}


void velo_circ_buffer_push(velo_circ_buffer_t * p_circ_buffer, uint8_t * p_data)
{
  // Enter critical region, so we don't do anything else whilst pushing to buffer.
  uint8_t is_nested;
  sd_nvic_critical_region_enter(&is_nested);
  memcpy(&p_circ_buffer->buffer[p_circ_buffer->head*p_circ_buffer->object_size],p_data,p_circ_buffer->object_size);
  _advance_pointer(p_circ_buffer);
  sd_nvic_critical_region_exit(is_nested) ;
}

bool velo_circ_buffer_pop(velo_circ_buffer_t * p_circ_buffer, uint8_t * p_data)
{
  if (p_circ_buffer->size == 0)
  {
    return false;
  }
  // Enter critical region, so we don't do anything else whilst popping from buffer.
  sd_nvic_critical_region_enter(0);
  if (p_data != NULL)
  {
    memcpy(p_data,&p_circ_buffer->buffer[p_circ_buffer->tail*p_circ_buffer->object_size],p_circ_buffer->object_size);
  }
  _retreat_pointer(p_circ_buffer);
  sd_nvic_critical_region_exit(0) ;
  return true;
}

void velo_circ_buffer_reset(velo_circ_buffer_t * p_circ_buffer)
{
  p_circ_buffer->head = 0;
  p_circ_buffer->tail = 0;
  p_circ_buffer->size = 0;
  p_circ_buffer->full = false; 
}

void velo_circ_buffer_init(velo_circ_buffer_t * p_circ_buffer, uint8_t * buffer, uint16_t object_size, uint16_t buffer_size)
{
  ASSERT(IS_POWER_OF_TWO(buffer_size));
  p_circ_buffer->object_size = object_size; // Size of each object in the buffer.
  p_circ_buffer->buffer_size = buffer_size; // Maximum number of objects in the buffer.  Needs to be a power of 2
  p_circ_buffer->buffer = buffer;
  velo_circ_buffer_reset(p_circ_buffer);
}

void velo_circ_buffer_peek(velo_circ_buffer_t * p_circ_buffer, uint8_t * p_data,  uint16_t index)
{
  
  if ((!(p_circ_buffer->size == 0)) & (p_circ_buffer->size > index))
  {
    memcpy(p_data,&p_circ_buffer->buffer[((p_circ_buffer->buffer_size - 1) & (p_circ_buffer->head - (index + 1))) 
                      * p_circ_buffer->object_size],p_circ_buffer->object_size);
  }
  else
  {
    p_data = NULL;
  }
}

void velo_circ_buffer_overwrite(velo_circ_buffer_t * p_circ_buffer, uint8_t * p_data,  uint16_t index)
{
  
  if ((!(p_circ_buffer->size == 0)) & (p_circ_buffer->size > index))
  {
    memcpy(&p_circ_buffer->buffer[((p_circ_buffer->buffer_size - 1) & (p_circ_buffer->head - (index + 1))) 
                      * p_circ_buffer->object_size],p_data, p_circ_buffer->object_size);
  }
}