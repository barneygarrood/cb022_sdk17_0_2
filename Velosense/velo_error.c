#include "velo_error.h"

#include "app_error.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "app_util_platform.h"
#include "nrf_strerror.h"

#if defined(SOFTDEVICE_PRESENT) && SOFTDEVICE_PRESENT
#include "nrf_sdm.h"
#endif

// Module to override standard error fault handler.  Callback to velo_driver to send ANT message.


static velo_error_cb_t _callback;

void velo_error_init(velo_error_cb_t callback)
{
  _callback = callback;
}
static uint32_t ia;
void app_error_fault_handler(uint32_t id, uint32_t pc, uint32_t info)
{
  ia = pc;
  velo_status.error_status.stack_ptr = pc;
  NRF_LOG_INFO("Error at stack address 0x%08x",(unsigned int)pc);
  error_info_t * p_info = (error_info_t *)info;
  velo_status.error_status.err_code = (uint16_t) (p_info->err_code & 0xFFFF);
  switch (id)
  {
#if defined(SOFTDEVICE_PRESENT) && SOFTDEVICE_PRESENT
    case NRF_FAULT_ID_SD_ASSERT:
      NRF_LOG_ERROR("SOFTDEVICE: ASSERTION FAILED");
      velo_status.error_status.sd_assert = true;
      break;
    case NRF_FAULT_ID_APP_MEMACC:
      NRF_LOG_ERROR("SOFTDEVICE: INVALID MEMORY ACCESS");
      velo_status.error_status.sd_mem_access = true;
      break;
#endif
    case NRF_FAULT_ID_SDK_ASSERT:
    {
      NRF_LOG_ERROR("ASSERTION FAILED at %s:%u",
                    p_info->p_file_name,
                    p_info->line_num);
      velo_status.error_status.sdk_assert = true;
      break;
    }
    case NRF_FAULT_ID_SDK_ERROR:
    {
      NRF_LOG_ERROR("ERROR %u [%s] at %s:%u\r\nPC at: 0x%08x",
                  p_info->err_code,
                  nrf_strerror_get(p_info->err_code),
                  p_info->p_file_name,
                  p_info->line_num,
                  pc);
      velo_status.error_status.sdk_error = true;
      NRF_LOG_ERROR("End of error report");
      break;
    }
    default:
      NRF_LOG_ERROR("UNKNOWN FAULT at 0x%08X", pc);
      break;
  }
  NRF_LOG_FLUSH();
  velo_status.error_status.error = true;

  // here we are sending a copy of the error status struct.  Although this is stored in velo_status
  // it could change later down the road.  may want to detach it from velo_status altogether?
  _callback(velo_status.error_status);
}

void velo_error_check_fpu()
{
  uint32_t fpscr = __get_FPSCR();
  if (fpscr & 0x07)
  {
    NRF_LOG_INFO("Issue in FPU");
  }
}