#ifndef VELO_DRIVER_H__
#define VELO_DRIVER_H__

#include <stdint.h>

typedef struct
{
	float baro_press_1;
	float baro_press_2;
	int16_t sdp_1;
	int16_t sdp_2;
	float baro_temp_1;
	float baro_temp_2;
	float baro_temp;
	float pdyn;
	float angle;
	
}velo_data_t;


extern uint8_t ant_velo_period_index;
extern uint8_t velo_poll_count;
extern uint8_t velo_max_poll_count;

/**@brief Returns microsecond clock count. */
uint32_t time_us(void);


/**@brief Function for initializing twi reader. */
void twi_read_init(void);

/**@brief Function for reading sensors. */
void read(void);
	
 /**@brief Initialisation function. */
void velo_init(void);

void velo_start_pairing(void);
void velo_stop_pairing(void);
void scan_twi(void);

#endif //VELO_DRIVER_H__

