#ifndef VELO_ACCEL_H__
#define VELO_ACCEL_H__

#include <math.h>
#include <stdint.h>
#include <stdbool.h>

#define VELO_ACCEL_RINGBUFFER_SIZE  8

typedef struct 
{ 
  float accel_x_sum;
  float accel_y_sum;
  float accel_z_sum;

  float accel_x_mean;
  float accel_y_mean;
  float accel_z_mean;

  uint16_t accel_mag_max;

  int16_t x_accel_inact;
  int16_t y_accel_inact;
  int16_t z_accel_inact;
  int32_t time_inact;
  uint16_t count;
  bool in_progress;

}velo_accel_data_t;

void velo_accel_sleep_mode();
void velo_accel_init(velo_accel_data_t * p_accel);
void velo_accel_reset_interrupts();
void velo_accel_setup_activity_interrupt();
void velo_accel_clear_tares();
void velo_accel_start_taring();
void velo_accel_end_taring();
void velo_accel_read(velo_accel_data_t * p_accel);
void velo_accel_clear_interrupt();
void velo_accel_reset_inactivity(velo_accel_data_t * p_accel);
void velo_accel_calc_stats(velo_accel_data_t * p_accel);

#endif //VELO_ACCEL_H__

