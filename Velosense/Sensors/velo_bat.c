
#include "nrf_log_ctrl.h"
#include "nrf_delay.h"
#include "velo_bat.h"
#include "velo_us_timer.h"
#include "velo_config.h"
#include "nrf_gpio.h"

#define NRF_LOG_MODULE_NAME velo_bat
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();
#define TIMEOUT 2000   //2s timeout..

static uint8_t soc_register = 0x1C;
static uint8_t voltage_register = 0x04;

velo_bat_status_enum_t velo_bat_get_status(velo_bat_t * p_battery)
{
  if (p_battery->soc < VELO_BAT_SOC_OFF)
  {
     return VELO_BAT_STATE_SUB_CRITICAL;
  }
  else if (p_battery->soc < VELO_BAT_SOC_ON)
  {
     return VELO_BAT_STATE_CRITICAL;
  }
  else
  {
     return VELO_BAT_STATE_OK;
  }
}

void velo_bat_init(velo_bat_t * p_battery)
{
  p_battery->last_poll_ms=0;
  p_battery->soc=0;
  p_battery->voltage=0;
  velo_bat_gauge_init();
}

void velo_bat_gauge_get_soc_transfer(uint8_t * p_data, nrf_twi_mngr_transfer_t * p_trans)
{
  velo_twi_read_register_transfer(BAT_GAUGE_ADDRESS, &soc_register, p_data, 2, p_trans);
}


void velo_bat_gauge_get_voltage_transfer(uint8_t * p_data, nrf_twi_mngr_transfer_t * p_trans)
{
  velo_twi_read_register_transfer(BAT_GAUGE_ADDRESS, &voltage_register, p_data, 2, p_trans);
}


void velo_bat_gauge_enter_shutdown_transfer(nrf_twi_mngr_transfer_t * p_trans)
{
  // enable shutdown mode.
   uint8_t reg[3]={0x00,0x1B,0x00};
   velo_twi_write_transfer(BAT_GAUGE_ADDRESS,reg,3,&p_trans[0]);
  // enter shutdown mode.
   reg[1] = 0x1C;
   velo_twi_write_transfer(BAT_GAUGE_ADDRESS,reg,3,&p_trans[1]);
}

int32_t velo_bat_gauge_read(velo_bat_t * battery)
{
  // Setup 4 transfers to read two registers.
  // each read requires writing the register address, then reading the bytes
  nrf_twi_mngr_transfer_t trans[4];
  uint8_t buffer[4];  // two bytes each for soc and voltage:
  // Create transfers:
  velo_bat_gauge_get_soc_transfer(&buffer[0],&trans[0]);
  velo_bat_gauge_get_voltage_transfer(&buffer[2],&trans[2]);
  ret_code_t ret = velo_twi_perform_transfers(trans,4);
  battery->soc = (uint8_t)uint16_decode(&buffer[0]);
  battery->voltage = uint16_decode(&buffer[2]);
  //NRF_LOG_INFO("Bat voltage = %u, SOC = %u", battery->voltage,battery->soc);
  return ret;
}


static void _write_cmd(uint8_t * p_cmd, uint8_t count)
{
  nrf_twi_mngr_transfer_t trans[1];
  velo_twi_write_transfer(BAT_GAUGE_ADDRESS,p_cmd,count,trans);
  velo_twi_perform_transfers(trans,1);
  // Note that we always require a little delay here to let sensor sort itself out.
  nrf_delay_ms(10);
}

static void _enter_cfg_update()
{
  nrf_twi_mngr_transfer_t trans[2];
  uint8_t cmd[3]; uint8_t dat[1];
  cmd[0] = 0x00; cmd[1] = 0x13; cmd[2] = 0x00;
  _write_cmd(&cmd[0],3);

  // wait until flag changes to show that we are in config update mode:
  uint32_t start = time_ms();
  cmd[0] = 0x06;
  velo_twi_read_register_transfer(BAT_GAUGE_ADDRESS, &cmd[0], &dat[0], 1, &trans[0]);
  while(!(dat[0] & 0x10))
  { 
    velo_twi_perform_transfers(&trans[0], 2);
    nrf_delay_ms(10);
  } 
}

static void _reset()
{
  nrf_twi_mngr_transfer_t trans[2];
  uint8_t cmd[3]; uint8_t dat[1];
  cmd[0] = 0x00; cmd[1] = 0x42; cmd[2] = 0x00;
  _write_cmd(&cmd[0],3);

  // wait until flag changes to show that we are in config update mode:
  uint32_t start = time_ms();
  cmd[0] = 0x06;
  velo_twi_read_register_transfer(BAT_GAUGE_ADDRESS, &cmd[0], &dat[0], 1, &trans[0]);
  while(dat[0] & 0x10)
  { 
    velo_twi_perform_transfers(&trans[0], 2);
    nrf_delay_ms(10);
  } 
}

static void _read_register(uint8_t *p_data, uint8_t data_class, uint8_t offset, uint8_t data_len)
{
  nrf_twi_mngr_transfer_t trans[2];
  uint8_t cmd[2];
  cmd[0]=0x61; cmd[1] = 0x00;
  _write_cmd(cmd,2);
  cmd[0]=0x3E; cmd[1] = data_class;
  _write_cmd(cmd,2);
  uint8_t data_offset = offset<32 ? 0 : 1;
  cmd[0]=0x3F; cmd[1] = data_offset;
  _write_cmd(cmd,2);
  //read register:
  cmd[0] = 0x40 + offset;
  velo_twi_read_register_transfer(BAT_GAUGE_ADDRESS,&cmd[0],p_data, data_len,&trans[0]);
  velo_twi_perform_transfers(trans, 2);
}

static void _write_register(uint8_t *p_old_data, uint8_t *p_new_data, uint8_t data_class, uint8_t offset, uint8_t data_len)
{
  nrf_twi_mngr_transfer_t trans[2];
  uint8_t cmd[8];
  cmd[0]=0x61; cmd[1] = 0x00;
  _write_cmd(cmd,2);
  cmd[0]=0x3E; cmd[1] = data_class;
  _write_cmd(cmd,2);
  uint8_t data_offset = offset<32 ? 0 : 1;
  cmd[0]=0x3F; cmd[1] = data_offset;
  _write_cmd(cmd,2);
  // read old checksum:
  cmd[0] = 0x60;
  uint8_t old_chksum;
  velo_twi_read_register_transfer(BAT_GAUGE_ADDRESS,&cmd[0],&old_chksum,1,&trans[0]);
  velo_twi_perform_transfers(trans, 2);

  // Compute new checksum.
  uint8_t temp = 255-old_chksum;
  for (int i=0;i<data_len;i++)
  {
    temp-=p_old_data[i];
  }
  uint8_t new_chksum = 255 - temp;
  for (int i=0;i<data_len;i++)
  {
    new_chksum-=p_new_data[i];
  }

  // Write new values:
  cmd[0]=0X40 + offset;
  for (int i=0;i<data_len;i++)
  {
    cmd[i+1]=p_new_data[i];
  }
  _write_cmd(cmd,data_len+1);

  // Write new checksum to register 0x60
  cmd[0] = 0x60;
  cmd[1] = new_chksum;
  _write_cmd(cmd,2);
}


// Function to initialise battery gauge.
void velo_bat_gauge_init()
{
  nrf_twi_mngr_transfer_t trans[2];

  uint8_t cmd[8];
  uint8_t dat[8];

  //Toggle GPOut - makes sure battery gauge boots correctly (toggling forces it to exit shutdown mode).
  nrf_gpio_cfg_input(PIN_GPOUT,NRF_GPIO_PIN_PULLDOWN);
  nrf_delay_ms(1);
  nrf_gpio_cfg_input(PIN_GPOUT,NRF_GPIO_PIN_PULLUP);
  nrf_delay_ms(1);


//  // Enter update mode:
//  _enter_cfg_update();
//  // get old op config register values:
//  uint8_t old_opconfig[2];
//  _read_register(old_opconfig,0x40,0x00,2);
//
//  // Modify OpConfig flags: high byte bit 3->0 low byte bit 2->1:
//  uint8_t new_opconfig[2];
//  new_opconfig[0] = old_opconfig[0] & ~(1UL<<3);
//  new_opconfig[1] = old_opconfig[1] | (1UL<<2);
//  // Write new values:
//  _write_register(old_opconfig,new_opconfig,0x40,0x00,2);
//  
//  // get old SOC1 threashold data:
//  uint8_t old_SOC1_threshold[2];
//  _read_register(old_SOC1_threshold, 0x31, 0x00, 2);
//  // Write new values (10% set, 15% clear thresholds):
//  uint8_t new_SOC1_threshold[2] = {VELO_BAT_SOC_OFF, VELO_BAT_SOC_ON};
//  // Write new values:
//  _write_register(old_SOC1_threshold, new_SOC1_threshold, 0x31, 0x00, 2);
//
//  // reset unit - gets out of update mode.
//  _reset();

}