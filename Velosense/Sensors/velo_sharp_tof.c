#include "velo_sharp_tof.h"
#include "velo_twi.h"
#include "nrf_delay.h"
#include "nrf_log_ctrl.h"
#include "nrf_error.h"

#include "gp2ap02vt_api.h"
#include "gp2ap02vt_i2c.h"
#include "gp2ap02vt_common.h"

#define NRF_LOG_MODULE_NAME velo_sharp_tof
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

/////////////////////////////////////////////////////////////////////////////


velo_lidar_status_enum_t velo_sharp_tof_setup(GP2AP0XVT_Dev_t * p_dev) 
{
ret_code_t ret;
  /* check device id */
  p_dev->slave_addr = 0x29; /* ( 0x29 << 1 ) | X, write_mode:0x52, read_mode:0x53 */
  ret = GetDeviceID(p_dev);
  if (ret != NRF_SUCCESS)
  {
    return VELO_LIDAR_DISCONNECTED;
  }
  //NRF_LOG_INFO("Device ID = 0x%x", p_dev->data.device_id);
  if (InitData(p_dev, LONG_RANGE_MODE) == INITIALIZATION_FAILED) 
  {
    NRF_LOG_INFO("INITIALIZATION FAILED!!!");
    return VELO_LIDAR_DISCONNECTED;
  }
  func_start(p_dev, LONG_RANGE_MODE);
  StartMeasurement(p_dev); /* start measurement */
  DELAY_FUNC(MEASUREMENT_TIME33M);
  NRF_LOG_FLUSH();
  return VELO_LIDAR_MEAS_NOT_READY;
}


velo_lidar_status_enum_t velo_sharp_tof_checkGetMeasurement(GP2AP0XVT_Dev_t * p_dev)
{
  uint8_t status;
  velo_lidar_status_enum_t ret;
  status = CheckMeasurementEndFlag(p_dev);
  //NRF_LOG_INFO("status flag = %u",status);
  if (status == MEASUREMENT_ENDED)
   {
    GetMeasurementData(p_dev);
    p_dev->data.range_mm = p_dev->data.range1;
    //NRF_LOG_INFO("Sharp data = %u",p_dev->data.range_mm);
    ClearMeasurementEndFlag(p_dev);
    ret = VELO_LIDAR_MEAS_READY;
    StartMeasurement(p_dev); /* restart */
  }
  else if (status == MEASUREMENT_NOT_END)
  {
    ret = VELO_LIDAR_MEAS_NOT_READY;
  }
  else
  {
    ret = VELO_LIDAR_DISCONNECTED;
  }
  NRF_LOG_FLUSH();
  return ret;
}