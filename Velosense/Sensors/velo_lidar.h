#ifndef VELO_LIDAR_H__
#define VELO_LIDAR_H__
#include "nrf_delay.h"
#include "velo_us_timer.h"
#include "velo_config.h"
#include "nrf_gpio.h"
#include "velo_lidar_common.h"
#include "vl53l0x_platform.h"
#include "gp2ap02vt_api.h"

typedef struct 
{ 
  VL53L0X_Dev_t st_device;
  GP2AP0XVT_Dev_t sharp_device;
  uint8_t device_id : 2;
  uint8_t unused : 6;

//  // Registers to store data - swap between these as we add values to prevent ANT taking half-way through sampling.
//  uint16_t range_sum[2];
//  uint8_t count[2];
//  uint8_t valid_count[2];
//  uint8_t toggle;
//  uint8_t first_two_status[2];
//  bool reset;

  float pct_valid;
  float range_av;
  uint32_t last_poll_ms;

  bool connected;
  uint8_t type;

}velo_lidar_device_t;

#define DEFAULT_VELO_LIDAR_DEVICE(id) \
  (velo_lidar_device_t) \
  { \
    .device_id = (id), \
    .connected = VELO_LIDAR_DISCONNECTED, \
    .type = VELO_LIDAR_TYPE_ST, \
  } \

velo_lidar_status_enum_t velo_lidar_init(velo_lidar_device_t * p_lidar, uint8_t address, bool trySharp);
void velo_lidar_read(velo_lidar_device_t * p_lidar);
void velo_lidar_calc(velo_lidar_device_t * p_lidar);
#endif //VELO_LIDAR_H__

