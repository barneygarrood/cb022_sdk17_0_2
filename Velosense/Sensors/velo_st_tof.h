#ifndef VELO_ST_TOF_H__
#define VELO_ST_TOF_H__
#include "vl53l0x_platform.h"
#include "velo_lidar_common.h"

velo_lidar_status_enum_t velo_st_tof_setup(uint8_t id, VL53L0X_Dev_t * p_lidar, uint8_t address);
velo_lidar_status_enum_t velo_st_tof_checkGetMeasurement(VL53L0X_Dev_t * p_lidar);

#endif //VELO_ST_TOF_H__