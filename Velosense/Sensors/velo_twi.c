#include "velo_twi.h"
#include "nrf_log_ctrl.h"
#include "velo_config.h"
#include "nrf_twi_mngr.h"

#define NRF_LOG_MODULE_NAME velo_twi
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

NRF_TWI_MNGR_DEF(m_nrf_twi_mngr, MAX_PENDING_TRANSACTIONS, TWI_INSTANCE_ID_0);

uint32_t velo_twi_event_time[2] = {0,0};
//twi_status says if waiting for response or not.
// One value for each twi channel, = 1 if a transaction scheduled.
uint8_t velo_twi_status[2]={0,0};

/*
 * --------------------------- TWI setup start ----------------------------------------------*
 */

// TWI (with transaction manager) initialization.
int32_t velo_twi_init()
{
  ret_code_t err_code;
  nrf_drv_twi_config_t twi_config = {
           .scl				= PIN_SCL,
           .sda				= PIN_SDA,
            .frequency                  = NRF_TWI_FREQ_400K,
           .interrupt_priority          = APP_IRQ_PRIORITY_HIGHEST,
           .clear_bus_init		= false
          };
  // Initilaise TWI 0
  err_code = nrf_twi_mngr_init(&m_nrf_twi_mngr, &twi_config);
  return err_code;
}

void velo_twi_uninit()
{
  nrf_twi_mngr_uninit(&m_nrf_twi_mngr);
}

static nrf_twi_mngr_transaction_t NRF_TWI_MNGR_BUFFER_LOC_IND transact;

// Function to schedule transfers.  On completion of transfers the callback function is called.
void velo_twi_schedule_transfers(nrf_twi_mngr_transfer_t * p_trans, uint8_t transfer_count, nrf_twi_mngr_callback_t cb_function)
{    
  ret_code_t err_code;
    
  transact.callback = cb_function,
  transact.p_user_data = NULL,
  transact.p_transfers = p_trans,
  transact.number_of_transfers = transfer_count;
    
  err_code=nrf_twi_mngr_schedule(&m_nrf_twi_mngr, &transact);
  APP_ERROR_CHECK(err_code);
}

// Function to perform a transfer and wait for it to be returned.
int32_t velo_twi_perform_transfers(nrf_twi_mngr_transfer_t * p_trans, uint8_t trans_count)
{
  ret_code_t err_code = nrf_twi_mngr_perform(&m_nrf_twi_mngr, NULL, p_trans, trans_count, NULL);
  if (err_code != 0)
  {
    NRF_LOG_INFO("TWI error.");
    //APP_ERROR_CHECK(err_code);
  }
  return err_code;
}



// Function to create write transfer:
void velo_twi_write_transfer(uint8_t twi_address, uint8_t * p_data, uint8_t data_len, nrf_twi_mngr_transfer_t * p_trans)
{
  p_trans[0] = (nrf_twi_mngr_transfer_t)NRF_TWI_MNGR_WRITE(twi_address,p_data,data_len,0);

//  p_trans->operation=NRF_TWI_MNGR_WRITE_OP(twi_address);
//  p_trans->p_data=p_data;
//  p_trans->length=data_len;
//  p_trans->flags=0;
}

// Function to create read transfer:
void velo_twi_read_transfer(uint8_t twi_address, uint8_t * p_data, uint8_t data_len, nrf_twi_mngr_transfer_t * p_trans)
{
  p_trans[0] = (nrf_twi_mngr_transfer_t)NRF_TWI_MNGR_READ(twi_address,p_data,data_len,0);

//  p_trans->operation=NRF_TWI_MNGR_READ_OP(twi_address);
//  p_trans->p_data=p_data;
//  p_trans->length=data_len;
//  p_trans->flags=0;
}

//Function to create transfers to read a specific register, which means first writing register address, then reading data.
void velo_twi_read_register_transfer(uint8_t twi_address, uint8_t * p_reg_addr, uint8_t * p_data, uint8_t data_len, nrf_twi_mngr_transfer_t * p_trans)
{
  // NRF_LOG_INFO("Reading %u bytes from register 0x%x",data_len,*p_reg_addr);
  velo_twi_write_transfer(twi_address,p_reg_addr,1,&p_trans[0]);
  velo_twi_read_transfer(twi_address,p_data,data_len,&p_trans[1]);
}


// Perform writing of data to register immediately:
int32_t velo_twi_perform_write_register(uint8_t twi_address, uint8_t reg_addr, uint8_t * p_data, uint8_t data_len)
{
  nrf_twi_mngr_transfer_t p_trans[1];
  uint8_t data[8];
  data[0] = reg_addr;
  for (int i=0;i<data_len;i++)
  {
    data[i+1] = p_data[i];
  }
  velo_twi_write_transfer(twi_address, data, data_len+1, p_trans);
  return velo_twi_perform_transfers(p_trans,1);
}


// Perform writing of data to register immediately:
int32_t velo_twi_perform_write_byte_to_register(uint8_t twi_address, uint8_t reg_addr, uint8_t byte)
{
  nrf_twi_mngr_transfer_t p_trans[1];
  uint8_t data[2];
  data[0] = reg_addr;
  data[1] = byte;
  velo_twi_write_transfer(twi_address, data, 2, p_trans);
  return velo_twi_perform_transfers(p_trans,1);
}

int32_t velo_twi_perform_read(uint8_t twi_address, uint8_t reg_addr, uint8_t * p_data, uint8_t data_len)
{
  nrf_twi_mngr_transfer_t p_trans[2];
  velo_twi_read_register_transfer(twi_address, &reg_addr, p_data, data_len, p_trans);
  return velo_twi_perform_transfers(p_trans,2);
  
}