#include "velo_lidar.h"
#include <math.h>
#include "velo_sharp_tof.h"
#include "velo_st_tof.h"
#include "velo_circ_buffer.h"
#include "velo_us_timer.h"

#define NRF_LOG_MODULE_NAME velo_lidar
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

#define LIDAR_RINGBUFFER_SIZE  64
#define LIDAR_COUNT 3
#define BYTES_PER_ENTRY   5  // time x2, range x 2, range status x 1.

velo_circ_buffer_t _circ_buffers[LIDAR_COUNT];
uint8_t _buffers[LIDAR_RINGBUFFER_SIZE * LIDAR_COUNT * BYTES_PER_ENTRY];

// Initialise velosense lidar object.
// NOTE This is distinct from the VL53L0X_Dev_t object, which is the ST library object.
velo_lidar_status_enum_t velo_lidar_init(velo_lidar_device_t * p_lidar, uint8_t address, bool trySharp)
{

  // Initialise ring buffer:
  velo_circ_buffer_init(&_circ_buffers[p_lidar->device_id],&_buffers[p_lidar->device_id*LIDAR_RINGBUFFER_SIZE * BYTES_PER_ENTRY],BYTES_PER_ENTRY,LIDAR_RINGBUFFER_SIZE);
  
  // #################################################################################################################
  // Initilise range sensor device:
  // #################################################################################################################
  // Try to init ST device.
  velo_lidar_status_enum_t status;
  p_lidar->connected = false;
  status = velo_st_tof_setup(p_lidar->device_id, &p_lidar->st_device, address);
  // If successfu, mark as connected and ST sensor type.
  if (status != VELO_LIDAR_DISCONNECTED)
  {
    p_lidar->type = VELO_LIDAR_TYPE_ST;
    p_lidar->connected = true;
    NRF_LOG_INFO("Lidar %u type = %u",p_lidar->device_id, p_lidar->type);
  }
  // Else try to init Sharp device.
  else if (trySharp)
  {
    status = velo_sharp_tof_setup(&p_lidar->sharp_device);
    if (status != VELO_LIDAR_DISCONNECTED)
    {
      p_lidar->type = VELO_LIDAR_TYPE_SHARP;
      p_lidar->connected = true;
      NRF_LOG_INFO("Lidar %u type = %u",p_lidar->device_id, p_lidar->type);
    }
  }
  if (!p_lidar->connected)
  {  
    NRF_LOG_INFO("Lidar %u disconnected",p_lidar->device_id);
  }
  
  // Return status.
  return status;
}


// Add data to registers:
static void _add_new_data(velo_lidar_device_t * p_lidar, uint16_t range, uint8_t range_status, uint16_t time)
{
  uint8_t data[BYTES_PER_ENTRY];
  // Either reset registers, or carry over values from other register:
  memcpy(&data[0],&time,2);
  memcpy(&data[2],&range,2);
  data[4] = range_status;
  //NRF_LOG_INFO("Range = %u, status = %u",range, range_status);
  velo_circ_buffer_push(&_circ_buffers[p_lidar->device_id],data);
}

void velo_lidar_read(velo_lidar_device_t * p_lidar)
{
    uint16_t r; uint8_t s;
    if (p_lidar->connected == true)
    {
      if (p_lidar->type == VELO_LIDAR_TYPE_ST)
      {
        velo_lidar_status_enum_t status = velo_st_tof_checkGetMeasurement(&p_lidar->st_device);
        uint16_t now = time_ms() & 0xFFFF;
        if (status == VELO_LIDAR_MEAS_READY)
        {
          _add_new_data(p_lidar,p_lidar->st_device.Data.range_mm, p_lidar->st_device.Data.range_status,now);
        }
        else if (status == VELO_LIDAR_DISCONNECTED)
        {
          p_lidar->connected = false;
        }
      }
      else if (p_lidar->type == VELO_LIDAR_TYPE_SHARP)
      {
        velo_lidar_status_enum_t status = velo_sharp_tof_checkGetMeasurement(&p_lidar->sharp_device);
        uint16_t now = time_ms() & 0xFFFF;
        if (status == VELO_LIDAR_MEAS_READY)
        {
          _add_new_data(p_lidar, p_lidar->sharp_device.data.range_mm, p_lidar->sharp_device.data.range_status,now);
        }
        else if (status == VELO_LIDAR_DISCONNECTED)
        {
          p_lidar->connected = false;
        }
      }
    }
}

// Calculate average range and percentage valid.
void velo_lidar_calc(velo_lidar_device_t * p_lidar)
{
  uint8_t count_ready = 0;
  uint8_t count_valid = 0;
  p_lidar->range_av = 0;
  uint8_t data[BYTES_PER_ENTRY];
  uint8_t range_status[2];
  uint16_t range;

  while (velo_circ_buffer_pop(&_circ_buffers[p_lidar->device_id],data))
  {
    //velo_circ_buffer_peek(&_circ_buffers[p_lidar->device_id],data,i);
    count_ready ++;
    if (data[4] == 0)
    {
      memcpy(&range,&data[2],2);
      p_lidar->range_av += range;
      count_valid++;
    }
    else if (count_ready<2)
    {
      range_status[count_ready] = data[4];
    }
  }
  if (count_valid>0)
  {
    p_lidar->range_av /= count_valid;
    p_lidar->pct_valid = ((float)count_valid)/count_ready;
  }
  else
  {
    p_lidar->range_av = 8000 + count_ready * 100 + range_status[0] * 10 + range_status[1];
    p_lidar->pct_valid = 0;
  }
  //NRF_LOG_INFO("ID = %u, count = %u, count_valid = %u, range = %u",p_lidar->device_id, count_ready, count_valid, p_lidar->range_av);
  if (p_lidar->range_av > 500)
  {
    int x=0;
  }
}
