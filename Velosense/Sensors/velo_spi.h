#ifndef SPI_INTERFACE_H__
#define SPI_INTERFACE_H__

#include "nrf_drv_spi.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "app_error.h"
#include "velo_config.h"

#define SPI_INSTANCE   1
//#define SPI_SCK_PIN     15  ///< SDC serial clock (SCK) pin.
//#define SPI_MOSI_PIN    13  ///< SDC serial data in (DI) pin.
//#define SPI_MISO_PIN    14  ///< SDC serial data out (DO) pin.
//#define SPI_CS_PIN      18  ///< SDC chip select (CS) pin.


void velo_spi_init();
void velo_spi_uninit();
ret_code_t velo_spi_txrx(uint8_t * p_tx_buffer, uint8_t tx_count, uint8_t * p_rx_buffer, uint8_t rx_count);

#endif //SPI_INTERFACE_H__