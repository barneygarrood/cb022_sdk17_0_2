#include "velo_sensors.h"
#include "velo_status.h"
#include "velo_twi.h"
#include "velo_sharp_tof.h"

#include "nrf_log_ctrl.h"

#define NRF_LOG_MODULE_NAME velo_sensors
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

// Sensor data objects:
velo_sensor_data_t velo_sensor_data;

void velo_sensors_init_twi()
{
  uint8_t err_code;
  if (!velo_status.twi_init)
  {
    err_code = velo_twi_init();
    APP_ERROR_CHECK(err_code);
    velo_status.twi_init=true;
  }
}

void velo_sensors_init_bat()
{
  uint8_t err_code;
  if (!velo_status.twi_init)
  {
    err_code = velo_twi_init();
    APP_ERROR_CHECK(err_code);
    velo_status.twi_init=true;
  }
  if (!velo_status.bat_init)
  {
    velo_bat_init(&velo_sensor_data.battery);
    velo_status.bat_init=1;
  }
}


// NOTE Initialisation function for sensors
void velo_sensors_init(bool init_lidar)
{
  uint8_t err_code;
  if (!velo_status.twi_init)
  {
    err_code = velo_twi_init();
    APP_ERROR_CHECK(err_code);
    velo_status.twi_init=true;
  }

  if (!velo_status.bat_init)
  {
    velo_bat_init(&velo_sensor_data.battery);
    velo_status.bat_init=1;
  }

  if (!velo_status.accel_init)
  {
    velo_accel_init(&velo_sensor_data.accel);
    velo_status.accel_init=1;
  }
  // Make sure all are of:
  nrf_gpio_pin_write(PIN_VL_1_XSHUT, 0);
  nrf_gpio_pin_write(PIN_VL_2_XSHUT, 0);
  nrf_gpio_pin_write(PIN_VL_3_XSHUT, 0);

  if (init_lidar && !velo_status.lidar_init)
  {
    velo_status.lidar_disconnected=0;
    velo_lidar_status_enum_t status;

  //NOTE If uninitialised Sharp sensor is plugged in, the ST sensors don't read properly.  I don't know why this is.
    //NOTE I have tried only initialising and reading from ST, and if sharp is plugged in it doesnt work.
    //NOTE Enable pin doesn't seem to do anything on Sharp.
    //NOTE Therefore need to initialise Sharp sensor first.
    // Turn on lidar 3:
    nrf_gpio_pin_write(PIN_VL_3_XSHUT, 1);
    // Give it a sec to come up.
    nrf_delay_ms(10);
    // Initialise - basically loads calibration settings and sets it into correct read mode.
    velo_sensor_data.lidar_3 = DEFAULT_VELO_LIDAR_DEVICE(2);
    status = velo_lidar_init(&velo_sensor_data.lidar_3,LIDAR_3_ADDRESS, true);
    if (status == VELO_LIDAR_DISCONNECTED)
    {
      NRF_LOG_INFO("Lidar 3 ST failed to initialise");
      velo_sensor_data.lidar_3.connected = false;
      velo_status.lidar_disconnected=1;
    }
    else
    {
      NRF_LOG_INFO("Lidar 3 initialised")
      velo_sensor_data.lidar_3.connected = true;
    }
    velo_sensor_data.lidar_3.last_poll_ms = time_ms();
   
    // Turn on lidar 1:
    nrf_gpio_pin_write(PIN_VL_1_XSHUT, 1);
    // Give it a sec to come up.
    nrf_delay_ms(10);
    // Initialise - basically loads calibration settings and sets it into correct read mode.
    velo_sensor_data.lidar_1 = DEFAULT_VELO_LIDAR_DEVICE(0);
    status = velo_lidar_init(&velo_sensor_data.lidar_1,LIDAR_1_ADDRESS, false);
    if (status == VELO_LIDAR_DISCONNECTED)
    {
      NRF_LOG_INFO("Lidar 1 failed to initialise");
      velo_sensor_data.lidar_1.connected = false;
      velo_status.lidar_disconnected=1;
    }
    else
    {
      NRF_LOG_INFO("Lidar 1 initialised")
      velo_sensor_data.lidar_1.connected = true;
    }
    velo_sensor_data.lidar_1.last_poll_ms = time_ms();

    // Turn on lidar 2:
    nrf_gpio_pin_write(PIN_VL_2_XSHUT, 1);
    // Give it a sec to come up.
    nrf_delay_ms(10);
    // Initialise - basically loads calibration settings and sets it into correct read mode.
    velo_sensor_data.lidar_2 = DEFAULT_VELO_LIDAR_DEVICE(1);
    status = velo_lidar_init(&velo_sensor_data.lidar_2,LIDAR_2_ADDRESS, false );
    if (status == VELO_LIDAR_DISCONNECTED)
    {
      NRF_LOG_INFO("Lidar 2 failed to initialise");
      velo_sensor_data.lidar_2.connected = false;
      velo_status.lidar_disconnected = 1;
    }
    else
    {
      NRF_LOG_INFO("Lidar 2 initialised")
      velo_sensor_data.lidar_2.connected = true;
    }
    velo_sensor_data.lidar_2.last_poll_ms = time_ms();

    velo_status.lidar_init=1;
  }
}

void velo_sensors_uninit_lidar(void)
{
   //Turn off range sensors:
  nrf_gpio_pin_write(PIN_VL_1_XSHUT, 0);
  nrf_gpio_pin_write(PIN_VL_2_XSHUT, 0);
  nrf_gpio_pin_write(PIN_VL_3_XSHUT, 0);
  velo_sensor_data.lidar_1.connected = false;
  velo_sensor_data.lidar_2.connected = false;
  velo_sensor_data.lidar_3.connected = false;
  velo_status.lidar_init=true;
}

// NOTE Uninitialise all sensors, twi, and spi modules:
void velo_sensors_uninit_all(void)
{
  velo_sensors_uninit_lidar();
  if (velo_status.twi_init)
  {
    velo_twi_uninit();
    velo_status.twi_init = false;
  }
}

// NOTE Function to read all sensors.
void velo_sensors_read(void)
{
  uint32_t time;
  if (velo_sensor_data.lidar_1.connected)
  {
    time = time_ms();
    if ((time - velo_sensor_data.lidar_1.last_poll_ms)>LIDAR_POLL_PERIOD_MS)
    {
      velo_sensor_data.lidar_1.last_poll_ms = time;
      velo_lidar_read(&velo_sensor_data.lidar_1);
      if (!velo_sensor_data.lidar_1.connected)
      {
        velo_status.lidar_disconnected=1;
      }
    }
  }

  if (velo_sensor_data.lidar_2.connected)
  {
    time = time_ms();
    if ((time - velo_sensor_data.lidar_2.last_poll_ms)>LIDAR_POLL_PERIOD_MS)
    {
      velo_sensor_data.lidar_2.last_poll_ms = time;
      velo_lidar_read(&velo_sensor_data.lidar_2);
      if (!velo_sensor_data.lidar_2.connected)
      {
        velo_status.lidar_disconnected=1;
      }
    } 
  }

  if (velo_sensor_data.lidar_3.connected)
  {
    time = time_ms();
    if ((time - velo_sensor_data.lidar_3.last_poll_ms)>LIDAR_POLL_PERIOD_MS)
    {
      velo_sensor_data.lidar_3.last_poll_ms = time;
      velo_lidar_read(&velo_sensor_data.lidar_3);
      if (!velo_sensor_data.lidar_3.connected)
      {
        velo_status.lidar_disconnected=1;
      }
    } 
  }

  //Get accelerometer data:
  velo_accel_read(&velo_sensor_data.accel);

  time = time_ms();
  if ((time - velo_sensor_data.battery.last_poll_ms)>BAT_POLL_PERIOD_MS)
  {
    velo_sensor_data.battery.last_poll_ms = time;
    velo_bat_gauge_read(&velo_sensor_data.battery);
    // Check charge status:
    velo_status.pending_actions |= VELO_PENDING_ACTION_CHECK_CHARGE_STATUS;
  }  
}