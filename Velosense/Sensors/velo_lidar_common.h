#ifndef VELO_LIDAR_COMMON_H__
#define VELO_LIDAR_COMMON_H__
typedef enum 
{
  VELO_LIDAR_MEAS_NOT_READY,
  VELO_LIDAR_MEAS_READY,
  VELO_LIDAR_DISCONNECTED
}velo_lidar_status_enum_t;

typedef enum
{
  VELO_LIDAR_TYPE_ST,
  VELO_LIDAR_TYPE_SHARP,
} velo_lidar_type_enum_t;

#endif //VELO_LIDAR_COMMON_H__