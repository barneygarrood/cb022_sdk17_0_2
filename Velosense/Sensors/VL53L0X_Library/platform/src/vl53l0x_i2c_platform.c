#include "nrf_drv_twi.h"
#include "vl53l0x_i2c_platform.h"
#include "vl53l0x_platform.h"
#include "nrf_log.h"
#include "app_util_platform.h"
#include "velo_twi.h"

#define twi_write_transfer velo_twi_write_transfer
#define twi_read_transfer velo_twi_read_transfer
#define twi_perform_transfers velo_twi_perform_transfers
#define twi_read_register_transfer velo_twi_read_register_transfer

/**
 * @brief Writes the supplied byte buffer to the device
 *
 * Wrapper for SystemVerilog Write Multi task
 *
 * @code
 *
 * Example:
 *
 * uint8_t *spad_enables;
 *
 * int status = VL53L0X_write_multi(RET_SPAD_EN_0, spad_enables, 36);
 *
 * @endcode
 *
 * @param  address - uint8_t device address value
 * @param  index - uint8_t register index value
 * @param  pdata - pointer to uint8_t buffer containing the data to be written
 * @param  count - number of bytes in the supplied byte buffer
 *
 * @return status - SystemVerilog status 0 = ok, 1 = error
 *
 */

int32_t VL53L0X_write_multi(uint8_t address, uint8_t index, uint8_t  *pdata, int32_t count)
{
	ret_code_t ret;

	/* device supports only limited number of bytes written in sequence */
	if (count > (VL53L0X_MAX_STRING_LENGTH_PLT))
	{
		return NRF_ERROR_INVALID_LENGTH;
	}

	/* All written data has to be in the same page */
	if ((address / (VL53L0X_MAX_STRING_LENGTH_PLT)) != ((index + count - 1) / (VL53L0X_MAX_STRING_LENGTH_PLT)))
	{
		return NRF_ERROR_INVALID_ADDR;
	}
        uint8_t buffer[1 + VL53L0X_MAX_STRING_LENGTH_PLT]; /* index + data */
        buffer[0] = (uint8_t)index;
        memcpy(buffer + 1, pdata, count);
        //Setup transfer:
        nrf_twi_mngr_transfer_t trans[1];
        twi_write_transfer(address>>1,buffer,count+1,trans);
        ret = twi_perform_transfers(trans,1);
        //ret = nrf_drv_twi_tx(&m_twi_master, address >> 1, buffer, count + 1, false);
	return ret;
}

/**
 * @brief  Reads the requested number of bytes from the device
 *
 * Wrapper for SystemVerilog Read Multi task
 *
 * @code
 *
 * Example:
 *
 * uint8_t buffer[COMMS_BUFFER_SIZE];
 *
 * int status = status  = VL53L0X_read_multi(DEVICE_ID, buffer, 2)
 *
 * @endcode
 *
 * @param  address - uint8_t device address value
 * @param  index - uint8_t register index value
 * @param  pdata - pointer to the uint8_t buffer to store read data
 * @param  count - number of uint8_t's to read
 *
 * @return status - SystemVerilog status 0 = ok, 1 = error
 *
 */

int32_t VL53L0X_read_multi(uint8_t address,  uint8_t index, uint8_t  *pdata, int32_t count)
{
    ret_code_t ret;

    if (count > (VL53L0X_MAX_STRING_LENGTH_PLT))
    {
        return NRF_ERROR_INVALID_LENGTH;
    }
    // Two transfers here - first to set register to read, second to read from it.
    nrf_twi_mngr_transfer_t trans[2];
    twi_read_register_transfer(address>>1, &index, pdata,count,trans);
    // perform transfers.  This is blocking..
    ret = twi_perform_transfers(trans,2);
    return ret;
}

/**
 * @brief  Writes a single byte to the device
 *
 * Wrapper for SystemVerilog Write Byte task
 *
 * @code
 *
 * Example:
 *
 * uint8_t page_number = MAIN_SELECT_PAGE;
 *
 * int status = VL53L0X_write_byte(PAGE_SELECT, page_number);
 *
 * @endcode
 *
 * @param  address - uint8_t device address value
 * @param  index - uint8_t register index value
 * @param  data  - uint8_t data value to write
 *
 * @return status - SystemVerilog status 0 = ok, 1 = error
 *
 */


int32_t VL53L0X_write_byte(uint8_t address,  uint8_t index, uint8_t data)
{
    ret_code_t ret;

    uint8_t buffer[2]; /* Addr + data */
    buffer[0] = index;
    buffer[1] = data;

    nrf_twi_mngr_transfer_t trans[1];
    twi_write_transfer(address>>1,buffer,2,trans);
    ret = twi_perform_transfers(trans,1);

    return ret;

}

/**
 * @brief  Writes a single word (16-bit unsigned) to the device
 *
 * Manages the big-endian nature of the device (first byte written is the MS byte).
 * Uses SystemVerilog Write Multi task.
 *
 * @code
 *
 * Example:
 *
 * uint16_t nvm_ctrl_pulse_width = 0x0004;
 *
 * int status = VL53L0X_write_word(NVM_CTRL__PULSE_WIDTH_MSB, nvm_ctrl_pulse_width);
 *
 * @endcode
 *
 * @param  address - uint8_t device address value
 * @param  index - uint8_t register index value
 * @param  data  - uin16_t data value write
 *
 * @return status - SystemVerilog status 0 = ok, 1 = error
 *
 */

int32_t VL53L0X_write_word(uint8_t address,  uint8_t index, uint16_t  data)
{
    ret_code_t ret;

    uint8_t buffer[3]; /* Addr + data */
    buffer[0] = index;
    buffer[1] = data >> 8;
    buffer[2] = data;

    nrf_twi_mngr_transfer_t trans[1];
    twi_write_transfer(address>>1,buffer,3,trans);
    ret = twi_perform_transfers(trans,1);

    return ret;
}

/**
 * @brief  Writes a single dword (32-bit unsigned) to the device
 *
 * Manages the big-endian nature of the device (first byte written is the MS byte).
 * Uses SystemVerilog Write Multi task.
 *
 * @code
 *
 * Example:
 *
 * uint32_t nvm_data = 0x0004;
 *
 * int status = VL53L0X_write_dword(NVM_CTRL__DATAIN_MMM, nvm_data);
 *
 * @endcode
 *
 * @param  address - uint8_t device address value
 * @param  index - uint8_t register index value
 * @param  data  - uint32_t data value to write
 *
 * @return status - SystemVerilog status 0 = ok, 1 = error
 *
 */

int32_t VL53L0X_write_dword(uint8_t address, uint8_t index, uint32_t  data)
{
    ret_code_t ret;

    uint8_t buffer[5]; /* Addr + data */
    buffer[0] = index;
    buffer[1] = data >> 24;
    buffer[2] = data >> 16;
    buffer[3] = data >> 8;
    buffer[4] = data;

    nrf_twi_mngr_transfer_t trans[1];
    twi_write_transfer(address>>1,buffer,5,trans);
    ret = twi_perform_transfers(trans,1);

    return ret;
}

/**
 * @brief  Reads a single byte from the device
 *
 * Uses SystemVerilog Read Byte task.
 *
 * @code
 *
 * Example:
 *
 * uint8_t device_status = 0;
 *
 * int status = VL53L0X_read_byte(STATUS, &device_status);
 *
 * @endcode
 *
 * @param  address - uint8_t device address value
 * @param  index  - uint8_t register index value
 * @param  pdata  - pointer to uint8_t data value
 *
 * @return status - SystemVerilog status 0 = ok, 1 = error
 *
 */

int32_t VL53L0X_read_byte(uint8_t address,  uint8_t index, uint8_t  *pdata)
{
    ret_code_t ret;

    // Two transfers here - first to set register to read, second to read from it.
    nrf_twi_mngr_transfer_t trans[2];
    twi_read_register_transfer(address>>1, &index, pdata,1,trans);
    // perform transfers.  This is blocking..
    ret = twi_perform_transfers(trans,2);


    return ret;
}

/**
 * @brief  Reads a single word (16-bit unsigned) from the device
 *
 * Manages the big-endian nature of the device (first byte read is the MS byte).
 * Uses SystemVerilog Read Multi task.
 *
 * @code
 *
 * Example:
 *
 * uint16_t timeout = 0;
 *
 * int status = VL53L0X_read_word(TIMEOUT_OVERALL_PERIODS_MSB, &timeout);
 *
 * @endcode
 *
 * @param  address - uint8_t device address value
 * @param  index  - uint8_t register index value
 * @param  pdata  - pointer to uint16_t data value
 *
 * @return status - SystemVerilog status 0 = ok, 1 = error
 *
 */

int32_t VL53L0X_read_word(uint8_t address,  uint8_t index, uint16_t *pdata)
{
    ret_code_t ret;

    uint8_t buffer[2];

    // Two transfers here - first to set register to read, second to read from it.
    nrf_twi_mngr_transfer_t trans[2];
    twi_read_register_transfer(address>>1, &index, buffer,2,trans);

    // perform transfers.  This is blocking..
    ret = twi_perform_transfers(trans,2);
    	
    *pdata = (buffer[0] << 8) + buffer[1];

    return ret;
}

/**
 * @brief  Reads a single dword (32-bit unsigned) from the device
 *
 * Manages the big-endian nature of the device (first byte read is the MS byte).
 * Uses SystemVerilog Read Multi task.
 *
 * @code
 *
 * Example:
 *
 * uint32_t range_1 = 0;
 *
 * int status = VL53L0X_read_dword(RANGE_1_MMM, &range_1);
 *
 * @endcode
 *
 * @param  address - uint8_t device address value
 * @param  index - uint8_t register index value
 * @param  pdata - pointer to uint32_t data value
 *
 * @return status - SystemVerilog status 0 = ok, 1 = error
 *
 */

int32_t VL53L0X_read_dword(uint8_t address, uint8_t index, uint32_t *pdata)
{
    ret_code_t ret;

    uint8_t buffer[4];
    	
    // Two transfers here - first to set register to read, second to read from it.
    nrf_twi_mngr_transfer_t trans[2];
    twi_read_register_transfer(address>>1, &index, buffer,4,trans);

    // perform transfers.  This is blocking..
    ret = twi_perform_transfers(trans,2);
    *pdata = (buffer[0] << 24) + (buffer[1] << 16) + (buffer[2] << 8) + buffer[3];
    return ret;
}


