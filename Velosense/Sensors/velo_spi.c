#include "velo_spi.h"

static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);  /**< SPI instance. */
static volatile bool spi_xfer_done;  /**< Flag used to indicate that SPI instance completed the transfer. */

void velo_spi_event_handler(nrf_drv_spi_evt_t const * p_event, void * p_context)
{
    spi_xfer_done = true;
}

void velo_spi_init()
{
  nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
  spi_config.ss_pin   = PIN_SPI_CS_ACCEL;
  spi_config.miso_pin = PIN_SPI_MISO;
  spi_config.mosi_pin = PIN_SPI_MOSI;
  spi_config.sck_pin  = PIN_SPI_CLK;
  spi_config.mode = NRF_DRV_SPI_MODE_3;
  spi_config.frequency   = NRF_DRV_SPI_FREQ_4M;
  //APP_ERROR_CHECK(nrf_drv_spi_init(&spi, &spi_config, spi_event_handler, NULL));
  ret_code_t err_code = nrf_drv_spi_init(&spi, &spi_config, NULL, NULL);
  APP_ERROR_CHECK(err_code);
 }

ret_code_t velo_spi_txrx(uint8_t * p_tx_buffer, uint8_t tx_count, uint8_t * p_rx_buffer, uint8_t rx_count)
{
  ret_code_t ret;
  spi_xfer_done = false;

  ret = nrf_drv_spi_transfer(&spi,p_tx_buffer,tx_count,p_rx_buffer,rx_count);
//  while (!spi_xfer_done)
//  {
//      __WFE();
//  }
  
  return ret;

}

void velo_spi_uninit()
{
  nrf_drv_spi_uninit(&spi);
}



