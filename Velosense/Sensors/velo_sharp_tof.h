#ifndef VELO_SHARP_TOF_H__
#define VELO_SHARP_TOF_H__

#include <stdint.h>
#include "velo_lidar_common.h"
#include "gp2ap02vt_api.h"

velo_lidar_status_enum_t velo_sharp_tof_setup(GP2AP0XVT_Dev_t * p_dev) ;
velo_lidar_status_enum_t velo_sharp_tof_checkGetMeasurement(GP2AP0XVT_Dev_t * p_dev);


#endif //VELO_SHARP_TOF_H__