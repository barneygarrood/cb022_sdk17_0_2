#include "velo_accel_twi.h"

#include "velo_twi.h"

#include "velo_power.h"
#include "velo_config.h"
#include "velo_us_timer.h"
#include "velo_status.h"
#include "velo_us_timer.h"

#include "app_error.h"

#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "nrf_delay.h"
#include "app_error.h"

#define NRF_LOG_MODULE_NAME velo_accel_twi
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

#define SAMPLE_COUNT  32   // Maximum sample count for accelerometer data.

#define ACCEL_I2C_ADDRESS 0x1D

#define REGISTER_ADDR_DEVICE_ID 0x00
#define REGISTER_ADDR_THRESH_ACT_CTRL 0x24
#define REGISTER_ADDR_ACT_INACT_CTRL 0x27
#define REGISTER_ADDR_BW_RATE_CTRL 0x2C
#define REGISTER_ADDR_POWER_CTRL 0x2D
#define REGISTER_ADDR_INT_ENABLE_CTRL 0x2E
#define REGISTER_ADDR_INT_MAP_CTRL 0x2F
#define REGISTER_ADDR_DATA_FORMAT_CTRL 0x31
#define REGISTER_ADDR_FIFO_CTRL 0x38

#define REGISTER_ADDR_OFSX  0x1E
#define REGISTER_ADDR_OFSY  0x1F
#define REGISTER_ADDR_OFSZ  0x20

#define REGISTER_ADDR_INT_SOURCE 0x30
#define REGISTER_ADDR_FIFO_BUFFER 0x32
#define REGISTER_ADDR_FIFO_STATUS 0x39

#define REGISTER_VALUE_POWER_STANDBY 0x00
#define REGISTER_VALUE_POWER_SLEEP 0x04
#define REGISTER_VALUE_POWER_MEASURE 0x08

#define REGISTER_VALUE_HI_PWR_RATE_3200  0xF
#define REGISTER_VALUE_HI_PWR_RATE_1600  0xE
#define REGISTER_VALUE_HI_PWR_RATE_800  0xD
#define REGISTER_VALUE_HI_PWR_RATE_400  0xC
#define REGISTER_VALUE_HI_PWR_RATE_200  0xB
#define REGISTER_VALUE_HI_PWR_RATE_100  0xA
#define REGISTER_VALUE_HI_PWR_RATE_50  0x9
#define REGISTER_VALUE_HI_PWR_RATE_25  0x8
#define REGISTER_VALUE_HI_PWR_RATE_12_5  0x7
#define REGISTER_VALUE_HI_PWR_RATE_6_25  0x6

#define REGISTER_VALUE_LO_PWR_RATE_400  0xC
#define REGISTER_VALUE_LO_PWR_RATE_200  0xB
#define REGISTER_VALUE_LO_PWR_RATE_100  0xA
#define REGISTER_VALUE_LO_PWR_RATE_50  0x9
#define REGISTER_VALUE_LO_PWR_RATE_25  0x8
#define REGISTER_VALUE_LO_PWR_RATE_12_5  0x7

#define REGISTER_VALUE_DATA_FORMAT_1G 0x00
#define REGISTER_VALUE_DATA_FORMAT_2G 0x01
#define REGISTER_VALUE_DATA_FORMAT_4G 0x02
#define REGISTER_VALUE_DATA_FORMAT_8G 0x03
#define REGISTER_VALUE_DATA_FORMAT_FULL_RES 0x08
#define REGISTER_VALUE_DATA_FORMAT_JUSTIFY 0x04

#define REGISTER_VALUE_INT_ENABLE_NONE 0x00

#define TARE_TIME_MS  2000  // Time in ms over which to tare accelerometers.


static int16_t _accel_data[3*SAMPLE_COUNT];
static uint16_t _accel_index;
static float _accel_tare[3];
static uint32_t _accel_tare_count;
static bool _accel_taring;
static uint32_t t_tare_start;

static void velo_accel_standby_mode()
{
  velo_twi_perform_write_byte_to_register(ACCEL_I2C_ADDRESS, REGISTER_ADDR_POWER_CTRL, REGISTER_VALUE_POWER_STANDBY);
}

void velo_accel_sleep_mode()
{
  velo_twi_perform_write_byte_to_register(ACCEL_I2C_ADDRESS, REGISTER_ADDR_POWER_CTRL, REGISTER_VALUE_POWER_SLEEP);
}

static void velo_accel_measurement_mode()
{
  velo_twi_perform_write_byte_to_register(ACCEL_I2C_ADDRESS, REGISTER_ADDR_POWER_CTRL, REGISTER_VALUE_POWER_MEASURE);
}

void velo_accel_init(velo_accel_data_t * p_accel)
{
  // Clear accelerometer data:
  memset(_accel_data,0,6*SAMPLE_COUNT);
  _accel_taring = false;
  _accel_index = 0;
  
  // Setup accelerometer:
  ret_code_t err_code;
  uint8_t tx_buffer[8];
  uint8_t rx_buffer[8];
  memset(tx_buffer,0,8);
  memset(rx_buffer,0,8);

  // Put device into standby mode:
  velo_accel_standby_mode();

  //Get device id - send read command, single byte, to register 0x00
  velo_twi_perform_read(ACCEL_I2C_ADDRESS,REGISTER_ADDR_DEVICE_ID,rx_buffer,1);
  NRF_LOG_INFO("Accelerometer Device ID = 0x%x",rx_buffer[0]);
  NRF_LOG_FLUSH();

  //Set FIFO to stream mode with 31 samples (10011111)
  // Note as I understand the FIFO buffer can take 32, but will flag watermark at 31 here.
  // Here we are not using the watermark anyway.
  tx_buffer[0]= (uint8_t)((1<<7) | (SAMPLE_COUNT-1));
  velo_twi_perform_write_byte_to_register(ACCEL_I2C_ADDRESS, REGISTER_ADDR_FIFO_CTRL,  tx_buffer[0]);

  // Register 02C = Rate:
  velo_twi_perform_write_byte_to_register(ACCEL_I2C_ADDRESS, REGISTER_ADDR_BW_RATE_CTRL,  REGISTER_VALUE_HI_PWR_RATE_6_25);

  // Set range to +/-16g full res mode, and also invert interupts (active low): register 0x31 => 00101011 = 0x2B (0001011 = 0x0B):
  tx_buffer[0] = REGISTER_VALUE_DATA_FORMAT_2G | REGISTER_VALUE_DATA_FORMAT_FULL_RES;
  velo_twi_perform_write_byte_to_register(ACCEL_I2C_ADDRESS, REGISTER_ADDR_DATA_FORMAT_CTRL,  tx_buffer[0]);

  // Clear interrupts:
  velo_accel_clear_interrupt();

  velo_accel_measurement_mode();
  // reset data to zeros.
  memset(p_accel,0,sizeof(*p_accel));
  NRF_LOG_INFO("Accelerometer initialisation SUCCESS");
}  

void velo_accel_reset_interrupts()
{
  // Disable interrupts for Activity- register 0x2E => 00000000 = 0X00
  velo_twi_perform_write_byte_to_register(ACCEL_I2C_ADDRESS, REGISTER_ADDR_INT_ENABLE_CTRL, REGISTER_VALUE_INT_ENABLE_NONE);

  // Clear interrupt register:
  velo_accel_clear_interrupt();
} 

void velo_accel_setup_activity_interrupt()
{
  //Set inactivity interrupt:
  ret_code_t err_code;
  uint8_t tx_buffer[8];
  uint8_t rx_buffer[8];

  // Activiy threshold:
  uint8_t threshold = ACCEL_INACTIVITY_THRESHOLD * 32;  // ACCEL_ACTIVITY_THRESHOLD In g.  threshold 31.2mg/LSB
  velo_twi_perform_write_byte_to_register(ACCEL_I2C_ADDRESS, REGISTER_ADDR_THRESH_ACT_CTRL, threshold);

  // Activity control - use AC control (means compares acceleration to baseline) and use all 3 axes:
  // 11110000 = 0xF0
  velo_twi_perform_write_byte_to_register(ACCEL_I2C_ADDRESS, REGISTER_ADDR_ACT_INACT_CTRL, 0xF0);

  // Enable interrupt for Activity- register 0x2E => 00010000 = 0X10
  velo_twi_perform_write_byte_to_register(ACCEL_I2C_ADDRESS, REGISTER_ADDR_INT_ENABLE_CTRL, 0x10);

  // Set to use interrupt 1 for all interrupts: register 0x2F => 00000000 = 0x00
  velo_twi_perform_write_byte_to_register(ACCEL_I2C_ADDRESS, REGISTER_ADDR_INT_MAP_CTRL, 0x00);

  // Clear interrupt register:
  velo_accel_clear_interrupt();
}

void velo_accel_clear_tares()
{
  // Set tares to zero:
  for (int i=0;i<3;i++)
  {
    _accel_tare[i]=0;
  }
  velo_twi_perform_write_byte_to_register(ACCEL_I2C_ADDRESS, REGISTER_ADDR_OFSX, 0);
  velo_twi_perform_write_byte_to_register(ACCEL_I2C_ADDRESS, REGISTER_ADDR_OFSY, 0);
  velo_twi_perform_write_byte_to_register(ACCEL_I2C_ADDRESS, REGISTER_ADDR_OFSZ, 0);
}

void velo_accel_start_taring()
{
  NRF_LOG_INFO("Starting tare process");
  velo_accel_clear_tares();
  _accel_tare_count = 0;
  _accel_taring = true;
  t_tare_start = time_ms();
}

void int16_to_byte_array(uint16_t value, uint8_t * p_array)
{
  p_array[0] = value & 0xFF;
  p_array[1] = (value>>8) & 0xFF;
}

void velo_accel_end_taring()
{
  NRF_LOG_INFO("StartingEnding tare process");
  _accel_taring = false;
  int16_t tares[3];
  if (_accel_tare_count>0)
  {
    for (int i=0;i<3;i++)
    {
      _accel_tare[i]/=_accel_tare_count;
      _accel_tare[i]/=512.0f;
      NRF_LOG_INFO("Raw tare %u = "NRF_LOG_FLOAT_MARKER,i,NRF_LOG_FLOAT(_accel_tare[i]));
      // Average, and at same time convert to right units: 0x7F = 1G.
      _accel_tare[i] *= 0x7F;
      NRF_LOG_INFO("Raw tare2 %u = "NRF_LOG_FLOAT_MARKER,i,NRF_LOG_FLOAT(_accel_tare[i]));
      tares[i] = (int16_t) (-_accel_tare[i]);
      NRF_LOG_INFO("Tare %i = "NRF_LOG_FLOAT_MARKER,i,NRF_LOG_FLOAT(tares[i]));
    }
    // Need to offset z tare by gravity:
    tares[2]-=0x7F;
      NRF_LOG_INFO("Tare 2 final = "NRF_LOG_FLOAT_MARKER,NRF_LOG_FLOAT(tares[2]));
    // Send new tare values:
    uint8_t data[2];
    int16_to_byte_array(tares[0],data);
    velo_twi_perform_write_register(ACCEL_I2C_ADDRESS, REGISTER_ADDR_OFSX,data,2);
    int16_to_byte_array(tares[1],data);
    velo_twi_perform_write_register(ACCEL_I2C_ADDRESS, REGISTER_ADDR_OFSY,data,2);
    int16_to_byte_array(tares[2],data);
    velo_twi_perform_write_register(ACCEL_I2C_ADDRESS, REGISTER_ADDR_OFSZ,data,2);
  }
}

void velo_accel_read(velo_accel_data_t * p_accel)
{
  p_accel->in_progress=true;
  ret_code_t err_code;
  uint8_t rx_buffer[6];
  uint8_t sample_count=0;
  int16_t * accel_values;

  //Read FIFO status register:
  velo_twi_perform_read(ACCEL_I2C_ADDRESS,REGISTER_ADDR_FIFO_STATUS,rx_buffer,1);

  sample_count = rx_buffer[0] > SAMPLE_COUNT ? SAMPLE_COUNT : rx_buffer[0];

  // Read data = 6 bytes;  2 for each axis:
  for (int i=0;i<sample_count;i++)
  {
    velo_twi_perform_read(ACCEL_I2C_ADDRESS,REGISTER_ADDR_FIFO_BUFFER,rx_buffer,6);
    if (p_accel->count < SAMPLE_COUNT)
    {
      accel_values = (int16_t *) rx_buffer;
      for (int j=0;j<3;j++)
      {
        _accel_data[p_accel->count*3+j]=accel_values[j];
        if (_accel_taring) _accel_tare[j] += accel_values[j];
      }
      p_accel->count++;
      if(_accel_taring) _accel_tare_count++;
    }
  }
  if (_accel_taring)
  {
    if ((time_ms() - t_tare_start) > TARE_TIME_MS) velo_accel_end_taring();
  }
}

void velo_accel_clear_interrupt()
{
  // Clear interrupts by reading register 0x30:
  //Read FIFO status register:
  uint8_t rx_buffer[1];
  velo_twi_perform_read(ACCEL_I2C_ADDRESS,REGISTER_ADDR_INT_SOURCE,rx_buffer,1);
}

static void reset(velo_accel_data_t * p_accel)
{
  memset(p_accel,0,sizeof(*p_accel));
}

void velo_accel_reset_inactivity(velo_accel_data_t * p_accel)
// Resets inactivity - we need to do this for example when unplugging charger.
{
  velo_status.inactive_warning = false;
  velo_status.inactive = false;
  p_accel->time_inact = time_ms();
}

// Calculate means standard devaition, range etc.  Also control inactivity.
// NOTE This is called from ANT_Zenith, so only when packet being put together.
// NOTE Data could be lost this way.  Need to implement a bigger buffer..
void velo_accel_calc_stats(velo_accel_data_t * p_accel)
{
// NOTE See notebook 11/9/19 - there is a way to calculate standard deviation incrementally, meaning we don't have to 
// store all acceleration values ahead of time.  Consider if running short on RAM.
  // Get data:
  uint16_t count = p_accel->count;
  // Bail out if no samples:
  if (count == 0) return;

  // Decrement counter by number of samples read:
  // Cast to 16 bit values:
  int16_t * accel_data = _accel_data;

  // Reset values:
  p_accel->accel_x_sum=0;
  p_accel->accel_y_sum=0;
  p_accel->accel_z_sum=0;
  p_accel->accel_mag_max=0;
  float accel_mag;
  for (int i=0;i<count;i++)
  {
    accel_mag=sqrt( accel_data[i*3] * accel_data[i*3] + 
                              accel_data[i*3+1] * accel_data[i*3+1] + 
                              accel_data[i*3+2] * accel_data[i*3+2] ); 
    if (accel_mag > p_accel->accel_mag_max) p_accel->accel_mag_max = accel_mag;

    p_accel->accel_x_sum += (float)accel_data[i*3];
    p_accel->accel_y_sum += (float)accel_data[i*3+1];
    p_accel->accel_z_sum += (float)accel_data[i*3+2];
  }
  p_accel->accel_x_mean = (float)p_accel->accel_x_sum / (float)p_accel->count;
  p_accel->accel_y_mean = p_accel->accel_y_sum / p_accel->count;
  p_accel->accel_z_mean = p_accel->accel_z_sum / p_accel->count;

  // Deal with inactivity:
  int32_t time = time_ms();
  if ((fabs(p_accel->accel_x_mean - p_accel->x_accel_inact) > ACCEL_INACTIVITY_THRESHOLD) | 
        (fabs(p_accel->accel_y_mean - p_accel->y_accel_inact) > ACCEL_INACTIVITY_THRESHOLD) |
        (fabs(p_accel->accel_z_mean - p_accel->z_accel_inact) > ACCEL_INACTIVITY_THRESHOLD))
  {
    p_accel->x_accel_inact = p_accel->accel_x_mean;
    p_accel->y_accel_inact = p_accel->accel_y_mean;
    p_accel->z_accel_inact = p_accel->accel_z_mean;
    p_accel->time_inact = time;
    if (velo_status.inactive_warning)
    {
      velo_status.inactive_warning = false;
      velo_status.dirty = true;
    }
  }
  // Reset:
  p_accel->count = 0;
#if INACTIVITY_ACTIVE
  else
  {
//  uint16_t tg = time-p_accel->time_inact;
//  uint16_t test = INACTIVITY_SHUTDOWN_TIMEOUT*1000;
//  if (tg>test)
//  {
//    int x = 0;
//  }
    if ((time-p_accel->time_inact)>INACTIVITY_SHUTDOWN_TIMEOUT*1000)
    {
      velo_status.inactive = true;
      velo_status.dirty = true;
    }
    else if ((time-p_accel->time_inact)>INACTIVITY_WARNING_TIMEOUT*1000)
    {
      if (!velo_status.inactive_warning)
      {
        velo_status.inactive_warning = true;
        velo_status.dirty = true;
      }
    }
  }
#endif
}