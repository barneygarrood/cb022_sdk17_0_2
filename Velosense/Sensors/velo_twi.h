#ifndef VELO_TWI_H__
#define VELO_TWI_H__

#include <stdio.h>
#include "nrf_twi_mngr.h"


/* TWI instance ID. */
#define TWI_INSTANCE_ID_0     0
#define MAX_PENDING_TRANSACTIONS    5

extern uint32_t velo_twi_event_time[];
extern uint8_t velo_twi_status[];

    
int32_t velo_twi_init();
void velo_twi_uninit();
void velo_twi_schedule_transfers(nrf_twi_mngr_transfer_t * p_trans, uint8_t transfer_count, nrf_twi_mngr_callback_t cb_function);
int32_t velo_twi_perform_transfers(nrf_twi_mngr_transfer_t * p_trans, uint8_t trans_count);

void velo_twi_write_transfer(uint8_t twi_address, uint8_t * p_data, uint8_t data_len, nrf_twi_mngr_transfer_t * p_trans);
void velo_twi_read_transfer(uint8_t twi_address, uint8_t * p_data, uint8_t data_len, nrf_twi_mngr_transfer_t * p_trans);
void velo_twi_read_register_transfer(uint8_t twi_address, uint8_t * p_reg_addr, uint8_t * p_data, uint8_t data_len, nrf_twi_mngr_transfer_t * p_trans);

int32_t velo_twi_perform_write_register(uint8_t twi_address, uint8_t reg_addr, uint8_t * p_data, uint8_t data_len);
int32_t velo_twi_perform_write_byte_to_register(uint8_t twi_address, uint8_t reg_addr, uint8_t byte);

int32_t velo_twi_perform_read(uint8_t twi_address, uint8_t reg_addr, uint8_t * p_data, uint8_t data_len);

#endif //VELO_TWI_H__
