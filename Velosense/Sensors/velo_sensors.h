#ifndef VELO_SENSORS_H__
#define VELO_SENSORS_H__

#include "velo_bat.h"
#include "velo_accel_twi.h"
#include "velo_lidar.h"

typedef struct 
{
  velo_lidar_device_t lidar_1;
  velo_lidar_device_t lidar_2; 
  velo_lidar_device_t lidar_3; 
  velo_accel_data_t accel;
  velo_bat_t battery;

} velo_sensor_data_t;

extern velo_sensor_data_t velo_sensor_data;

void velo_sensors_init_twi();
void velo_sensors_init_bat();
void velo_sensors_init(bool init_lidar);
void velo_sensors_uninit_lidar(void);
void velo_sensors_uninit_all(void);
void velo_sensors_uninit();
void velo_sensors_read();


void velo_sensors_accel_process();
#endif //VELO_SENSORS_H__