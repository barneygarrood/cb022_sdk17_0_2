#ifndef BAT_LEVEL_H__
#define BAT_LEVEL_H__

#include "velo_twi.h"

#define BAT_GAUGE_ADDRESS 0x55
#define BAT_GAUGE_TWI_INSTANCE 1

typedef struct 
{
  uint8_t soc;
  uint16_t voltage;
  uint32_t last_poll_ms;
} velo_bat_t;

typedef enum
{
  VELO_BAT_STATE_SUB_CRITICAL = 0,
  VELO_BAT_STATE_CRITICAL = 1,
  VELO_BAT_STATE_OK = 2,
} velo_bat_status_enum_t;

velo_bat_status_enum_t velo_bat_get_status(velo_bat_t * p_battery);
void velo_bat_init(velo_bat_t * battery);
void bat_gauge_get_soc_transfer(uint8_t * p_data, nrf_twi_mngr_transfer_t * p_trans);
void velo_bat_gauge_get_voltage_transfer(uint8_t * p_data, nrf_twi_mngr_transfer_t * p_trans);
void velo_bat_gauge_enter_shutdown_transfer(nrf_twi_mngr_transfer_t * p_trans);
int32_t velo_bat_gauge_read(velo_bat_t * battery);
void velo_bat_gauge_init();

#endif //BAT_LEVEL_H__

