#include "velo_st_tof.h"
#include "vl53l0x_api.h"
#include "vl53l0x_i2c_platform.h"
#include "velo_lidar_common.h"
#include "nrf_error.h"
#include "nrf_log_ctrl.h"
#include "velo_config.h"

#define NRF_LOG_MODULE_NAME velo_st_tof
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

static void print_pal_error(VL53L0X_Error Status){
	if(Status != 0){
		char buf[VL53L0X_MAX_STRING_LENGTH];
		VL53L0X_GetPalErrorString(Status, buf);
		NRF_LOG_DEBUG("API Status: %i : %s", (uint32_t)Status, *buf);
	}
}

static velo_lidar_status_enum_t isMeasurementReady(VL53L0X_Dev_t * p_lidar){
	uint8_t device_ready;

  VL53L0X_Error err_code = VL53L0X_ERROR_NONE;
  err_code = VL53L0X_GetMeasurementDataReady(p_lidar, &device_ready);
  
  if (err_code != VL53L0X_ERROR_NONE)
  {
    return VELO_LIDAR_DISCONNECTED;
  }
  else if (device_ready)
  {
    return VELO_LIDAR_MEAS_READY;
  }
	else
  {
    return VELO_LIDAR_MEAS_NOT_READY;
  }
}

ret_code_t _do_settings(VL53L0X_Dev_t * p_lidar, float rate, long timing_budget, long sigma, long pre_period, long post_period)
{
  ret_code_t err_code = VL53L0X_ERROR_NONE;
  // Device settings:
  // Signal rate, in MCPS (mega counts per second?), lower limit for number of counts required for valid measurement.
  // Default 0.25
    VL53L0X_SetDeviceMode(p_lidar, VL53L0X_DEVICEMODE_SINGLE_RANGING);
    if (err_code == VL53L0X_ERROR_NONE) {
            err_code = VL53L0X_SetLimitCheckValue(p_lidar,
                            VL53L0X_CHECKENABLE_SIGNAL_RATE_FINAL_RANGE,
                            (FixPoint1616_t) (rate * 65536));
    }
    // Sigma - don't fully understand this, but higher value = lower standrad, more valids.
    // Default = 32
    if (err_code == VL53L0X_ERROR_NONE) {
            err_code = VL53L0X_SetLimitCheckValue(p_lidar,
                            VL53L0X_CHECKENABLE_SIGMA_FINAL_RANGE,
                            (FixPoint1616_t) (sigma
                             * 65536));
    }

    // Default 30ms
    if (err_code == VL53L0X_ERROR_NONE) 
    {
            err_code = VL53L0X_SetMeasurementTimingBudgetMicroSeconds(p_lidar,
                            timing_budget);
    }
    // default 14;
    if (err_code == VL53L0X_ERROR_NONE) 
    {
        err_code = VL53L0X_SetVcselPulsePeriod(p_lidar,
        VL53L0X_VCSEL_PERIOD_PRE_RANGE, pre_period);
    }
    // default 10
    if (err_code == VL53L0X_ERROR_NONE) 
    {
        err_code = VL53L0X_SetVcselPulsePeriod(p_lidar,
        VL53L0X_VCSEL_PERIOD_FINAL_RANGE, post_period);
    }
}

velo_lidar_status_enum_t velo_st_tof_setup(uint8_t id, VL53L0X_Dev_t * p_lidar, uint8_t address)
{
  // #################################################################################################################
  // Initilise range sensor:
  // #################################################################################################################
  // Initialize Comms
  p_lidar->I2cDevAddr      =  LIDAR_ORIG_ADDRESS;
  p_lidar->comms_type      =  1;
  p_lidar->comms_speed_khz =  400;

  VL53L0X_Version_t                   Version;
  VL53L0X_Version_t                  *pVersion   = &Version;
  uint8_t deviceRevisionMajor;
  uint8_t deviceRevisionMinor;

  uint8_t vhvCalibrationValue;
  uint8_t phaseCalibrationValue;
  ret_code_t err_code = VL53L0X_ERROR_NONE;
  if(VL53L0X_ERROR_NONE == err_code){

    err_code = VL53L0X_DataInit(p_lidar);
    if(err_code != VL53L0X_ERROR_NONE) 
    {
      NRF_LOG_ERROR("DataInit error code: %d on lidar addr %u\r\n", err_code,p_lidar->I2cDevAddr);
      return VELO_LIDAR_DISCONNECTED;
    }

  // ####################################################################
  // sandbox - changing device address
  // NOTE This changes i2c device address.  
  // Use the following line to switch off other devices so you dont change all at once.
  // Of course leave the ones whose address you have already set on. 
  // Turning off resets the device address.
  //  nrf_gpio_pin_write(PIN_VL_XSHUT, 0);
    int8_t status =VL53L0X_SetDeviceAddress(p_lidar,address);
    // If that failed then the device isn't physically connected, so set flag accordingly.
    if (status != 0)
    {
      p_lidar->I2cDevAddr = 0;
      return VELO_LIDAR_DISCONNECTED;
    }
    else
    {
      p_lidar->I2cDevAddr = address;
    }
  // ####################################################################

    uint16_t osc_calibrate_val=0;
    err_code = VL53L0X_RdWord(p_lidar, VL53L0X_REG_OSC_CALIBRATE_VAL,&osc_calibrate_val);
    if (err_code!=0)
    {
      return VELO_LIDAR_DISCONNECTED;
    }
    err_code = VL53L0X_StaticInit(p_lidar);
    if(VL53L0X_ERROR_NONE != err_code) NRF_LOG_ERROR("StaticInit: %d", err_code);

    uint32_t refSpadCount;
    uint8_t isApertureSpads;

    VL53L0X_PerformRefSpadManagement(p_lidar, &refSpadCount, &isApertureSpads);
    if(VL53L0X_ERROR_NONE != err_code) 
    {
            NRF_LOG_ERROR("SpadCal: %d\r\n", err_code)
    } else 
    {
            NRF_LOG_DEBUG("refSpadCount: %d\r\nisApertureSpads: %d", refSpadCount,isApertureSpads)
    }

    err_code = VL53L0X_PerformRefCalibration(p_lidar, &vhvCalibrationValue, &phaseCalibrationValue);
    print_pal_error(err_code);
    NRF_LOG_DEBUG("Calibration Values:\r\n VHV:%d phaseCal:%d", vhvCalibrationValue, phaseCalibrationValue);

    err_code = VL53L0X_GetVersion(pVersion);
    print_pal_error(err_code);
    NRF_LOG_INFO("VL53L0X API Version: %d.%d.%d (revision %d)", pVersion->major, pVersion->minor ,pVersion->build, pVersion->revision);

    err_code = VL53L0X_GetProductRevision(p_lidar, &deviceRevisionMajor, &deviceRevisionMinor);

    if(VL53L0X_ERROR_NONE == err_code)
    {
            NRF_LOG_INFO("VL53L0X product revision: %d.%d", deviceRevisionMajor, deviceRevisionMinor);
    } else {
            NRF_LOG_ERROR("Version Retrieval Failed.  ");
            print_pal_error(err_code);
    }
    switch (id)
    {
      case 0:
      case 1:
      case 2:
        //err_code = _do_settings(p_lidar, 0.1, 200000,60,18,14);
        err_code = _do_settings(p_lidar, 0.1, 30000,60,14,10);
        break;
      
    }

  } else 
  {
      NRF_LOG_ERROR("Data Init Failed.");
  }
  NRF_LOG_FLUSH();
  //Start a measurement:
  VL53L0X_StartMeasurement(p_lidar);
  return VELO_LIDAR_MEAS_NOT_READY;
}

velo_lidar_status_enum_t velo_st_tof_checkGetMeasurement(VL53L0X_Dev_t * p_device)
{
  //NRF_LOG_INFO("Interrogating Lidar %u",p_lidar->device_id);
  // Check measurement is ready:
  velo_lidar_status_enum_t ready = isMeasurementReady(p_device);
  if (ready == VELO_LIDAR_MEAS_READY)
  {
    VL53L0X_RangingMeasurementData_t rangingData;
    //NRF_LOG_INFO("Reading lidar %u..",p_lidar->device_id);
    VL53L0X_GetRangingMeasurementData(p_device, &rangingData);
    // NOTE Subtract 50mm:
    p_device->Data.range_mm = rangingData.RangeMilliMeter;
    p_device->Data.range_status = rangingData.RangeStatus;
    VL53L0X_ClearInterruptMask(p_device, 0);
    // Addd data to registers:
    //_add_new_data(p_lidar,rangingData.RangeMilliMeter,rangingData.RangeStatus);
    //Start a new measurement:
    VL53L0X_StartMeasurement(p_device);
  }
  return ready;
}