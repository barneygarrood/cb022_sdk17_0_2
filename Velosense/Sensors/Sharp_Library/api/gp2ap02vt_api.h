#ifndef __GP2AP02VT_API_H__
#define __GP2AP02VT_API_H__

#ifdef __KERNEL__ 
#include <linux/types.h>
#include <linux/delay.h>
#include <linux/of_gpio.h>
#include "gp2ap02vt.h"
#endif

/* software version */
#define SOFT_REVISION                "gp2ap02vt_v402"

/* device id */
#define CHIP_ID                       0x2F

/* Slave Address */
#define SLAVE_ADDR_MAIN               0x29

/* operation mode */
#define SHORT_RANGE_MODE              0
#define LONG_RANGE_MODE               1


#ifdef __KERNEL__
#define DELAY_FUNC(value) msleep(value)
extern int gp2ap_write_data_file(int8_t *, int32_t);
extern int gp2ap_read_data_from_file(int8_t *);
#endif

#ifdef PIC18F26K22
#define DELAY_FUNC(value) delay_ms(value)
#endif

#ifdef ARDUINO_UNO
#define DELAY_FUNC(value) delay(value)
#endif

#ifdef STM32 
#define DELAY_FUNC(period) HAL_Delay(period)
#endif

#ifdef NRF52840_XXAA
#include "nrf_delay.h"
#define  DELAY_FUNC(period) nrf_delay_ms(period)
#endif

/* status bit */
#define FLAG_BIT                      0x02
#define WAF_BIT                       0x10
#define WAF_BIT_MASK                  0xEF
#define NEAR_BIT_MASK                 0xBF
#define LOW_SN_BIT                    0x04 
#define LOW_SN_BIT_MASK               0xFB 
#define LOW_SIG_BIT                   0x02 
#define LOW_SIG_BIT_MASK              0xFD 
#define HXTALK_BIT                    0x80
#define HXTALK_BIT_MASK               0x7F

#define EIGHTTIMES                    8 
#define SIGAMBRATIO                 2

/* error status */
#define VALID_DATA                    0
#define ERROR_CALCULATION             0x20


#define INITIALIZATION_PASSED         0 
#define INITIALIZATION_FAILED         -1 

#define MEASUREMENT_DISCONNECTED       2
#define MEASUREMENT_ENDED             1
#define MEASUREMENT_NOT_END           0 
/* mode change */
#define LONG2SHORT_RANGE              80
#define SHORT2LONG_RANGE              100

/* factory calibration */
#define FACTORY_CALIBRATION_DONE      0x03
#define CALIBRATION_PASSED            0 
#define CALIBRATION_FAILED            -1 
#define TRIMMING_PASSED               0 
#define TRIMMING_FAILED               -1 
#define DISTANCE_OFFSET_CALIB100MM    100
#define XTALK_MAXIMUM_CALIB           0x049A /* 0.00625 */
#define XTALK_CALIB_SETTING           0x0C00

#define RANGEMINUS100MM               -100
#define RANGE0MM                      0
#define RANGE80MM                     80
#define RANGE100MM                    100
#define RANGE300MM                    300
#define RANGE500MM                    500
#define RANGE550MM                    550
#define RANGE80MM_x1000               (int32_t)80000
#define WAF_RETURN_TH9                9   
#define WAF_RETURN_TH40               40  
#define WAF_RETURN_TH109              109 
#define WAF_RETURN_TH5000             5000
#define WAF_RETURN_TH3500             3500
#define WAF_RETURN_TH2000             2000
#define WAF_COEF1                     -10
#define WAF_COEF2                     -5
#define WAF_COEF3                     -2
#define RETURN_SIG_TH9                9
#define RETURN_SIG_TH40               40
#define RETURN_SIG_TH500              500
#define WAF_LOWSIG_TH 8

#define RANGEOFFSETMAX                75 
#define RANGEOFFSETMIN                -75
#define NUMMEASUREMENTS               20
#define NUMMEASUREMENTS5              5 

#define NUMMEASUREMENTSMEDIAN         5

#define DRANGE 20

/* FLOATING FIXED CONVERSION */
#define FLOAT14BIT_EXP_BIAS       12
#define FLOAT_TO_FIXED16P_EXP_MAX 14
#define FLOAT14BIT_SIGN_BIT       0x2000
#define FLOAT14BIT_EXP_BIT        0x1F00
#define FLOAT14BIT_MANTISSA_BIT   0x00FF
#define FLOAT14BIT_HIDDEN_BIT     0x0100
#define FLOAT8BIT_SIGN_MASK       0x1FFF
#define FIXED28P_DEC              16
#define FIXED16P_28P              4096 //=65536/16

#define FIXED_MSB                 32
#define FIXED16P_BIAS             16
#define MANTISSA_BIT_SIZE         8
#define EXP_BIT_SIZE              5
#define FLP8BTI_MANTISSA_SIZE     3
#define FIXED28P_ROUNDINGBIT      0x08

//Register
#define REG_00H               0x00
#define REG_01H               0x01
#define REG_02H               0x02
#define REG_03H               0x03
#define REG_04H               0x04
#define REG_05H               0x05
#define REG_06H               0x06
#define REG_07H               0x07
#define REG_0DH               0x0D
#define REG_0EH               0x0E
#define REG_0FH               0x0F
#define REG_10H               0x10
#define REG_11H               0x11
#define REG_12H               0x12
#define REG_13H               0x13
#define REG_14H               0x14
#define REG_15H               0x15
#define REG_16H               0x16
#define REG_17H               0x17
#define REG_18H               0x18
#define REG_1AH               0x1A
#define REG_1BH               0x1B
#define REG_1CH               0x1C
#define REG_1DH               0x1D
#define REG_1EH               0x1E
#define REG_1FH               0x1F
#define REG_28H               0x28
#define REG_2AH               0x2A
#define REG_2BH               0x2B
#define REG_2CH               0x2C
#define REG_2EH               0x2E
#define REG_2FH               0x2F
#define REG_38H               0x38
#define REG_41H               0x41
#define REG_43H               0x43
#define REG_45H               0x45
#define REG_47H               0x47
#define REG_4AH               0x4A
#define REG_4DH               0x4D
#define REG_4EH               0x4E
#define REG_50H               0x50
#define REG_54H               0x54
#define REG_55H               0x55
#define REG_56H               0x56
#define REG_39D               39
#define REG_88D               88
#define REG_89D               89
#define REG_100D              100
#define REG_102D              102
#define REG_103D              103
#define REG_106D              106
#define REG_108D              108
#define REG_109D              109

#define SHUTDOWN              0x00 
#define ACTIVE                0x80 
#define EXTPIN_DISABLE        0x00 
#define OUTPUT_FLAG_EVERYTIME 0x34 
#define INTTYPE_LEVEL         0x00 

#define IDRV_LONG             0x0F
#define IDRV_SHORT            0x07

#define SPAD_AREA_0 0x3E
#define SPAD_AREA_24 0x2B

#define MEASUREMENT_TIME_NORMAL 0x03 // 13.51msec 

#define SOFTWARE_RESET        0x01  
#define MULTI_SLAVE           0x80
#define CLEAR_FLAG            0x0D 

#define NORMALAUTOVHV         0x60

#define REG_03H_SETTING       0x00
#define REG_04H_SETTING       0x00
#define REG_05H_SETTING       0x0C
#define REG_07H_SETTING       0xDF
#define REG_0DH_SETTING       0x82
#define REG_0EH_SETTING       0x02
#define REG_11H_SETTING       0x98 // 03vt
#define REG_12H_SETTING       0x00
#define REG_13H_SETTING       0x15
#define REG_14H_SETTING_D     0x14 // 20
#define REG_14H_SETTING_P     0x28 // 40  // 02vt

#define REG_15H_SETTING       0x01
#define REG_1AH_SETTING       0xAA
#define REG_1BH_SETTING       0xAA
#define REG_1CH_SETTING       0xAA
#define REG_1DH_SETTING       0xAA
#define REG_1EH_SETTING       0xF4
#define REG_1EH_SETTING0XF4   0xF4
#define REG_1EH_SETTING0XF8   0xF8
#define REG_1EH_SETTING0XFF   0xFF
#define REG_1FH_SETTING       0xF7
#define REG_28H_SETTING       0x84
#define REG_2AH_SETTING       0x0B
#define REG_2BH_SETTING       0x03
#define REG_43H_SETTING     0xD0
#define REG_45H_SETTING       0x04
#define REG_47H_SETTING_D     0x00
#define REG_47H_SETTING_P     0x20
#define REG_4DH_SETTING       0x06

#define ERROR_RESULT          8888
#define BITSHIFT14            14
#define BITSHIFT8             8
#define BITSHIFT5             5
#define BITSHIFT4             4
#define BITSHIFT3             3
#define BITSHIFT1             1
#define BIT14                 14
#define MASK_LSB              0x00FF
#define MASK_MSB              0xFF00

/* i2c:400kHz only */
#define MEASUREMENT_TIME33M   33 

#define WORD_0X37             0x37
#define WORD_120              120
#define WORD_14               14
#define WORD_8                8
#define RET_SIGNAL            0x00
#define REF_SIGNAL            0x80
#define BIN2                  2

#define INITIAL_SETTING       0x00
#define MASK_TRIMMING_PLL     0x78

#define XTALK_UPDATE_OFF      0x44
#define XTALK_UPDATE_ON       0x04

/* no overflow for 16bit */
#define RANGECOEF35         (int32_t)35
#define RANGECOEF442        (int32_t)442
#define RANGECOEF14400      (int32_t)14400
#define RANGECOEF180        (int32_t)180
#define RANGECOEF80         (int32_t)80


#define TEN                   10
#define ONEHUNDRED            100
#define ONETHOUSAND           1000
#define TENTHOUSAND           10000
#define RANGECOEF8            8
#define RANGECOEF15           15
#define RANGECOEF25           25
#define RANGECOEF75           75
#define RANGECOEF150          150
#define RANGECOEF350          350
#define ROUNDOFF500           500
#define COEFF_AMBTOFRATE40    40

#define PARA_Y1_L              10000
#define PARA_Y2_L              3600
#define FINE_REF_SLOPE_COEF1_L 142
#define FINE_REF_SLOPE_COEF2_L 3110000
#define FINE_REF_SLOPE_COEF3_L 1500
#define FINE_REF_COR_COEF1_L   161000
#define FINE_REF_COR_COEF2_L   866
#define FINE_REF_COR_COEF3_L   175
#define FINE_REF_COR_COEF4_L   302
#define FINE_REF_COR_COEF5_L   490

#define FRATE_TH1_L            1500
#define FRATE_TH2_L            1200
#define FRATE_COEF_A_L         228
#define FRATE_COEF_B_L         2000
#define FRATE_COEF_C_L         1200 
#define FRATE_COEF_D_L         52200

#define PARA_S_JUDG_L          8540
#define PARA_S_NEAR1_L         427
#define PARA_S_NEAR2_L         4310
#define PARA_S_NEAR3_L         8540
#define PARA_S_NEAR4_L         88

#define PARA_S_FAR1_L          3340
#define PARA_S_FAR2_L          5190

#define PARA_Y1_S              13000           
#define PARA_Y2_S              2800
#define FINE_REF_SLOPE_COEF1_S 209
#define FINE_REF_SLOPE_COEF2_S 1680000
#define FINE_REF_SLOPE_COEF3_S 3550
#define FINE_REF_COR_COEF1_S   173000
#define FINE_REF_COR_COEF2_S   1110
#define FINE_REF_COR_COEF3_S   312
#define FINE_REF_COR_COEF4_S   384
#define FINE_REF_COR_COEF5_S   690

#define TEMP_COLLECT1        850
#define TEMP_COLLECT2        1030

#define FRATE_TH1_S            1500
#define FRATE_TH2_S            1030
#define FRATE_COEF_A_S         110
#define FRATE_COEF_B_S         1600
#define FRATE_COEF_C_S         1030
#define FRATE_COEF_D_S         51844

#define PARA_S_NEAR1_S         750
#define PARA_S_NEAR2_S         1230
#define PARA_S_NEAR3_S         13900
#define PARA_S_NEAR4_S         2240

#define COLLECT_F_COEF1      5151
#define COLLECT_F_COEF2      2147
#define COLLECT_F_COEF3      15700

#define XTALK_TH_1            655 /* 0.01*65536 */
#define XTALK_TH_2            557 /* 0.01*65536*0.85*/
#define XTALK_DELTA_MAX       200
#define MAXCNT_COUNT          350208
#define MAXCNT_COUNT_TH       19000

#define RETURNSIGMAX          526336
#define RANGEZEROTH2900       2900
#define FRATE_FIX28PTH        0x8 //0.5(=8/16)


#define SUMREFTH52500         52500
#define SUMREFTH47500         47500
#define SUMREFTH42500         42500
#define SUMREFTH37500         37500
#define SUMREFTH32500         32500
#define SUMREFTH27500         27500
#define SUMREFTH22500         22500
#define SUMREFTH17500         17500  
#define SUMREFTH12500         12500


#define XTALKMINVALUE0108     0x0108
#define XTALKMINVALUE0066     0x0066
#define XTALKMINVALUE0123     0x0123
#define XTALKMINVALUE0070     0x0070
#define XTALKMINVALUE0143     0x0143
#define XTALKMINVALUE007C     0x007C
#define XTALKMINVALUE016C     0x016C
#define XTALKMINVALUE008C     0x008C
#define XTALKMINVALUE01A0     0x01A0
#define XTALKMINVALUE00A0     0x00A0
#define XTALKMINVALUE01E5     0x01E5
#define XTALKMINVALUE00BA     0x00BA
#define XTALKMINVALUE0223     0x0223
#define XTALKMINVALUE00E0     0x00E0
#define XTALKMINVALUE026C     0x026C
#define XTALKMINVALUE0118     0x0118
#define XTALKMINVALUE02E5     0x02E5
#define XTALKMINVALUE0175     0x0175
#define XTALKMINVALUE036C     0x036C
#define XTALKMINVALUE0218     0x0218

/**
 * sensor data
 */
typedef struct {
    uint8_t device_id; /*!< device id */

    int16_t range1; /*!< range1 mm */
    int16_t range2; /*!< range2 mm for using error judegement inside ic */
    int16_t range1_pre; /*!< previous distance value */
    int16_t range1_ave; /*!< average of range1 mm */
    int16_t range2_ave; /*!< average of range2 mm */
    uint8_t range1_status; /*!< measurement status1 */
    uint8_t range1_status_pre; /*!< previous status1 */
    uint8_t range1_status_or; /*!< range1 status for calibration */
    uint8_t flag_update; /*!< error update timing */
    uint8_t num_median1; /*!< number of median */
    int16_t range1_median[NUMMEASUREMENTSMEDIAN]; /*< median variable */
    int16_t range1_raw; /* < range1 mm for checking value before error processing */
    uint8_t range1_status_raw; /* < measurment status1 for checking value before error processing */
    uint16_t error_handling_num; /*!< error handling number */
    uint8_t flag_short2long_lowsig;
    uint8_t flag_comp_rate;

    uint8_t vspad; /*!< ic setting */
    uint8_t vs1; /*!< ic setting */
    uint8_t vcsel_current;
    uint8_t mode_change;
    uint8_t prst_hxtalk;
    uint8_t spad_area;
    
    /* calibration */
    int8_t factory_calibrated;
    int8_t offset_calibration; /*!< flag of offset calibration  */
    int8_t xtalk_calibration; /*!< flag of crosstalk calibration */

    /* offset calibration */
    int16_t offset_target;
    int8_t offset_short1; /*!< offset for short range mode 1 */
    int8_t offset_short2; /*!< offset for short range mode 2 */
    int8_t offset_long1; /*!< offset for long range mode 1 */
    int8_t offset_long2; /*!< offset for long range mode 2 */

    /* crosstalk calibration */
    int32_t data_xtalk; /*!< crosstalk data */
    int32_t data_xtalk_pre; /*!< crosstalk data */
    int32_t data_xtalk_factory; /*!< crosstalk data_factory */
    int32_t data_xtalk_ih; /*!< crosstalk data */
    int32_t data_xtalk_il; /*!< crosstalk data */
    uint8_t num_xtalk_data; /*!< number of average for xtalk calib */
    int32_t data_xtalk_ave; /*!< average data */
    uint8_t reg1Eh;

    /* temporary variable for distance calculation */
    int8_t offset_internal; /*!< offset of sensor  */
    int8_t pll;
    int32_t ret_sig; /*!< return signal */
    int32_t ref_sig; /*!< reference signal */
    int32_t ret_amb; /*!< return signal */
    int32_t sum_sig2; /*!< return waf signal */
    uint8_t maxbin_ret;
    uint8_t maxbin_ref;
    int32_t fine_ret_fx16p;
    int32_t fine_ref_fx16p;
    int32_t frate_fx28p;
    int32_t hist39binfp;
    int32_t pw_ref_fx16p;
    int32_t rethistmax_fx28p;
    int32_t ret_maxcnt_fx28p;
    int32_t refhist_fx28p[6];
    int32_t pw_ref3;
    int32_t frate_sig;
    int32_t fine_ref_cor4;
    int32_t delta_bin;
    int32_t pw_ret;
    int32_t fine_ref_slope_x;
    int32_t collect_f;
    int32_t correction_slop;
    int32_t amb_fx28p;
    int32_t amb_ref_fx28p;
    int32_t sum_sig_fx28p;
    int32_t sum_ref_fx28p;
    int32_t hist_ref39_fx28p;
    uint8_t d_range;
  // Following variables shared between sharp and st devices to make consolidation easier.
  uint16_t range_mm;
  uint8_t range_status;
} GP2AP0XVT_Data_t;

typedef struct {
    uint8_t slave_addr;

#ifdef __KERNEL__
    struct mutex mutex;
    struct i2c_client *client;

    struct input_dev *range_input_dev;
    struct delayed_work range_work;
    struct work_struct range_int_work;
    uint8_t range_enabled;
    int range_delay;
    int range_gpio;
    int range_irq;
    int irq_flags;

    int gp2ap02vt_opened;
    uint8_t flag_read_calibration_file;

    enum of_gpio_flags irq_gpio_flags;

#endif

    uint8_t measurement_mode;

    uint8_t debug;
    GP2AP0XVT_Data_t data;

} GP2AP0XVT_Dev_t;

#define GP2AP0XVT_DevDataGet(Dev) &(Dev->data);

#ifdef __KERNEL__ 
extern int gp2ap_i2c_write(GP2AP0XVT_Dev_t *, uint8_t, uint8_t *, uint8_t);
extern int gp2ap_i2c_read(GP2AP0XVT_Dev_t *, uint8_t, uint8_t *, uint8_t);
extern int gp2ap_read_data_from_file(int8_t *);
#endif

#ifndef __KERNEL__ 
//void gp2ap_i2c_write(GP2AP0XVT_Dev_t *, uint8_t, uint8_t *, uint8_t);
//void gp2ap_i2c_read(GP2AP0XVT_Dev_t *, uint8_t, uint8_t *, uint8_t);
#endif

int32_t FloatingToFixed16p(int32_t, int8_t);
void GetMedianRange(GP2AP0XVT_Data_t *);
void ClearMedianRange(GP2AP0XVT_Data_t *);
void InitParams(GP2AP0XVT_Data_t *);
int8_t InitData(GP2AP0XVT_Dev_t *, uint8_t);
int8_t InitSetting(GP2AP0XVT_Dev_t *, uint8_t);
void SoftwareReset(GP2AP0XVT_Dev_t *);
void SetVcselCurrent(GP2AP0XVT_Dev_t *, uint8_t);
void SetRegisters(GP2AP0XVT_Dev_t *);
void StartMeasurement(GP2AP0XVT_Dev_t *);
void StopMeasurement(GP2AP0XVT_Dev_t *);
uint8_t CheckMeasurementEndFlag(GP2AP0XVT_Dev_t *);
void ClearMeasurementEndFlag(GP2AP0XVT_Dev_t *);
void GetMeasurementData(GP2AP0XVT_Dev_t *);
void ReadMeasurementData(GP2AP0XVT_Dev_t *);
void CalculateDistance(GP2AP0XVT_Dev_t *);
int32_t CalcParaS(GP2AP0XVT_Dev_t *);
void XTalkAutomaticSet(GP2AP0XVT_Dev_t *);
uint8_t XTalkUpdate(GP2AP0XVT_Dev_t *);
void GetXTalkDataAverage(GP2AP0XVT_Dev_t *);
void SetOffsetCalibrationData(GP2AP0XVT_Dev_t *);
void SetXTalkCalibrationData(GP2AP0XVT_Dev_t *, uint32_t);
void GetDataAverageForCalibration(GP2AP0XVT_Dev_t *);
int8_t PerformOffsetCalibration(GP2AP0XVT_Dev_t *, int16_t);
int8_t PerformXTalkCalibration(GP2AP0XVT_Dev_t *);
void GetDataVHVVS1(GP2AP0XVT_Dev_t *);
int32_t GetDeviceID(GP2AP0XVT_Dev_t *);
int8_t PerformI2CSlaveAddressTrimming(GP2AP0XVT_Dev_t *, uint8_t);
void ModeSwitching(GP2AP0XVT_Dev_t *);
void ModeChangeShort2Long(GP2AP0XVT_Dev_t *);
void ModeChangeLong2Short(GP2AP0XVT_Dev_t *);
#endif //__GP2AP02VT_API_H__