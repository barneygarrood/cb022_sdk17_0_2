/** 
 * @file gp2ap02vt_api.c
 * @brief  API of the GP2AP02VT sensor   
 */
/////////////////////////////////////////////////////////////////////////
//Copyright (C) 2019 SHARP All Rights Reserved.
//This program is for modeling purposes only, and is not guaranteed.
//This program is API of the GP2AP02VT sensor.
/////////////////////////////////////////////////////////////////////////
#include "gp2ap02vt_api.h"
#include "gp2ap02vt_i2c.h"


/*! @name for use inside the API */
/* @{ */

/**
 * @brief Converts floating point to fixed point.
 * @param[in] floating point value
 * @param[in] bit width
 * @return Fixed point, integer part: 28 bits, decimal part: 4 bits
 * @note for use inside the API,no need to call from outside.
 */
static int32_t FloatingToFixed28p(int32_t floating, int8_t bit)
{
    int32_t exp = 0;
    int32_t mantissa = 0;
    int32_t fixed28p = 0;
    int8_t sign = 1; // pos(=1),neg(=-1)
    int32_t temp1;

    /* 8bits floating to 14bits floating without sign bit*/
    if (bit == BITSHIFT8) {
        floating = (floating << BITSHIFT5) & FLOAT8BIT_SIGN_MASK;
    }

    temp1 = floating & FLOAT14BIT_SIGN_BIT; //sign bit
    if (temp1 >= 1)
        sign = -1;

    exp = ((floating & FLOAT14BIT_EXP_BIT) >> BITSHIFT8);
    mantissa = floating & FLOAT14BIT_MANTISSA_BIT;
    if (exp != 0) {
        exp = exp - FLOAT14BIT_EXP_BIAS;
        mantissa = (mantissa + FLOAT14BIT_HIDDEN_BIT);
    } else if (exp == 0 && mantissa != 0) {
        exp = -11;
    }
    if (exp < 0)
        fixed28p = mantissa >> (-exp);
    else
        fixed28p = mantissa << exp;

    if ((fixed28p & FIXED28P_ROUNDINGBIT) == FIXED28P_ROUNDINGBIT)
        fixed28p += FIXED28P_ROUNDINGBIT;
    fixed28p = fixed28p >> BITSHIFT4;

    return (sign) * fixed28p;

}

/**
 * @brief Converts floating point to fixed point.
 * @param[in] floating point value
 * @param[in] bit width
 * @return Fixed point, integer part: 16 bits, decimal part: 16 bits
 * @note for use inside the API,no need to call from outside.
 */
int32_t FloatingToFixed16p(int32_t floating, int8_t bit)
{
    int32_t exp = 0; //exp
    int32_t mantissa = 0;
    int32_t fixed16p = 0;
    int8_t sign = 1; // pos(=1),neg(=-1)
    int32_t temp1;

    /* 8bits floating to 14bits floating without sign bit*/
    if (bit == BITSHIFT8) {
        floating = (floating << BITSHIFT5) & FLOAT8BIT_SIGN_MASK;
    }

    temp1 = floating & FLOAT14BIT_SIGN_BIT; //sign bit
    if (temp1 >= 1)
        sign = -1;

    exp = ((floating & FLOAT14BIT_EXP_BIT) >> BITSHIFT8);
    mantissa = floating & FLOAT14BIT_MANTISSA_BIT;
    if (exp != 0) {
        exp = exp - FLOAT14BIT_EXP_BIAS;
        mantissa = (mantissa + FLOAT14BIT_HIDDEN_BIT) << BITSHIFT8;
    } else if (exp == 0 && mantissa != 0) {
        exp = -11;
        mantissa = mantissa << BITSHIFT8;
    }

    if (exp > FLOAT_TO_FIXED16P_EXP_MAX) {//Max restriction 32704
        exp = FLOAT_TO_FIXED16P_EXP_MAX;
        mantissa = ((int32_t)FLOAT14BIT_HIDDEN_BIT + FLOAT14BIT_MANTISSA_BIT)
                << BITSHIFT8;
    }

    if (exp < 0)
        fixed16p = mantissa >> (-exp);
    else
        fixed16p = mantissa << exp;

    return (sign) * fixed16p;
}

/**
 * @brief Converts fixed point to floating point.
 * @param[in] fixed point value
 * @param[in] bit width
 * @return floating point, 13:sign, 12-8:exp, 7-0:frac
 * @note for use inside the API,no need to call from outside.
 */
static int32_t Fixed16pToFloating(int32_t fixed16p, int8_t bit)
{
    int32_t ret = 0;
    int8_t sign = 0; //sign bit
    int16_t exp = 0;
    uint16_t mantissa = 0;
    int8_t indx = 0;
    int32_t shift_bit = 0;
    int32_t msb_bit = FIXED_MSB;
    int32_t test_bit = 0;
    uint32_t search_bit[5] = {
        0xFFFF0000,
        0xFF000000,
        0xF0000000,
        0xC0000000,
        0x80000000
    };

    if (bit == BITSHIFT14 || bit == BITSHIFT8) {

        if (fixed16p < 0) {
            sign = 1;
            fixed16p *= -1;
        }

        for (indx = 0; indx < 5; indx++) {
            test_bit = search_bit[indx] >> shift_bit;

            if ((test_bit & fixed16p) == 0) {
                shift_bit += FIXED_MSB >> (indx + 1); //=shift_bit + 2^(4-indx)
            }
        }

        msb_bit = FIXED_MSB - shift_bit;
        exp = msb_bit - FIXED16P_BIAS - 1;
        if (exp <= -FLOAT14BIT_EXP_BIAS) {
            exp = 0;
            msb_bit = 6;
        } else {
            exp += FLOAT14BIT_EXP_BIAS;
        }

        if (msb_bit > MANTISSA_BIT_SIZE)
            mantissa = (fixed16p >> (msb_bit - 1 - MANTISSA_BIT_SIZE)) & 0xFF;
        else
            mantissa = (fixed16p << (1 + MANTISSA_BIT_SIZE - msb_bit)) & 0xFF;

        if (bit == BITSHIFT14) {
            ret = (sign << (EXP_BIT_SIZE + MANTISSA_BIT_SIZE)) | (exp << MANTISSA_BIT_SIZE) | mantissa;
        } else if (bit == BITSHIFT8) {
            ret = (exp << FLP8BTI_MANTISSA_SIZE) | (mantissa >> (BITSHIFT8 - FLP8BTI_MANTISSA_SIZE));
        }
    }
    return ret;
}

/** 
 * @brief Calculates the median of distance 
 * @param[in] *p_gp pointer to structure of sensor data 
 * @note for use inside the API,no need to call from outside.
 */
void GetMedianRange(GP2AP0XVT_Data_t *p_gp)
{
    uint8_t a, b;
    int16_t temp;
    int16_t sub_data[NUMMEASUREMENTSMEDIAN];


    p_gp->range1_median[p_gp->num_median1] = p_gp->range1;


    for (a = 0; a < NUMMEASUREMENTSMEDIAN; a++) {
        sub_data[a] = p_gp->range1_median[a];
    }

    for (a = 0; a < NUMMEASUREMENTSMEDIAN - 1; a++) {
        for (b = NUMMEASUREMENTSMEDIAN - 1; b > a; b--) {
            if (sub_data[b - 1] > sub_data[b]) {
                temp = sub_data[b];
                sub_data[b] = sub_data[b - 1];
                sub_data[b - 1] = temp;
            }
        }
    }

    p_gp->range1 = sub_data[NUMMEASUREMENTSMEDIAN / 2];

    if (p_gp->num_median1 == NUMMEASUREMENTSMEDIAN - 1)
        p_gp->num_median1 = 0;
    else
        p_gp->num_median1++;
}

/** 
 * @brief initializes the array to find the median.
 * @param[in] *p_gp pointer to structure of sensor data 
 * @note for use inside the API,no need to call from outside.
 */
void ClearMedianRange(GP2AP0XVT_Data_t *p_gp)
{
    p_gp->num_median1 = 0;
    p_gp->range1_median[0] = 0;
    p_gp->range1_median[1] = 0;
    p_gp->range1_median[2] = 0;
    p_gp->range1_median[3] = ERROR_RESULT;
    p_gp->range1_median[4] = ERROR_RESULT;
}

/** 
 * @brief initializes parameters
 * @param[in] *p_gp pointer to structure of sensor data 
 */
void InitParams(GP2AP0XVT_Data_t *p_gp)
{
    p_gp->range1 = 0;
    p_gp->range1_pre = 0;
    p_gp->range1_status = 0;
    p_gp->range1_status_pre = 0;

    ClearMedianRange(p_gp);

    p_gp->offset_calibration = 0;
    p_gp->xtalk_calibration = 0;

    p_gp->flag_update = 1;
    p_gp->flag_short2long_lowsig = 0;

    p_gp->prst_hxtalk = 0;
    
    p_gp->data_xtalk_pre = p_gp->data_xtalk_factory;
    p_gp->data_xtalk_ih = p_gp->data_xtalk_factory;

    p_gp->data_xtalk_il = Fixed16pToFloating(FloatingToFixed16p(p_gp->data_xtalk_factory, BIT14)
            * 11 / 10, BIT14);

    p_gp->d_range=DRANGE;
}
/* @} */

/*! @name API to call */
/* @{ */

/** 
 * @brief Initializes sensor.
 * @param[in] *dev pointer to the structure of sensor
 * @return returns status INITIALIZATION_FAILED or INITIALIZATION_PASSED.
 * @note Execute only once before starting measurement.\n LONG_RANGE_MODE can be set only GP2AP02VT
 */
int8_t InitData(GP2AP0XVT_Dev_t *dev, uint8_t measurement_setting)
{
    int8_t status;
    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);

    SoftwareReset(dev);
    InitParams(p_gp);
    status = InitSetting(dev, measurement_setting);
    return status;
}
/* @} */

/*! @name for use inside the API */
/* @{ */

/** 
 * @brief Initializes sensor setting.
 * @param[in] *dev pointer to the structure of sensor
 * @return returns status INITIALIZATION_FAILED or INITIALIZATION_PASSED.
 * @note for use inside the API,no need to call from outside.LONG_RANGE_MODE can be set only GP2AP02VT.
 */

int8_t InitSetting(GP2AP0XVT_Dev_t *dev, uint8_t measurement_setting)
{
    uint8_t rdata[4],wdata;
    int8_t status = INITIALIZATION_FAILED;

    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);

    /* check device id */
    GetDeviceID(dev);
    if (p_gp->device_id != CHIP_ID) {
        return status;
    }

    /* init parameters */
    dev->measurement_mode = measurement_setting; /* SHORT_RANGE_MODE/LONG_RANGE_MODE */

    p_gp->spad_area=SPAD_AREA_0;
  
    SetRegisters(dev);
    SetOffsetCalibrationData(dev);
    SetXTalkCalibrationData(dev, p_gp->data_xtalk_factory);
    if (dev->measurement_mode == LONG_RANGE_MODE) {
        SetVcselCurrent(dev, IDRV_LONG);
    } else {
        SetVcselCurrent(dev, IDRV_SHORT);
    } 

    gp2ap_i2c_read(dev, REG_50H, rdata, sizeof (rdata));
    if ((rdata[0] == 0xFF) &&
            (rdata[1] == 0xFF) &&
            (rdata[2] == 0xFF) &&
            (rdata[3] == 0xFF)) {
        status = INITIALIZATION_FAILED;
    } else {
        p_gp->pll = (rdata[1] & MASK_TRIMMING_PLL) >> BITSHIFT3;
        if(p_gp->pll > 7){
            p_gp->pll = p_gp->pll - 16; 
        }
        p_gp->offset_internal = (int8_t) rdata[2];

        status = INITIALIZATION_PASSED;
    }
    wdata = p_gp->spad_area;
    gp2ap_i2c_write(dev, REG_4EH, &wdata, sizeof (wdata));
    return status;
}


/** 
 * @brief changes mode to LONG_RANGE_MODE.
 * @param[in] *dev pointer to the structure of sensor
 * @note for use inside the API,no need to call from outside.Valid only for GP2AP02VT.
 */

void ModeChangeShort2Long(GP2AP0XVT_Dev_t *dev)
{
    uint8_t wdata;

    dev->measurement_mode = LONG_RANGE_MODE;
    SetOffsetCalibrationData(dev);
    SetVcselCurrent(dev, IDRV_LONG);
    wdata = NORMALAUTOVHV | IDRV_LONG;
    gp2ap_i2c_write(dev, REG_06H, &wdata, sizeof (wdata));
    wdata = REG_14H_SETTING_D;
    gp2ap_i2c_write(dev, REG_14H, &wdata, sizeof (wdata));
    wdata = REG_43H_SETTING;
    gp2ap_i2c_write(dev, REG_43H, &wdata, sizeof (wdata));
    wdata = REG_47H_SETTING_D;
    gp2ap_i2c_write(dev, REG_47H, &wdata, sizeof (wdata));
}


/** 
 * @brief changes mode to SHORT_RANGE_MODE.
 * @param[in,out] *dev pointer to the structure of sensor
 * @note for use inside the API,no need to call from outside.Valid only for GP2AP02VT.
 */
void ModeChangeLong2Short(GP2AP0XVT_Dev_t *dev)
{
    uint8_t wdata;
    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);

    dev->measurement_mode = SHORT_RANGE_MODE;

    p_gp->flag_comp_rate = 0;
    SetOffsetCalibrationData(dev);

    SetVcselCurrent(dev, IDRV_SHORT);
    wdata = NORMALAUTOVHV | IDRV_SHORT;
    gp2ap_i2c_write(dev, REG_06H, &wdata, sizeof (wdata));
    SetOffsetCalibrationData(dev);

    wdata = REG_14H_SETTING_P;
    gp2ap_i2c_write(dev, REG_14H, &wdata, sizeof (wdata));
    wdata = REG_43H_SETTING;
    gp2ap_i2c_write(dev, REG_43H, &wdata, sizeof (wdata));
    wdata = REG_47H_SETTING_P;
    gp2ap_i2c_write(dev, REG_47H, &wdata, sizeof (wdata));
    wdata = REG_1EH_SETTING0XF4;
    gp2ap_i2c_write(dev, REG_1EH, &wdata, sizeof (wdata));
    p_gp->reg1Eh = wdata;
}

/** 
 * @brief Software reset.
 * @param[in] *dev pointer to the structure of sensor.
 * @note for use inside the API, no need to call from outside.
 */
void SoftwareReset(GP2AP0XVT_Dev_t *dev)
{
    uint8_t wdata;

    wdata = SOFTWARE_RESET;
    gp2ap_i2c_write(dev, REG_02H, &wdata, sizeof (wdata));

    /* Set the internal trimming value */
    wdata = ACTIVE;
    gp2ap_i2c_write(dev, REG_00H, &wdata, sizeof (wdata));
    DELAY_FUNC(2); // 2msec
    wdata = SHUTDOWN;
    gp2ap_i2c_write(dev, REG_00H, &wdata, sizeof (wdata));
    wdata = 0x00;
    gp2ap_i2c_write(dev, REG_2EH, &wdata, sizeof (wdata));
}

/** 
 * @brief sets VCSEL current.
 * @param[in,out] *dev pointer to the structure of sensor
 * @param[in] wdata1 register setting value of VCSEL current
 * @note for use inside the API,no need to call from outside.
 */
void SetVcselCurrent(GP2AP0XVT_Dev_t *dev, uint8_t wdata1)
{
    uint8_t wdata;
    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);

    wdata = wdata1 | REG_04H_SETTING;
    gp2ap_i2c_write(dev, REG_04H, &wdata, sizeof (wdata));

    if (wdata1 == IDRV_LONG) {
        p_gp->vcsel_current = IDRV_LONG;
    } else if (wdata1 == IDRV_SHORT) {
        p_gp->vcsel_current = IDRV_SHORT;
    }
}

/** 
 * @brief sets the datas for sensor.
 * @param[in] *dev pointer to the structure of sensor
 * @note for use inside the API,no need to call from outside.
 */
void SetRegisters(GP2AP0XVT_Dev_t *dev)
{
    uint8_t wdata;

    wdata = OUTPUT_FLAG_EVERYTIME | INTTYPE_LEVEL | EXTPIN_DISABLE;
    gp2ap_i2c_write(dev, REG_02H, &wdata, sizeof (wdata));
    wdata = REG_03H_SETTING;
    gp2ap_i2c_write(dev, REG_03H, &wdata, sizeof (wdata));
    wdata = REG_05H_SETTING;
    gp2ap_i2c_write(dev, REG_05H, &wdata, sizeof (wdata));
    if (dev->measurement_mode == LONG_RANGE_MODE) {
        wdata = NORMALAUTOVHV | IDRV_LONG;
    } else {
        wdata = NORMALAUTOVHV | IDRV_SHORT;
    }
    gp2ap_i2c_write(dev, REG_06H, &wdata, sizeof (wdata));
    wdata = REG_07H_SETTING;
    gp2ap_i2c_write(dev, REG_07H, &wdata, sizeof (wdata));
    wdata = REG_0DH_SETTING;
    gp2ap_i2c_write(dev, REG_0DH, &wdata, sizeof (wdata));
    wdata = REG_0EH_SETTING;
    gp2ap_i2c_write(dev, REG_0EH, &wdata, sizeof (wdata));
    wdata = REG_11H_SETTING;
    gp2ap_i2c_write(dev, REG_11H, &wdata, sizeof (wdata));
    wdata = REG_12H_SETTING;
    gp2ap_i2c_write(dev, REG_12H, &wdata, sizeof (wdata));
    wdata = REG_13H_SETTING;
    gp2ap_i2c_write(dev, REG_13H, &wdata, sizeof (wdata));
    if (dev->measurement_mode == LONG_RANGE_MODE) {
        wdata = REG_14H_SETTING_D;
        gp2ap_i2c_write(dev, REG_14H, &wdata, sizeof (wdata));
        wdata = REG_47H_SETTING_D;
        gp2ap_i2c_write(dev, REG_47H, &wdata, sizeof (wdata));
    } else {
        wdata = REG_14H_SETTING_P;
        gp2ap_i2c_write(dev, REG_14H, &wdata, sizeof (wdata));
        wdata = REG_47H_SETTING_P;
        gp2ap_i2c_write(dev, REG_47H, &wdata, sizeof (wdata));
    }
    wdata = REG_15H_SETTING;
    gp2ap_i2c_write(dev, REG_15H, &wdata, sizeof (wdata));
    wdata = REG_1AH_SETTING;
    gp2ap_i2c_write(dev, REG_1AH, &wdata, sizeof (wdata));
    wdata = REG_1BH_SETTING;
    gp2ap_i2c_write(dev, REG_1BH, &wdata, sizeof (wdata));
    wdata = REG_1CH_SETTING;
    gp2ap_i2c_write(dev, REG_1CH, &wdata, sizeof (wdata));
    wdata = REG_1DH_SETTING;
    gp2ap_i2c_write(dev, REG_1DH, &wdata, sizeof (wdata));
    wdata = REG_1EH_SETTING;
    gp2ap_i2c_write(dev, REG_1EH, &wdata, sizeof (wdata));
    wdata = REG_1FH_SETTING;
    gp2ap_i2c_write(dev, REG_1FH, &wdata, sizeof (wdata));
    wdata = REG_28H_SETTING;
    gp2ap_i2c_write(dev, REG_28H, &wdata, sizeof (wdata));
    wdata = REG_2AH_SETTING;
    gp2ap_i2c_write(dev, REG_2AH, &wdata, sizeof (wdata));
    wdata = REG_2BH_SETTING;
    gp2ap_i2c_write(dev, REG_2BH, &wdata, sizeof (wdata));
    wdata = REG_43H_SETTING;
    gp2ap_i2c_write(dev, REG_43H, &wdata, sizeof (wdata));
    wdata = REG_45H_SETTING;
    gp2ap_i2c_write(dev, REG_45H, &wdata, sizeof (wdata));
    wdata = REG_4DH_SETTING;
    gp2ap_i2c_write(dev, REG_4DH, &wdata, sizeof (wdata));

}

/* @} */
/*! @name API to call */
/* @{ */

/** 
 * @brief starts measurement.
 * @param[in] *dev pointer to the structure of sensor.
 */
void StartMeasurement(GP2AP0XVT_Dev_t *dev)
{
    uint8_t wdata;

    wdata = ACTIVE;
    gp2ap_i2c_write(dev, REG_00H, &wdata, sizeof (wdata));
}
/**
 * @brief Stop measurement
 * @param[in] *dev pointer to the structure of sensor
 */
void StopMeasurement(GP2AP0XVT_Dev_t *dev)
{
    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);

    /* init parameters */
    InitParams(p_gp);
    ClearMedianRange(p_gp);
}
/** 
 * @brief checks measurement end flag bits.
 * @param[in] *dev pointer to the structure of sensor
 * @return returns sensor status MEASUREMENT_ENDED or MEASUREMENT_NOT_ENDED
 *  */
uint8_t CheckMeasurementEndFlag(GP2AP0XVT_Dev_t *dev)
{
    uint8_t status;
    uint8_t rdata_01h;

    int32_t err_code = gp2ap_i2c_read(dev, REG_01H, &rdata_01h, sizeof (rdata_01h));
    if (err_code!=0)
    {
      return MEASUREMENT_DISCONNECTED;
    }
    if ((rdata_01h & FLAG_BIT) == FLAG_BIT) {
        status = MEASUREMENT_ENDED;
    } else {
        status = MEASUREMENT_NOT_END;
    }

    return status;
}

/** 
 * @brief clears measurement end flag bits.
 * @param[in] *dev pointer to the structure of sensor
 */
void ClearMeasurementEndFlag(GP2AP0XVT_Dev_t *dev)
{
    uint8_t wdata;

    wdata = CLEAR_FLAG;
    gp2ap_i2c_write(dev, REG_01H, &wdata, sizeof (wdata));
}

/** 
 * @brief gets mesurement data, calculates distance and uptades the range mode and the cross talk setting.
 * @param[in] *dev pointer to the structure of sensor
 * @note call this function after the function CheckMeasurementEndFlag()  returns MEASUREMENT_ENDED
 */
void GetMeasurementData(GP2AP0XVT_Dev_t *dev)
{
    ReadMeasurementData(dev);

    CalculateDistance(dev);

    ModeSwitching(dev);

    XTalkAutomaticSet(dev);
}
/* @} */
/*! @name for use inside the API */
/* @{ */

/** 
 * @brief reads measurement data via i2c.
 * @param[in] *dev pointer to the structure of sensor
 * @note for use inside the API,no need to call from outside.
 */
void ReadMeasurementData(GP2AP0XVT_Dev_t *dev)
{
    uint8_t rdata[3];
    uint8_t rdata_18h[2];
    uint8_t wdata;
    int32_t data_xtalk_16p, temp1, temp2;

    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);

    gp2ap_i2c_read(dev, REG_2EH, rdata, sizeof (rdata));
    p_gp->range1_status = rdata[0];
    p_gp->range1_status_raw = rdata[0];
    p_gp->range2 = (((uint16_t) rdata[2]) << BITSHIFT8) + (uint16_t) rdata[1];

    wdata = REG_100D | RET_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
    p_gp->maxbin_ret = rdata[1];

    wdata = REG_102D | RET_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
    p_gp->fine_ret_fx16p = (int32_t) rdata[2] << BITSHIFT8 | (int32_t) rdata[1];
    p_gp->fine_ret_fx16p = FloatingToFixed16p(p_gp->fine_ret_fx16p, BIT14);

    wdata = REG_100D | REF_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
    p_gp->maxbin_ref = rdata[1];

    wdata = REG_102D | REF_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
    p_gp->fine_ref_fx16p = (int32_t) rdata[2] << BITSHIFT8 | (int32_t) rdata[1];
    p_gp->fine_ref_fx16p = FloatingToFixed16p(p_gp->fine_ref_fx16p, BIT14);

    wdata = REG_103D | REF_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
    p_gp->pw_ref_fx16p = (int32_t) rdata[2] << BITSHIFT8 | (int32_t) rdata[1];
    p_gp->pw_ref_fx16p = FloatingToFixed16p(p_gp->pw_ref_fx16p, BIT14);

    wdata = REG_39D | RET_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
    p_gp->hist39binfp = (int32_t) rdata[2] << BITSHIFT8 | (int32_t) rdata[1];

    wdata = REG_108D | RET_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
    p_gp->amb_fx28p = (int32_t) rdata[2] << BITSHIFT8 | (int32_t) rdata[1];
    p_gp->amb_fx28p = FloatingToFixed28p(p_gp->amb_fx28p, BIT14);

    wdata = REG_108D | REF_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
    p_gp->amb_ref_fx28p = (int32_t) rdata[2] << BITSHIFT8 | (int32_t) rdata[1];
    p_gp->amb_ref_fx28p = FloatingToFixed28p(p_gp->amb_ref_fx28p, BIT14);

    wdata = REG_106D | RET_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
    p_gp->sum_sig_fx28p = (int32_t) rdata[2] << BITSHIFT8 | (int32_t) rdata[1];
    p_gp->sum_sig_fx28p = FloatingToFixed28p(p_gp->sum_sig_fx28p, BIT14);

    wdata = REG_106D | REF_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
    p_gp->sum_ref_fx28p = (int32_t) rdata[2] << BITSHIFT8 | (int32_t) rdata[1];
    p_gp->sum_ref_fx28p = FloatingToFixed28p(p_gp->sum_ref_fx28p, BIT14);

    wdata = (p_gp->maxbin_ref - 1) | REF_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
    p_gp->refhist_fx28p[0] = (int32_t) rdata[2] << BITSHIFT8 | (int32_t) rdata[1];
    p_gp->refhist_fx28p[0] = FloatingToFixed28p(p_gp->refhist_fx28p[0], BIT14);

    wdata = REG_109D | RET_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
    p_gp->ret_maxcnt_fx28p = (int32_t) rdata[2] << BITSHIFT8 | (int32_t) rdata[1];
    p_gp->ret_maxcnt_fx28p = FloatingToFixed28p(p_gp->ret_maxcnt_fx28p, BIT14);

    wdata = p_gp->maxbin_ret | RET_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
    p_gp->rethistmax_fx28p = (int32_t) rdata[2] << BITSHIFT8 | (int32_t) rdata[1];
    p_gp->rethistmax_fx28p = FloatingToFixed28p(p_gp->rethistmax_fx28p, BIT14);

    wdata = (p_gp->maxbin_ref + 1) | REF_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof(wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof(rdata));
    p_gp->refhist_fx28p[2] = (int32_t) rdata[2] << BITSHIFT8 | (int32_t)rdata[1];
    p_gp->refhist_fx28p[2] = FloatingToFixed28p(p_gp->refhist_fx28p[2], BIT14);

    wdata = (p_gp->maxbin_ref + 4) | REF_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof(wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof(rdata));
    p_gp->refhist_fx28p[3] = (int32_t) rdata[2] << BITSHIFT8 | (int32_t)rdata[1];
    p_gp->refhist_fx28p[3] = FloatingToFixed28p(p_gp->refhist_fx28p[3], BIT14);

    wdata = (p_gp->maxbin_ref + 5) | REF_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof(wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof(rdata));
    p_gp->refhist_fx28p[4] = (int32_t) rdata[2] << BITSHIFT8 | (int32_t)rdata[1];
    p_gp->refhist_fx28p[4] = FloatingToFixed28p(p_gp->refhist_fx28p[4], BIT14);

    wdata = (p_gp->maxbin_ref + 6) | REF_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof(wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof(rdata));
    p_gp->refhist_fx28p[5] = (int32_t) rdata[2] << BITSHIFT8 | (int32_t)rdata[1];
    p_gp->refhist_fx28p[5] = FloatingToFixed28p(p_gp->refhist_fx28p[5], BIT14);

    wdata = REG_39D | REF_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof(wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof(rdata));
    p_gp->hist_ref39_fx28p = (int32_t)rdata[2] << BITSHIFT8 | (int32_t)rdata[1];
    p_gp->hist_ref39_fx28p = FloatingToFixed28p(p_gp->hist_ref39_fx28p, BIT14);

    wdata = REG_88D | RET_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
    temp1 = (int32_t) rdata[2] << BITSHIFT8 | (int32_t) rdata[1];
    wdata = REG_89D | RET_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
    temp2 = (int32_t) rdata[2] << BITSHIFT8 | (int32_t) rdata[1];
    p_gp->sum_sig2 = temp2 << BITSHIFT14 | temp1;
    if ((p_gp->sum_sig2 & 0x80000) == 0x80000) {
        p_gp->sum_sig2 = p_gp->sum_sig2 - 1;
        p_gp->sum_sig2 = p_gp->sum_sig2 ^ 0xFFFFF;
        p_gp->sum_sig2 = p_gp->sum_sig2 & 0x7FFFF;
        p_gp->sum_sig2 *= -1;
    }
    wdata = INITIAL_SETTING;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));

    p_gp->ret_sig = p_gp->sum_sig_fx28p / FIXED28P_DEC;
    p_gp->ret_amb = 10 * p_gp->amb_fx28p / FIXED28P_DEC;
    p_gp->ref_sig = p_gp->sum_ref_fx28p / FIXED28P_DEC;

    /* crosstalk */
    gp2ap_i2c_read(dev, REG_18H, rdata_18h, sizeof (rdata_18h));
    p_gp->data_xtalk = (((uint16_t) rdata_18h[1]) << BITSHIFT8) + (uint16_t) rdata_18h[0];

    if ((p_gp->data_xtalk & FLOAT14BIT_SIGN_BIT) == FLOAT14BIT_SIGN_BIT) {
        p_gp-> data_xtalk = 0;
    }
    if((p_gp->data_xtalk < p_gp->data_xtalk_factory) && (p_gp->xtalk_calibration == 0)){
        p_gp->data_xtalk = p_gp->data_xtalk_factory;
    }
    
    data_xtalk_16p = FloatingToFixed16p(p_gp->data_xtalk, BIT14);

    if (p_gp->data_xtalk != p_gp->data_xtalk_pre) {

        if ((data_xtalk_16p - Fixed16pToFloating(p_gp->data_xtalk_pre, BIT14) < XTALK_DELTA_MAX) || (p_gp->xtalk_calibration)) {
            if (dev->measurement_mode == LONG_RANGE_MODE) {
                p_gp->data_xtalk_ih = p_gp->data_xtalk;
                p_gp->data_xtalk_il = data_xtalk_16p * 11 / 10; //55 / 100;
                p_gp->data_xtalk_il = Fixed16pToFloating(p_gp->data_xtalk_il, BIT14);
                p_gp->flag_comp_rate = 0;
            } else {
                p_gp->data_xtalk_ih = data_xtalk_16p * 15 / 10; //26 / 10;
                p_gp->data_xtalk_ih = Fixed16pToFloating(p_gp->data_xtalk_ih, BIT14);
                p_gp->data_xtalk_il = p_gp->data_xtalk;
            } 
            p_gp->data_xtalk_pre = p_gp->data_xtalk;
        } else {
            p_gp->data_xtalk = p_gp->data_xtalk_pre;
        }
    }

    /* crosstalk calibration */
    if (p_gp->xtalk_calibration) {
        if ((p_gp->data_xtalk != XTALK_CALIB_SETTING) && (p_gp->data_xtalk != 0)) {
            p_gp->num_xtalk_data += 1;
            p_gp->data_xtalk = FloatingToFixed16p(p_gp->data_xtalk, BIT14);
            p_gp->data_xtalk_ave += p_gp->data_xtalk;
        }
    }
}

/** 
 * @brief calculations of distance value.
 * @param[in] *dev pointer to the structure of sensor
 * @note for use inside the API,no need to call from outside.
 */
void CalculateDistance(GP2AP0XVT_Dev_t *dev)
{
    int32_t pw_ret, sum_ref2_fx28p, histAve, fine_ref3,range1_x1000;
    int32_t data_xtalk_16p, chkdiv;
    int32_t PARA_X, PARA_Y, PARA_S, B, sum_ref3_fx28p;
    int16_t range1_temp, range2_temp, temp;
    uint8_t isZero = 0;
    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);

    p_gp->error_handling_num = 0x0000;

    chkdiv = p_gp->pw_ref_fx16p * FIXED28P_DEC / FIXED16P_28P;
    if (chkdiv != 0) {
        sum_ref2_fx28p = p_gp->sum_ref_fx28p + 7 * p_gp->amb_ref_fx28p -7 * p_gp->hist_ref39_fx28p;
        p_gp->refhist_fx28p[1] = p_gp->sum_ref_fx28p * FIXED28P_DEC / chkdiv * FIXED28P_DEC + p_gp->amb_ref_fx28p;        
    } else {
        isZero = 1;
    }

    sum_ref3_fx28p = (p_gp->refhist_fx28p[3] + p_gp->refhist_fx28p[4] + p_gp->refhist_fx28p[5]) - 3 * p_gp->hist_ref39_fx28p;

    chkdiv=(sum_ref2_fx28p + sum_ref3_fx28p);
    if(chkdiv != 0){
        PARA_X = sum_ref3_fx28p * ONETHOUSAND / chkdiv;
        if(dev->measurement_mode == LONG_RANGE_MODE) {
            PARA_Y = (PARA_X * PARA_Y1_L / (p_gp->fine_ref_fx16p / 256 * ONETHOUSAND / 256 + PARA_Y2_L));
        } else {
            PARA_Y = (PARA_X * PARA_Y1_S / (p_gp->fine_ref_fx16p / 256 * ONETHOUSAND / 256 + PARA_Y2_S));
        }
    }else{
        isZero = 1;
    }
    histAve = ((p_gp->refhist_fx28p[0] + p_gp->refhist_fx28p[1] + p_gp->refhist_fx28p[2]) / 3 - p_gp->hist_ref39_fx28p) / FIXED28P_DEC;

    if((histAve != 0) && (p_gp->pw_ref_fx16p != 0)){
        p_gp->pw_ref3 = (sum_ref2_fx28p / FIXED28P_DEC) * ONETHOUSAND / (histAve);
        fine_ref3 = (p_gp->fine_ref_fx16p) * TENTHOUSAND / (p_gp->pw_ref_fx16p) * (p_gp->pw_ref3) / TENTHOUSAND;
        if(dev->measurement_mode == LONG_RANGE_MODE) {
            if ((p_gp->pw_ref3 != 0)&&(fine_ref3 != FINE_REF_SLOPE_COEF3_L)) {

                p_gp->fine_ref_slope_x = (p_gp->pw_ref3 * ONETHOUSAND - fine_ref3 * fine_ref3 / ONETHOUSAND * FINE_REF_SLOPE_COEF1_L - FINE_REF_SLOPE_COEF2_L)
                    / (FINE_REF_SLOPE_COEF3_L - fine_ref3);

                chkdiv = p_gp->fine_ref_slope_x * p_gp->fine_ref_slope_x / ONETHOUSAND * FINE_REF_COR_COEF2_L / TENTHOUSAND
                            - FINE_REF_COR_COEF3_L * p_gp->fine_ref_slope_x / ONETHOUSAND + FINE_REF_COR_COEF4_L;
                if(chkdiv != 0) {
                    p_gp->fine_ref_cor4 = ((p_gp->fine_ref_fx16p * ONETHOUSAND / p_gp->pw_ref3 * ONETHOUSAND / 256 * ONETHOUSAND / 256 - FINE_REF_COR_COEF1_L)
                        / chkdiv + FINE_REF_COR_COEF5_L);
                } else {
                    isZero = 1;
                }
                p_gp->collect_f = (-sum_ref2_fx28p / FIXED28P_DEC * COLLECT_F_COEF1 / TENTHOUSAND * (sum_ref2_fx28p / FIXED28P_DEC) / TENTHOUSAND
                                    + sum_ref2_fx28p / FIXED28P_DEC * COLLECT_F_COEF2 / ONETHOUSAND - COLLECT_F_COEF3)/ ONETHOUSAND;                
            } else {
                isZero = 1;
            }
        } else {
            if ((p_gp->pw_ref3 != 0)&&(fine_ref3 != FINE_REF_SLOPE_COEF3_S)) {

                p_gp->fine_ref_slope_x = (p_gp->pw_ref3 * ONETHOUSAND - fine_ref3 * fine_ref3 / ONETHOUSAND * FINE_REF_SLOPE_COEF1_S - FINE_REF_SLOPE_COEF2_S)
                    / (FINE_REF_SLOPE_COEF3_S - fine_ref3);

                chkdiv = p_gp->fine_ref_slope_x * p_gp->fine_ref_slope_x / ONETHOUSAND * FINE_REF_COR_COEF2_S / TENTHOUSAND
                            - FINE_REF_COR_COEF3_S * p_gp->fine_ref_slope_x / ONETHOUSAND + FINE_REF_COR_COEF4_S;
                if(chkdiv != 0) {
                    p_gp->fine_ref_cor4 = ((p_gp->fine_ref_fx16p * ONETHOUSAND / p_gp->pw_ref3 * ONETHOUSAND / 256 * ONETHOUSAND / 256 - FINE_REF_COR_COEF1_S)
                        / chkdiv + FINE_REF_COR_COEF5_S);
                } else {
                    isZero = 1;
                }
                p_gp->collect_f = (-sum_ref2_fx28p / FIXED28P_DEC * COLLECT_F_COEF1 / TENTHOUSAND * (sum_ref2_fx28p / FIXED28P_DEC) / TENTHOUSAND
                                    + sum_ref2_fx28p / FIXED28P_DEC * COLLECT_F_COEF2 / ONETHOUSAND - COLLECT_F_COEF3)/ ONETHOUSAND;                
            } else {
                isZero = 1;
            }
        }  
    }else{
        isZero = 1;    
    }

    // calculate the distance
    if ((p_gp->hist39binfp & FLOAT14BIT_SIGN_BIT) == FLOAT14BIT_SIGN_BIT) {
        p_gp->range1 = RANGE0MM;
        p_gp->range1_status = VALID_DATA;
        p_gp->ret_sig = RETURNSIGMAX;
        p_gp->error_handling_num |= 0x0040;
    } else if ((isZero != 1) && (p_gp->ret_maxcnt_fx28p / FIXED28P_DEC != 0) && (((p_gp->rethistmax_fx28p - p_gp->amb_fx28p) / FIXED28P_DEC) != 0)) {
        p_gp->frate_fx28p = p_gp->amb_fx28p * FIXED28P_DEC * COEFF_AMBTOFRATE40 / p_gp->ret_maxcnt_fx28p;
        p_gp->frate_sig = (p_gp->sum_sig_fx28p / FIXED28P_DEC) * RANGECOEF8 * ONETHOUSAND / (p_gp->ret_maxcnt_fx28p / FIXED28P_DEC);
        p_gp->pw_ret = (p_gp->sum_sig_fx28p / FIXED28P_DEC) * RANGECOEF8 * ONETHOUSAND / ((p_gp->rethistmax_fx28p - p_gp->amb_fx28p) / FIXED28P_DEC);

        p_gp->delta_bin = (int32_t)p_gp->maxbin_ret * ONETHOUSAND - (int32_t)p_gp->maxbin_ref * ONETHOUSAND + 
                    (p_gp->fine_ret_fx16p * p_gp->pw_ret / p_gp->pw_ref_fx16p - p_gp->fine_ref_cor4) - p_gp->collect_f;

        if (dev->measurement_mode == LONG_RANGE_MODE) {
               p_gp->delta_bin += PARA_Y;
        } else {
            if(PARA_Y > TEMP_COLLECT1) {
                B = PARA_Y * PARA_Y / ONETHOUSAND + (PARA_Y - TEMP_COLLECT1) * (PARA_Y - TEMP_COLLECT1) / ONETHOUSAND * (PARA_Y - TEMP_COLLECT1) / ONEHUNDRED * TEMP_COLLECT2 / TEN;
            } else {
                B = PARA_Y * PARA_Y / ONETHOUSAND;
            }
            p_gp->delta_bin += B;
        }

        p_gp->correction_slop = (RANGECOEF35 * ONETHOUSAND * ONETHOUSAND - p_gp->delta_bin * p_gp->delta_bin / TEN / RANGECOEF442 * ONETHOUSAND);

        if (p_gp->correction_slop > 0) {
            range1_x1000 = p_gp->delta_bin * RANGECOEF75 / ONEHUNDRED * (TENTHOUSAND + (int32_t)RANGECOEF25 * p_gp->pll -
                (RANGECOEF350 - p_gp->delta_bin * p_gp->delta_bin / (RANGECOEF442 * ONETHOUSAND))) / ONEHUNDRED;
        } else {
            range1_x1000 = p_gp->delta_bin * RANGECOEF75 / ONETHOUSAND * (TENTHOUSAND + (int32_t)RANGECOEF25 * p_gp->pll) / TEN;
        }

        if (dev->measurement_mode == LONG_RANGE_MODE) {
            if(p_gp->frate_sig > FRATE_TH1_L){
                range1_x1000 += FRATE_COEF_D_L;
            } else if (FRATE_TH2_L < p_gp->frate_sig){
                range1_x1000 += (p_gp->frate_sig) * FRATE_COEF_A_L / TEN + (p_gp->frate_sig - FRATE_COEF_C_L) * (p_gp->frate_sig - FRATE_COEF_C_L) / ONETHOUSAND * FRATE_COEF_B_L / TEN;
            } else if (0 < p_gp->frate_sig){
                range1_x1000 += (p_gp->frate_sig) * FRATE_COEF_A_L / TEN; 
            }
        } else {
            if(p_gp->frate_sig > FRATE_TH1_S){
                range1_x1000 += FRATE_COEF_D_S;
            } else if (FRATE_TH2_S < p_gp->frate_sig){
                range1_x1000 += (p_gp->frate_sig) * FRATE_COEF_A_S / TEN + (p_gp->frate_sig - FRATE_COEF_C_S) * (p_gp->frate_sig - FRATE_COEF_C_S) / ONETHOUSAND * FRATE_COEF_B_S / TEN;
            } else if (0 < p_gp->frate_sig){
                range1_x1000 += (p_gp->frate_sig) * FRATE_COEF_A_S / TEN; 
            }
        }

        if(p_gp->sum_sig_fx28p != 0){
            PARA_S = CalcParaS(dev);
            range1_x1000 += PARA_S * p_gp->amb_fx28p / p_gp->sum_sig_fx28p;
        }

        if (dev->measurement_mode == LONG_RANGE_MODE) {
            range1_x1000 += ((int32_t)(p_gp->offset_long1 + p_gp->offset_internal) * ONETHOUSAND);
        } else {
            range1_x1000 += ((int32_t)(p_gp->offset_short1 + p_gp->offset_internal) * ONETHOUSAND);
        }
        if(range1_x1000>0){
            p_gp->range1 = (range1_x1000 + ROUNDOFF500) / ONETHOUSAND;
        }else{
            p_gp->range1 = (range1_x1000 - ROUNDOFF500) / ONETHOUSAND;            
        }
        data_xtalk_16p = FloatingToFixed16p(p_gp->data_xtalk, BIT14);
        p_gp->range1_raw = p_gp->range1;
        /* error handler */
        // Flow_Number_3

        if ((dev->measurement_mode == LONG_RANGE_MODE) &&
                (p_gp->ret_sig >= RETURN_SIG_TH9)) {
            p_gp->range1_status = p_gp->range1_status & LOW_SIG_BIT_MASK;
            p_gp->error_handling_num |= 0x0200;
        }

        // Flow_Number_4
        if (dev->measurement_mode == LONG_RANGE_MODE) {
            if (data_xtalk_16p > XTALK_TH_1) {
                p_gp->range1_status = p_gp->range1_status | HXTALK_BIT;
                p_gp->error_handling_num |= 0x0800;
            }
        } else {
            if (data_xtalk_16p > XTALK_TH_2) {
                p_gp->range1_status = p_gp->range1_status | HXTALK_BIT;
                p_gp->error_handling_num |= 0x0800;
            }
        } 

        // Flow_Number_5
        if (((p_gp->ret_sig * EIGHTTIMES * FIXED28P_DEC)) > TEN * p_gp->amb_fx28p * SIGAMBRATIO) {
            p_gp->range1_status = p_gp->range1_status & LOW_SN_BIT_MASK;
            p_gp->error_handling_num |= 0x0002;
        }

        // Flow_Number_6
        if ((p_gp->frate_fx28p < FRATE_FIX28PTH) &&
                ((((p_gp->ret_sig * (MAXCNT_COUNT / FIXED28P_DEC)) / (p_gp->ret_maxcnt_fx28p / FIXED28P_DEC) * FIXED28P_DEC) > MAXCNT_COUNT_TH))) {
            p_gp->range1_status = p_gp->range1_status & LOW_SN_BIT_MASK & LOW_SIG_BIT_MASK;
            p_gp->error_handling_num |= 0x0004;
        }

        // Flow_Number_7
        if ((-262144 >= p_gp->sum_sig2) || (p_gp->sum_sig2 >= 262144)) {
                p_gp->range2 = 0;
                p_gp->error_handling_num |= 0x0008;
        }

        range1_temp = p_gp->range1 + 400;
        range2_temp = p_gp->range2 + 400;

        if (range1_temp >= 3000) {
                range1_temp = range1_temp - 3000;
        }

        if (range2_temp >= 4032) {
                range2_temp = range2_temp - 4032;
        }

        temp = range1_temp - range2_temp;

        if (temp < 0)
            temp = temp * -1;

        if (temp < 500) {
            p_gp->range1_status = p_gp->range1_status & WAF_BIT_MASK;
        } else {
            p_gp->range1_status = p_gp->range1_status | WAF_BIT;
            p_gp->error_handling_num |= 0x0008;
        }

        if ((-262144 < p_gp->sum_sig2) && (p_gp->sum_sig2 < WAF_LOWSIG_TH)) {
            p_gp->range1_status = p_gp->range1_status | LOW_SIG_BIT;
            p_gp->error_handling_num |= 0x0001;
        }
        

        // Flow_Number_13

        if ((dev->measurement_mode == LONG_RANGE_MODE) &&
                (p_gp->amb_fx28p <= (9 * FIXED28P_DEC)) && (p_gp->sum_sig_fx28p >= (9 * FIXED28P_DEC))) {
            p_gp->range1_status = p_gp->range1_status & LOW_SN_BIT_MASK;
            p_gp->error_handling_num |= 0x0400;
        }

        // Flow_Number_21
        pw_ret = p_gp->sum_sig_fx28p * RANGECOEF8 * ONETHOUSAND / (p_gp->rethistmax_fx28p - p_gp->amb_fx28p);
        if (pw_ret >= 7500) {
            p_gp->range1_status |= LOW_SN_BIT;
            p_gp->error_handling_num |=0x0100;
        }

        // Flow_Number_14
        if ((p_gp->range1_status & HXTALK_BIT) == HXTALK_BIT) {
            if (p_gp->prst_hxtalk < 2) {
                p_gp->range1_status = p_gp->range1_status & HXTALK_BIT_MASK;
                p_gp->prst_hxtalk += 1;
            }
        } else {
            p_gp->prst_hxtalk = 0;
        }
        // Flow_Number_20
        if ((p_gp->maxbin_ret < p_gp->maxbin_ref) && ((p_gp->ret_sig >> 1) >= p_gp->sum_sig2)) {
            p_gp->range1_status = p_gp->range1_status | LOW_SIG_BIT;
            p_gp->error_handling_num |= 0x0010;
        }
        // Flow_Number_23
        if ((p_gp->maxbin_ret < p_gp->maxbin_ref) && (p_gp->ret_sig < RETURN_SIG_TH500)) {
            p_gp->range1_status = p_gp->range1_status | LOW_SIG_BIT;
            p_gp->error_handling_num |= 0x1000;
        }

    } else {
        //Flow_Number_2
        p_gp->range1 = 8888;
        p_gp->range1_status |= ERROR_CALCULATION;
        p_gp->error_handling_num |= 0x0080;
    }

    /* near bit mask */
    // Flow_Number_19
    p_gp->range1_status = p_gp->range1_status & NEAR_BIT_MASK;

    if (p_gp->range1_status != VALID_DATA) {
        if (p_gp->flag_update == 0) {
            //Flow_Number_15

            if ((dev->measurement_mode == SHORT_RANGE_MODE)
                    && ((p_gp->range1_status & WAF_BIT_MASK & LOW_SN_BIT_MASK) & LOW_SIG_BIT) == LOW_SIG_BIT) {
                p_gp->flag_short2long_lowsig = 1;
            }

            // Flow_Number_11
            p_gp->flag_update = 1;
            p_gp->range1 = p_gp->range1_pre;
            p_gp->range1_status = p_gp->range1_status_pre;
        } else {
            ClearMedianRange(p_gp);
            p_gp->range1 = ERROR_RESULT;
        }
    } else {
        // Flow_Number_12
        if ((p_gp->range1 > RANGEZEROTH2900) ||
                (p_gp->range1 < RANGE0MM)) {
            p_gp->range1 = RANGE0MM;
            p_gp->error_handling_num |= 0x0020;
        }
        // Flow_Number_22
        if ((p_gp->range1 !=ERROR_RESULT)&&
                (p_gp->offset_calibration == 0)&&(p_gp->xtalk_calibration == 0)){
            if(p_gp->range1 >= p_gp->range1_pre){
                if(p_gp->range1 - p_gp->range1_pre >= (int16_t) (p_gp->d_range)){
                    ClearMedianRange(p_gp);
                }
            }else{
                if(p_gp->range1_pre - p_gp->range1 >= (int16_t) (p_gp->d_range)){
                    ClearMedianRange(p_gp);
                }
            }
        }
        // Flow_Number_17
        GetMedianRange(p_gp);

        p_gp->range1_pre = p_gp->range1;
        p_gp->range1_status_pre = p_gp->range1_status;
        p_gp->flag_update = 0;

    }

}

/**
* @brief calculate PARA_S_NEAR and PARA_S_FAR
* @param[in] *dev pointer to the structure of sensor
* @note only GP2AP02VT
*/
int32_t CalcParaS(GP2AP0XVT_Dev_t *dev)
{
    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);
    int32_t delta_bin_norm, delta_bin_saw, para_s_far;
    int32_t result;
    
    if (dev->measurement_mode == LONG_RANGE_MODE) {
        if(p_gp->delta_bin > PARA_S_JUDG_L){
            delta_bin_norm = (p_gp->delta_bin - PARA_S_NEAR3_L) / PARA_S_FAR1_L;
            delta_bin_saw =  ((p_gp->delta_bin - PARA_S_NEAR3_L) 
                    * ONETHOUSAND / PARA_S_FAR1_L) - delta_bin_norm * ONETHOUSAND;
            if(((uint8_t)(delta_bin_norm / 1000) & 1) == 0){ 
                para_s_far = PARA_S_FAR2_L;
            }else{
                para_s_far = -PARA_S_FAR2_L;
            }
            result = para_s_far * (ONETHOUSAND- delta_bin_saw) / ONETHOUSAND 
                    * delta_bin_saw / ONETHOUSAND + PARA_S_NEAR4_L;
        }else{
            result = PARA_S_NEAR1_L * (p_gp->delta_bin - PARA_S_NEAR2_L) / ONETHOUSAND 
                    * (p_gp->delta_bin - PARA_S_NEAR3_L) / ONETHOUSAND + PARA_S_NEAR4_L;
        }    
    } else {
        result = PARA_S_NEAR1_S * (p_gp->delta_bin - PARA_S_NEAR2_S) / ONETHOUSAND *
                (p_gp->delta_bin - PARA_S_NEAR3_S) / ONETHOUSAND + PARA_S_NEAR4_S;
    }
    return result;    
}

/** 
 * @brief changes range mode automatically.
 * @param[in] *dev pointer to the structure of sensor
 * @details When the range is over the SHORT2LONG_RANGE or the object cannot be detected,
 * chages mode to LONG_RANGE_MODE.
 * When the range is under the LONG2SHORT_RANGE ,chages mode to SHORT_RANGE_MODE.
 * @note only GP2AP02VT
 */
void ModeSwitching(GP2AP0XVT_Dev_t *dev)
{
    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);

    if (p_gp->offset_calibration == 0) {
        if ((dev->measurement_mode == LONG_RANGE_MODE) &&
                ((p_gp->range1_status & HXTALK_BIT_MASK) == 0) &&
                (p_gp->range1 < LONG2SHORT_RANGE)) {
            ModeChangeLong2Short(dev);
        } else if ((dev->measurement_mode == SHORT_RANGE_MODE) && ((p_gp->range1_status & HXTALK_BIT_MASK) == 0) &&
                (((p_gp->range1 > SHORT2LONG_RANGE)) || (p_gp->flag_short2long_lowsig))) {

            ModeChangeShort2Long(dev);
            ClearMedianRange(p_gp);
            p_gp->flag_short2long_lowsig = 0;
            p_gp->flag_comp_rate = 1;
        }
    }
}

/**
 * @brief sets cross talk value  at mode switching.
 * @param[in,out] *dev pointer to the structure of sensor
 * @note for use inside the API,no need to call from outside.
 */
void XTalkAutomaticSet(GP2AP0XVT_Dev_t *dev)
{
    uint8_t flag_xtalk_min = 0;
    uint8_t wdata;
    int32_t xtalk_ref_rate_16p;
    uint8_t reg1Eh_pre;
    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);

    reg1Eh_pre = p_gp->reg1Eh;

    if (dev->measurement_mode == LONG_RANGE_MODE) {
        xtalk_ref_rate_16p = FloatingToFixed16p(p_gp->data_xtalk_ih, BIT14);
    } else if (dev->measurement_mode == SHORT_RANGE_MODE) {
        xtalk_ref_rate_16p = FloatingToFixed16p(p_gp->data_xtalk_il, BIT14);
    }

    flag_xtalk_min = XTalkUpdate(dev);

    if (dev->measurement_mode == LONG_RANGE_MODE){
        if(p_gp->flag_comp_rate){
            if(flag_xtalk_min == 0) {
                if ((xtalk_ref_rate_16p * p_gp->ref_sig) > (320 * 65536)) {
                    p_gp->reg1Eh = REG_1EH_SETTING0XFF;
                } else if ((xtalk_ref_rate_16p * p_gp->ref_sig) > (110 * 65536) && (p_gp->reg1Eh == REG_1EH_SETTING0XF4)) {
                    p_gp->reg1Eh = REG_1EH_SETTING0XF8;
                } else if ((xtalk_ref_rate_16p * p_gp->ref_sig) < (50 * 65536)) {
                    p_gp->reg1Eh = REG_1EH_SETTING0XF4;
                } else if ((xtalk_ref_rate_16p * p_gp->ref_sig) < (150 * 65536) && (p_gp->reg1Eh == REG_1EH_SETTING0XFF)) {
                    p_gp->reg1Eh = REG_1EH_SETTING0XF8;
                }
            }else{
                if ((xtalk_ref_rate_16p * p_gp->ref_sig) < (50 * 65536)) {
                    p_gp->reg1Eh = REG_1EH_SETTING0XF4;
                }
            }
        } else {
            if ((xtalk_ref_rate_16p * p_gp->ref_sig) > (800 * 65536)) {
                p_gp->reg1Eh = REG_1EH_SETTING0XF8;
            } else if ((xtalk_ref_rate_16p * p_gp->ref_sig) < (400 * 65536)) {
                p_gp->reg1Eh = REG_1EH_SETTING0XF4;
            } else if (p_gp->reg1Eh == REG_1EH_SETTING0XFF) {
                p_gp->reg1Eh = REG_1EH_SETTING0XF4;
            }
        }
    }else{
        p_gp->reg1Eh = REG_1EH_SETTING0XF4;
    }
    if (reg1Eh_pre != p_gp->reg1Eh) {
        wdata = p_gp->reg1Eh;
        gp2ap_i2c_write(dev, REG_1EH, &wdata, sizeof (wdata));
    }
}

/**
 * @brief sets cross talk value  at mode switching.
 * @param[in] *dev pointer to the structure of sensor
 * @note for use inside the API,no need to call from outside.
 */
uint8_t XTalkUpdate(GP2AP0XVT_Dev_t *dev)
{
    uint8_t flag_xtalk_min = 0;
    int32_t xtalk_std = 0;

    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);

    if (dev->measurement_mode == LONG_RANGE_MODE) {
        p_gp->data_xtalk = p_gp->data_xtalk_ih;
    } else {
        p_gp->data_xtalk = p_gp->data_xtalk_il;
    }

    if (p_gp->ref_sig >= SUMREFTH52500) {
        if (dev->measurement_mode == LONG_RANGE_MODE) {
            xtalk_std = XTALKMINVALUE0108;
        } else {
            xtalk_std = XTALKMINVALUE0066;
        }
    } else if (p_gp->ref_sig >= SUMREFTH47500) {
        if (dev->measurement_mode == LONG_RANGE_MODE) {
            xtalk_std = XTALKMINVALUE0123;
        } else {
            xtalk_std = XTALKMINVALUE0070;
        }
    } else if (p_gp->ref_sig >= SUMREFTH42500) {
        if (dev->measurement_mode == LONG_RANGE_MODE) {
            xtalk_std = XTALKMINVALUE0143;
        } else {
            xtalk_std = XTALKMINVALUE007C;
        }
    } else if (p_gp->ref_sig >= SUMREFTH37500) {
        if (dev->measurement_mode == LONG_RANGE_MODE) {
            xtalk_std = XTALKMINVALUE016C;
        } else {
            xtalk_std = XTALKMINVALUE008C;
        }
    } else if (p_gp->ref_sig >= SUMREFTH32500) {
        if (dev->measurement_mode == LONG_RANGE_MODE) {
            xtalk_std = XTALKMINVALUE01A0;
        } else {
            xtalk_std = XTALKMINVALUE00A0;
        }
    } else if (p_gp->ref_sig >= SUMREFTH27500) {
        if (dev->measurement_mode == LONG_RANGE_MODE) {
            xtalk_std = XTALKMINVALUE01E5;
        } else {
            xtalk_std = XTALKMINVALUE00BA;
        }
    } else if (p_gp->ref_sig>= SUMREFTH22500) {
        if (dev->measurement_mode == LONG_RANGE_MODE) {
            xtalk_std = XTALKMINVALUE0223;
        } else {
            xtalk_std = XTALKMINVALUE00E0;
        }
    } else if (p_gp->ref_sig >= SUMREFTH17500) {
        if (dev->measurement_mode == LONG_RANGE_MODE) {
            xtalk_std = XTALKMINVALUE026C;
        } else {
            xtalk_std = XTALKMINVALUE0118;
        }
    }else if (p_gp->ref_sig >= SUMREFTH12500) {
        if (dev->measurement_mode == LONG_RANGE_MODE) {
            xtalk_std = XTALKMINVALUE02E5;
        } else {
            xtalk_std = XTALKMINVALUE0175;
        }
    }else {
        if (dev->measurement_mode == LONG_RANGE_MODE) {
            xtalk_std = XTALKMINVALUE036C;
        } else {
            xtalk_std = XTALKMINVALUE0218;
        }
    }

    if (p_gp->data_xtalk <= xtalk_std) {
        p_gp->data_xtalk = xtalk_std;
        flag_xtalk_min = 1;
    }

    if (((dev->measurement_mode == SHORT_RANGE_MODE) && (p_gp->data_xtalk <= 0x0516)) ||
            ((dev->measurement_mode == LONG_RANGE_MODE) && (p_gp->data_xtalk <= 0x0547))) {
        if (p_gp->xtalk_calibration == 0) {
            SetXTalkCalibrationData(dev, p_gp->data_xtalk);
        }
    }

    return flag_xtalk_min;
}

/**
 * @brief gets average value of crosstalk.
 * @param[in] *dev pointer to the structure of sensor
 * @note for use inside the API,no need to call from outside.
 */
void GetXTalkDataAverage(GP2AP0XVT_Dev_t *dev)
{
    int8_t i, itr;
    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);

    /* 5 + 20 times */
    for (i = 0; i < NUMMEASUREMENTS5 + NUMMEASUREMENTS ; i++) {
        StartMeasurement(dev);

        DELAY_FUNC(MEASUREMENT_TIME33M);
        itr = 0;
        while(CheckMeasurementEndFlag(dev) != MEASUREMENT_ENDED){
            DELAY_FUNC(1);
             if(itr >= 9) {
                p_gp->num_xtalk_data = 0;
                p_gp->data_xtalk_factory = 0;
                return; 
            }
            itr++;
        }
        if(i >= NUMMEASUREMENTS5){
            GetMeasurementData(dev);
        }
        ClearMeasurementEndFlag(dev);
    }
    
    if (p_gp->num_xtalk_data != 0) {
        p_gp->data_xtalk_ave = p_gp->data_xtalk_ave / p_gp->num_xtalk_data;
    } else {

        p_gp->data_xtalk_ave = 0;
    }
    p_gp->data_xtalk_factory = Fixed16pToFloating(p_gp->data_xtalk_ave, BIT14);
}

/**
 * @brief sets offset calibration data to offset registers via i2c 
 * @param[in] *dev pointer to the structure of sensor
 * @note for use inside the API,no need to call from outside.
 */
void SetOffsetCalibrationData(GP2AP0XVT_Dev_t *dev)
{
    uint8_t wdata1;
    uint8_t wdata2;
    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);

    if (dev->measurement_mode == LONG_RANGE_MODE) {
        wdata1 = (uint8_t) p_gp->offset_long1;
        wdata2 = (uint8_t) p_gp->offset_long2;
    } else {
        wdata1 = (uint8_t) p_gp->offset_short1;
        wdata2 = (uint8_t) p_gp->offset_short2;
    } 

    gp2ap_i2c_write(dev, REG_0FH, &wdata1, sizeof (wdata1));
    gp2ap_i2c_write(dev, REG_10H, &wdata2, sizeof (wdata2));
}

/**
 * @brief sets crosstalk calibration data to offset registers via i2c 
 * @param[in] *dev pointer to the structure of sensor
 * @note for use inside the API,no need to call from outside.
 */
void SetXTalkCalibrationData(GP2AP0XVT_Dev_t *dev, uint32_t xtalk)
{

    uint8_t wdata;

    wdata = (uint8_t) (xtalk & MASK_LSB);
    gp2ap_i2c_write(dev, REG_16H, &wdata, sizeof (wdata));
    wdata = (uint8_t) ((xtalk & MASK_MSB) >> BITSHIFT8);
    gp2ap_i2c_write(dev, REG_17H, &wdata, sizeof (wdata));
}

/**
 * @brief gets avarage data for clibration 
 * @param[in] *dev pointer to the structure of sensor
 * @note for use inside the API,no need to call from outside.
 */
void GetDataAverageForCalibration(GP2AP0XVT_Dev_t *dev)
{
    int8_t i;
    int8_t itr;
    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);

    p_gp->range1_ave = 0;
    p_gp->range2_ave = 0;
    p_gp->range1_status_or = 0;

    /* 20 times */
    for (i = 0; i < NUMMEASUREMENTS; i++) {

        StartMeasurement(dev);

        DELAY_FUNC(MEASUREMENT_TIME33M);
        itr = 0;
        while(CheckMeasurementEndFlag(dev) != MEASUREMENT_ENDED){
            DELAY_FUNC(1);
             if(itr >= 9) {
                p_gp->range1_status_or = 0xFF;
                return; 
            }
            itr++;
        }

        GetMeasurementData(dev);
        ClearMeasurementEndFlag(dev);
        p_gp->range1_ave += p_gp->range1_raw;
        if(p_gp->range2 < 2000){
            p_gp->range2_ave += p_gp->range2;
        }        
        p_gp->range1_status_or |= p_gp->range1_status;
    }

    p_gp->range1_ave = p_gp->range1_ave / NUMMEASUREMENTS;
    p_gp->range2_ave = p_gp->range2_ave / NUMMEASUREMENTS;
}
/* @} */
/*! @name API to call */
/* @{ */

/**
 * @brief  performs offset calibration at factory. 
 * @param[in] *dev pointer to the structure of sensor
 * @param[in] distance distance form  the sensor to the object.
 * @return returns status  CALIBRATION_FAILED or CALIBRATION_PASSED.
 * @details  The offset value shift  is acceptable between  RANGEOFFSETMIN and  RANGEOFFSETMAX
 */
int8_t PerformOffsetCalibration(GP2AP0XVT_Dev_t *dev, int16_t distance)
{
    int8_t status = CALIBRATION_FAILED;
    int16_t offset_short1 = 0, offset_short2 = 0, offset_long1 = 0, offset_long2 = 0;
    uint8_t wdata, range1_status_or_short;
    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);

    /* ************* short range mode ************* */
    p_gp->offset_target = distance;
    p_gp->offset_short1 = 0;
    p_gp->offset_short2 = 0;

    if (InitData(dev, SHORT_RANGE_MODE) == INITIALIZATION_FAILED) {
        return CALIBRATION_FAILED;
    }

    p_gp->offset_calibration = 1;
    wdata = XTALK_UPDATE_OFF;
    gp2ap_i2c_write(dev, REG_45H, &wdata, sizeof (wdata));

    /* average of measurements */
    GetDataAverageForCalibration(dev);

    offset_short1 = distance - p_gp->range1_ave;
    offset_short2 = distance - p_gp->range2_ave;
    range1_status_or_short = p_gp->range1_status_or;

    /* ************* long range mode ************* */
    p_gp->offset_long1 = 0;
    p_gp->offset_long2 = 0;

    if (InitData(dev, LONG_RANGE_MODE) == INITIALIZATION_FAILED) {
        return CALIBRATION_FAILED;
    }
    p_gp->offset_calibration = 1;
    wdata = XTALK_UPDATE_OFF;
    gp2ap_i2c_write(dev, REG_45H, &wdata, sizeof (wdata));

    /* average of measurements */
    GetDataAverageForCalibration(dev);

    offset_long1 = distance - p_gp->range1_ave;
    offset_long2 = distance - p_gp->range2_ave;

    /* judgement */
    if ((RANGEOFFSETMIN < offset_short1) && (offset_short1 < RANGEOFFSETMAX) &&
            (RANGEOFFSETMIN < offset_short2) && (offset_short2 < RANGEOFFSETMAX) &&
            (RANGEOFFSETMIN < offset_long1) && (offset_long1 < RANGEOFFSETMAX) &&
            (RANGEOFFSETMIN < offset_long2) && (offset_long2 < RANGEOFFSETMAX) &&
            (p_gp->range1_status_or == VALID_DATA)&&(range1_status_or_short == VALID_DATA)) {
        status = CALIBRATION_PASSED;
        p_gp->offset_short1 = (int8_t)offset_short1;
        p_gp->offset_short2 = (int8_t)offset_short2;
        p_gp->offset_long1 = (int8_t)offset_long1;
        p_gp->offset_long2 = (int8_t)offset_long2;

        /* write offset data to sensor's registers */
        SetOffsetCalibrationData(dev);

    } else { /* failed */
        status = CALIBRATION_FAILED;
    }
    p_gp->offset_calibration = 0;
    wdata = XTALK_UPDATE_ON;
    gp2ap_i2c_write(dev, REG_45H, &wdata, sizeof (wdata));

    return status;
}

/** 
 * @brief  performs crosstalk calibration at factory. 
 * @param[in] *dev pointer to the structure of sensor
 * @return returns status CALIBRATION_PASSED or CALIBRATION_FAILED .
 * @note  Do not put anything in the range within 600mm from the sensor. 
 * The crosstalk value is output in floating point.Crosstalk value should be 
 * 0.00625(XTALK_MAXIMUM_CALIB:0x049A) or less. If it is more than that, 
 * you need to reconsider how to attach the panel.
 */
int8_t PerformXTalkCalibration(GP2AP0XVT_Dev_t *dev)
{
    int8_t status = 0;
    uint8_t wdata;
    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);

    InitData(dev, LONG_RANGE_MODE);

    p_gp->xtalk_calibration = 1;
    p_gp->data_xtalk_factory = XTALK_CALIB_SETTING;
    p_gp->data_xtalk_ave = 0;
    p_gp->num_xtalk_data = 0;

    SetXTalkCalibrationData(dev, p_gp->data_xtalk_factory);
    wdata = 0x0F;
    gp2ap_i2c_write(dev, REG_15H, &wdata, sizeof (wdata));

    /* average of measurements */
    GetXTalkDataAverage(dev);

    if ((p_gp->data_xtalk_factory < XTALK_MAXIMUM_CALIB) && (p_gp->num_xtalk_data != 0)) {
        status = CALIBRATION_PASSED;

    } else {
        status = CALIBRATION_FAILED;
    }

    p_gp->xtalk_calibration = 0;
    wdata = 0x01;
    gp2ap_i2c_write(dev, REG_15H, &wdata, sizeof (wdata));

    return status;
}
/* @} */

/*! @name for use inside the API */
/* @{ */

/**
 * @brief gets the internal voltage value 
 * @param[in] *dev pointer to the structure of sensor
 * @note for use inside the API,no need to call from outside.
 */
void GetDataVHVVS1(GP2AP0XVT_Dev_t *dev)
{

    uint8_t rdata[2];
    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);

    gp2ap_i2c_read(dev, REG_4AH, rdata, sizeof (rdata));

    p_gp->vspad = rdata[0];
    p_gp->vs1 = rdata[1];
}
/* @} */

/*! @name API to call */
/* @{ */

/**
 * @brief gets device id
 * @param[in] *dev pointer to the structure of sensor
 * @return  returns the chipID 
 */
int32_t GetDeviceID(GP2AP0XVT_Dev_t * p_dev)
{
    uint8_t rdata;

    uint32_t ret = gp2ap_i2c_read(p_dev, REG_41H, &rdata, sizeof (rdata));
    p_dev->data.device_id = rdata;
    return ret;
}

/**
 * @brief performs slave address trimming
 * @param[in] *dev pointer to the structure of sensor
 * @param[in] slave_address set slave address.
 * @return returns status TRIMMING_PASSED or TRIMMING_FAILED
 * @note Execute the program by setting the power supply voltage to 5 volts, 
 * the GPIO pin of only the target sensor is set to low voltage.
 */

int8_t PerformI2CSlaveAddressTrimming(GP2AP0XVT_Dev_t *dev, uint8_t slave_address)
{

    int8_t status = TRIMMING_PASSED;
    uint8_t rdata;
    uint8_t wdata;

    gp2ap_i2c_read(dev, REG_50H, &rdata, sizeof (rdata));
    wdata = 0xC0;
    gp2ap_i2c_write(dev, REG_00H, &wdata, sizeof (wdata));

    DELAY_FUNC(1);

    wdata = 0x05;
    gp2ap_i2c_write(dev, REG_54H, &wdata, sizeof (wdata));
    DELAY_FUNC(1);
    wdata = 0xF0;
    gp2ap_i2c_write(dev, REG_55H, &wdata, sizeof (wdata));
    DELAY_FUNC(1);

    wdata = rdata | (slave_address & 0x0F);
    gp2ap_i2c_write(dev, REG_56H, &wdata, sizeof (wdata));
    DELAY_FUNC(1);

    wdata = 0x00;
    gp2ap_i2c_write(dev, REG_00H, &wdata, sizeof (wdata));

    /* software reset */
    SoftwareReset(dev);

    wdata = 0x80;
    gp2ap_i2c_write(dev, REG_00H, &wdata, sizeof (wdata));
    DELAY_FUNC(2);
    wdata = 0x00;
    gp2ap_i2c_write(dev, REG_00H, &wdata, sizeof (wdata));

    gp2ap_i2c_read(dev, REG_50H, &rdata, sizeof (rdata));

    if ((rdata & 0x0F) != (slave_address & 0x0F)) {
        status = TRIMMING_FAILED;
    }
    return status;
}
/* @} */

