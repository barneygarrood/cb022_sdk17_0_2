/**
 * @file  gp2ap02vt_common.c
 * @brief Example functions to use API
 * @note This file is consists of four types of functions "Example functions to use API",
 * "Example functions to check parameter" ,"Functions for WINDOWS application" and 
 * "Functions for terminal soft "
 * Refer to "Example functions to use API" for code porting.
 */
#include "gp2ap02vt_common.h"
#include "gp2ap02vt_api.h"
#include "gp2ap02vt_i2c.h"
#define NRF_LOG_MODULE_NAME gp2ap02vt_common
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

/*
 * Example functions to use API
 *   func_start
 *   func_stop
 *   OutputRangeTerminal
 *   OffsetCalibration
 *   XTalkCalibration
 *   SetCalibrationDataFromStoreData
 */
/*! @name Example functions to use API */
/* @{ */

/**
 * @brief reads calbration data,initializes device,sets polling timer and starts device  
 * @param [in] *dev pointer to structure of sensor
 * @param [in] measurement_setting set measure range mode(SHORT_RANGE_MODE/LONG_RANGE_MODE[02VT only])
 * @note use APIs InitData() and StartMeasurement()
 */
void func_start(GP2AP0XVT_Dev_t *dev, uint8_t measurement_setting)
{
    //SetCalibrationDataFromStoreData(dev);
    if (InitData(dev, measurement_setting) == INITIALIZATION_PASSED) {
      NRF_LOG_INFO("INITIALIZATION");
      return;
    }

//    if (get_data_mode == POLLING_MODE) {
//        set_timer0(set_timer0_counts); /* Polling timer */
//        enable_interrupts(INT_TIMER0);
//    } else if (get_data_mode == INTERRUPT_MODE) {
//
//        enable_interrupts(INT_EXT_H2L); /* External interrupt */
//    }
//    enable_interrupts(GLOBAL); /* enable interrupt */

    StartMeasurement(dev);
    DELAY_FUNC(33);
}

/**
 * @brief stops polling timer,and clears device data. The device stops automatically.  
 * @param [in] *dev pointer to structure of sensor 
 */

void func_stop(GP2AP0XVT_Dev_t *dev)
{
//    disable_interrupts(INT_TIMER0);
//    disable_interrupts(INT_EXT_H2L);
//    disable_interrupts(GLOBAL);
    StopMeasurement(dev);
}

//#ifndef SIMPLEMODE
//
///**
// * @brief displays the measurement results on the terminal screen
// * @param[in,out] *dev pointer to structure of sensor 
// */
//void OutputRangeTerminal(GP2AP0XVT_Dev_t *dev)
//{
//    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);
//    if (dev->debug == 0) {
//        printf("%4Ld, ", p_gp->range1);
//        printf("0x%02LX, ", p_gp->range1_status);
//
//        if (dev->measurement_mode == LONG_RANGE_MODE) {
//            printf("L, ");
//        } else {
//            printf("S, ");
//        } 
//
//        printf("\r\n");
//    } else {
//        OutputRangeTerminalDebug(dev);
//    }
//
//}
//#else
//
///**
// * @brief displays the measurement results on the terminal screen with binary
// * @param[in,out] *dev pointer to structure of sensor 
// * @note data order:0xFF 0xFF range1(lsb) range1(msb) range1_status 
// */
//void OutputRangeTerminal(GP2AP0XVT_Dev_t *dev)
//{
//    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);
//    putchar(0xFF);//for separate other data  
//    putchar(0xFF);//for separate other data  
//    putchar((uint8_t)p_gp->range1);
//    putchar((uint8_t)(p_gp->range1>>8));
//    putchar((uint8_t)p_gp->range1_status);
//}
//
//#endif

/**
 * @brief  Performs offset calibration and record to NVM if successful.
 * @param [in] *dev pointer to structure of sensor
 * @note  NVM_ADDRESS1:offset_short1\n
 * NVM_ADDRESS2:offset_short2\n
 * NVM_ADDRESS3:offset_long1(GP2AP02VT only)\n
 * NVM_ADDRESS4:offset_long2(GP2AP02VT only)\n
 * NVM_ADDRESS7:factory_calibarated \n
 */
//void OffsetCalibration(GP2AP0XVT_Dev_t *dev)
//{
//    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);
////    printf("* * * Perform offset calibration * * * \r\n");
////    printf("Set white card at %ldmm\r\n", g_distance);
//    if (PerformOffsetCalibration(dev, g_distance) == CALIBRATION_PASSED) {
////        printf("offset_short1:%d\r\n", p_gp->offset_short1);
////        printf("offset_short2:%d\r\n", p_gp->offset_short2);
////
////        printf("offset_long1:%d\r\n", p_gp->offset_long1);
////        printf("offset_long2:%d\r\n", p_gp->offset_long2);
////
////        printf("OFFSET CALIBRATION PASSED\r\n");
//        /* store offset data to NVM */
//        WRITE_EEPROM(NVM_ADDRESS1, p_gp->offset_short1);
//        WRITE_EEPROM(NVM_ADDRESS2, p_gp->offset_short2);
//
//        WRITE_EEPROM(NVM_ADDRESS3, p_gp->offset_long1);
//        WRITE_EEPROM(NVM_ADDRESS4, p_gp->offset_long2);
//
//        WRITE_EEPROM(NVM_ADDRESS7, 0x01); //offset data writen  
//    } else {
////        printf("OFFSET CALIBRATION FAILED\r\n");
//    }
//}

/**
 * @brief  Performs cross talk calibration and record to NVM if successful.
 * @param [in] *dev pointer to structure of sensor
 * @note  * NVM_ADDRESS5:xtalk_data_lsb\n
 * NVM_ADDRESS6:xtalk_data_msb\n
 * NVM_ADDRESS7:factory_calibarated 
 */
//void XTalkCalibration(GP2AP0XVT_Dev_t *dev)
//{
//    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);
//    uint8_t wdata, rdata;
////    printf("* * * Perform crosstalk calibration * * * \r\n");
////    printf("Do not put anything in the range over 600mm from the sensor\r\n");
//    if (PerformXTalkCalibration(dev) == CALIBRATION_PASSED) {
////        printf("data_xtalk_factory:0x%04lX\r\n", p_gp->data_xtalk_factory);
////        printf("CROSSTALK CALIBRATION PASSED\r\n");
//        /* store xtalk data to NVM */
//        WRITE_EEPROM(NVM_ADDRESS5, (uint8_t) p_gp->data_xtalk_factory); /* lsb */
//        WRITE_EEPROM(NVM_ADDRESS6, (uint8_t) (p_gp->data_xtalk_factory >> BITSHIFT8)); /* msb */
//        rdata = READ_EEPROM(NVM_ADDRESS7);
//        wdata = rdata | 0x02;
//        WRITE_EEPROM(NVM_ADDRESS7, wdata); //xtalk data writen
//
//    } else {
////        printf("CROSSTALK CALIBRATION FAILED\r\n");
//    }
////    printf("data_xtalk_ave:%ld\r\n", p_gp->data_xtalk_ave);
//}

/** reads calibration data from NVM  and set parameter for sensor
 * @param[out] p_gp structure of sensor
 * @note NVM_ADDRESS1:offset_short1\n
 * NVM_ADDRESS2:offset_short2\n
 * NVM_ADDRESS3:offset_long1\n
 * NVM_ADDRESS4:offset_long2\n
 * NVM_ADDRESS5:xtalk_data_lsb\n
 * NVM_ADDRESS6:xtalk_data_msb\n
 * NVM_ADDRESS7:factory_calibarated 
 */
//void SetCalibrationDataFromStoreData(GP2AP0XVT_Dev_t *dev)
//{
//    int8_t data1, data2, data3, data4, data5, data6, data7;
//    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);
//
//    data1 = READ_EEPROM(NVM_ADDRESS1);
//    data2 = READ_EEPROM(NVM_ADDRESS2);
//
//    data3 = READ_EEPROM(NVM_ADDRESS3);
//    data4 = READ_EEPROM(NVM_ADDRESS4);
//
//    data5 = READ_EEPROM(NVM_ADDRESS5);
//    data6 = READ_EEPROM(NVM_ADDRESS6);
//    data7 = READ_EEPROM(NVM_ADDRESS7);
//
//    p_gp->offset_short1 = (int8_t) data1;
//    p_gp->offset_short2 = (int8_t) data2;
//
//    p_gp->offset_long1 = (int8_t) data3;
//    p_gp->offset_long2 = (int8_t) data4;
//
//    p_gp->data_xtalk_factory = ((uint16_t) data6 << BITSHIFT8) | (uint8_t) data5;
//    p_gp->factory_calibrated = (int8_t) data7;
//
//}
/* @} */


/*
 * Example functions to check parameter for debug
 *  CheckParams
 *  OutputRangeTerminalDebug
 * 
 */

/*! @name Example functions to check parameter for debug */
/* @{ */

/** 
 * @brief conforms setting params.
 * @param[in] p_gp pointer to structure of sensor
 */
void CheckParams(GP2AP0XVT_Dev_t *dev)
{
    uint8_t rdata[88];
    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);

    gp2ap_i2c_read(dev, REG_00H, &rdata[REG_00H], WORD_0X37);
    gp2ap_i2c_read(dev, REG_41H, &rdata[REG_41H], WORD_14);
    gp2ap_i2c_read(dev, REG_50H, &rdata[REG_50H], WORD_8);

    GetDataVHVVS1(dev);
    //SetCalibrationDataFromStoreData(dev);

    uint8_t i;
//    printf("Software ver:%s\r\n", SOFT_REVISION);
//    printf("Device ID:%2X\r\n", GetDeviceID(dev));
//    printf("Internal offset:%d\r\n", p_gp->offset_internal);
//    printf("Pll:%d\r\n", p_gp->pll);
//    printf("Offset_short1:%d, 0x%02X\r\n", p_gp->offset_short1, p_gp->offset_short1);
//    printf("Offset_short2:%d, 0x%02X\r\n", p_gp->offset_short2, p_gp->offset_short2);
//
//    printf("Offset_long1:%d, 0x%02X\r\n", p_gp->offset_long1, p_gp->offset_long1);
//    printf("Offset_long2:%d, 0x%02X\r\n", p_gp->offset_long2, p_gp->offset_long2);
//
//    printf("Data_xtalk:0x%04lX\r\n", p_gp->data_xtalk);
//    printf("Data_xtalk_factory:0x%04lX\r\n", p_gp->data_xtalk_factory);
//    printf("VHV:0x%02X, VS1:0x%02X\r\n", p_gp->vspad, p_gp->vs1);
//    printf("Factory_calibrated:0x%02X\r\n", p_gp->factory_calibrated);
//    for (i = 0; i < 44; i++) {
//        printf("%2XH: 0x%2X, %2XH: 0x%2X\r\n", i * 2, rdata[i * 2], i * 2 + 1, rdata[i * 2 + 1]);
//    }

}

/**
 * @brief displays the measurement results for debug on the terminal screen
 * @param[in] *dev pointer to structure of sensor 
 */
//void OutputRangeTerminalDebug(GP2AP0XVT_Dev_t *dev)
//{
//    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);
//
//    // Raw Range Information
//    printf("%4ld,0x%02X,%ld,", p_gp->range1_raw, p_gp->range1_status_raw, p_gp->range2);
//    printf(",0x%02X,",p_gp->spad_area);
//    // XtalkData
//    printf("0x%04lX,0x%04lX,", p_gp->data_xtalk, p_gp->data_xtalk_pre);
//    // Range Parameter
//    printf("%4ld,0x%02X,", p_gp->range1, p_gp->range1_status);
//    // Error Information
//    printf("%d,0x%04lX,", p_gp->flag_update, p_gp->error_handling_num);
//    // Mode Information
//    if (dev->measurement_mode == LONG_RANGE_MODE) {
//        printf("L,");
//    } else {
//        printf("S,");
//    } 
//    printf("%ld,%ld,", p_gp->sum_sig_fx28p, p_gp->amb_fx28p);
//    printf("%ld,%ld,", p_gp->sum_ref_fx28p, p_gp->amb_ref_fx28p);
//    printf("%d,%d,", p_gp->maxbin_ret, p_gp->maxbin_ref);
//    printf("%ld,%ld,%ld,", p_gp->fine_ret_fx16p, p_gp->fine_ref_fx16p, p_gp->pw_ref_fx16p);
//    printf("%ld,0x%4X,%ld,%ld,", p_gp->frate_fx28p, p_gp->hist39binfp, p_gp->hist_ref39_fx28p, p_gp->refhist_fx28p[0]);
//    printf("%ld,%ld,%ld,%ld,", p_gp->refhist_fx28p[1],p_gp->refhist_fx28p[2], p_gp->refhist_fx28p[3], p_gp->refhist_fx28p[4]);
//    printf("%ld,%ld,%ld,", p_gp->refhist_fx28p[5],p_gp->ret_maxcnt_fx28p, p_gp->rethistmax_fx28p);
//    printf("%ld,%ld,",  p_gp->pw_ref3, p_gp->frate_sig);
//    printf("%ld,%ld,%ld,", p_gp->fine_ref_slope_x, p_gp->fine_ref_cor4, p_gp->collect_f);
//    printf("%ld,%ld,%ld,", p_gp->pw_ret, p_gp->delta_bin, p_gp->correction_slop);
//    printf("%d,%d,%d,%d,", p_gp->pll, p_gp->offset_internal, p_gp->offset_short1,p_gp->offset_short2);
//    printf("%d,%d,", p_gp->offset_long1,p_gp->offset_long2);
//    printf("\r\n");
//}

/* @} */


/*
 * Functions for our WINDOWS application
 * void ReadSRAM(GP2AP0XVT_Dev_t *dev)
 * void GetRawData(GP2AP0XVT_Dev_t *dev)
 * 
 */

/*! @name Functions for our WINDOWS application (ignore these) */
/* @{ */

/**
 * reads data from the sensor and send to windows 
 * @param[in] *dev pointer to structure of sensor 
 */

void ReadSRAM(GP2AP0XVT_Dev_t *dev)
{
    uint8_t wdata;
    uint8_t flag_data;
    uint8_t range_data[12];
    uint8_t raw_data[3];
    uint8_t i = 0;
    uint8_t n = 0;

    //SensorON & Measurement
    wdata = ACTIVE;
    gp2ap_i2c_write(dev, REG_00H, &wdata, sizeof (wdata));
    DELAY_FUNC(33);

    //RangeRead
    gp2ap_i2c_read(dev, REG_01H, &flag_data, sizeof (flag_data));
    gp2ap_i2c_read(dev, REG_2CH, range_data, sizeof (range_data));
//    if (output_mode == WINDOWS_APP_MODE) {
//        printf("SC");
//    }
//    printf("%02LX", flag_data);
//    for (i = 0; i < 12; i++) {
//        printf("%02LX", range_data[i]);
//    }

    for (n = 0; n < WORD_120; n++) {
        wdata = n | RET_SIGNAL;
        gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
        gp2ap_i2c_read(dev, REG_38H, raw_data, sizeof (raw_data));
//        printf("%02LX%02LX", raw_data[1], raw_data[2]);
        //free(raw_data);
    }
    for (n = 0; n < WORD_120; n++) {
        wdata = n | REF_SIGNAL;
        gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
        gp2ap_i2c_read(dev, REG_38H, raw_data, sizeof (raw_data));
        //printf("%02LX%02LX", raw_data[1], raw_data[2]);
        //free(raw_data);
    }

//    if (output_mode == WINDOWS_APP_MODE) {
//
//        printf("P");
//    }

    wdata = INITIAL_SETTING;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
}

/**
 * reads data from the sensor and send to windows 
 * @param[in] *dev pointer to structure of sensor 
 */
void GetRawData(GP2AP0XVT_Dev_t *dev)
{
    uint8_t rdata_00h;
    uint8_t rdata_01h;
    uint8_t rdata_data[6];
    uint8_t rdata[3];
    uint8_t wdata;
    uint8_t i;
    GP2AP0XVT_Data_t *p_gp = GP2AP0XVT_DevDataGet(dev);

    //printf("SC");
    gp2ap_i2c_read(dev, REG_00H, &rdata_00h, sizeof (rdata_00h));
    gp2ap_i2c_read(dev, REG_01H, &rdata_01h, sizeof (rdata_01h));
    gp2ap_i2c_read(dev, REG_2CH, rdata_data, sizeof (rdata_data));

//    if (rdata_00h != SHUTDOWN)
//        printf("Error 00h:0x00\r\n");
//
//    printf("%02LX", rdata_01h);
//
//    for (i = 0; i < 6; i++) {
//
//        printf("%02LX", rdata_data[i]);
//    }

    wdata = REG_100D | RET_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
    p_gp->maxbin_ret = rdata[1];
//    printf("%02LX%02LX", rdata[1], rdata[2]);

    wdata = REG_102D | RET_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
//    printf("%02LX%02LX", rdata[1], rdata[2]);

    wdata = REG_100D | REF_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
    p_gp->maxbin_ref = rdata[1];
//    printf("%02LX%02LX", rdata[1], rdata[2]);

    wdata = REG_102D | REF_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
//    printf("%02LX%02LX", rdata[1], rdata[2]);

    wdata = REG_103D | REF_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
//    printf("%02LX%02LX", rdata[1], rdata[2]);

    wdata = REG_39D | RET_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
//    printf("%02LX%02LX", rdata[1], rdata[2]);

    wdata = REG_108D | RET_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
//    printf("%02LX%02LX", rdata[1], rdata[2]);

    wdata = REG_108D | REF_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
//    printf("%02LX%02LX", rdata[1], rdata[2]);

    wdata = REG_106D | RET_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
    printf("%02LX%02LX", rdata[1], rdata[2]);

    wdata = REG_106D | REF_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
//    printf("%02LX%02LX", rdata[1], rdata[2]);

    wdata = (p_gp->maxbin_ref - 1) | REF_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
//    printf("%02LX%02LX", rdata[1], rdata[2]);

    wdata = REG_109D | RET_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
//    printf("%02LX%02LX", rdata[1], rdata[2]);

    wdata = p_gp->maxbin_ret | RET_SIGNAL;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));
    gp2ap_i2c_read(dev, REG_38H, rdata, sizeof (rdata));
//    printf("%02LX%02LX", rdata[1], rdata[2]);

    wdata = INITIAL_SETTING;
    gp2ap_i2c_write(dev, REG_38H, &wdata, sizeof (wdata));

//    printf("P");
}

/* @} */

/*
 * Functions for terminal soft 
 */

/*! @name Functions for the terminal soft   */
/* @{ */

/**
 * HexToDec one char 
 */
uint16_t m_HexToDec(uint8_t ch)
{
    uint16_t dec;

    if ((ch >= '0') && (ch <= '9')) {
        dec = ch - '0';
    } else if ((ch >= 'A') && (ch <= 'Z')) {
        dec = 10 + ch - 'A';
    } else if ((ch >= 'a') && (ch <= 'z')) {
        dec = 10 + ch - 'a';
    } else {

        dec = 0;
    }
    return dec;
}

/**
 * GetDataHex one char 
 */
uint16_t m_GetData_hex()
{
    uint16_t data = 0;
    uint8_t buf[3] = {0, 0, 0};

    buf[0] = getchar();
    buf[1] = getchar();
    buf[2] = getchar();

    if ((buf[2] == 'h') | (buf[2] == 'H')) {

        data = m_HexToDec(buf[0]) * 16 + m_HexToDec(buf[1]);
    }
    return data;
}

/**
 * GetDataHex one char without h/H 
 */
uint16_t m_GetData_hex_woh()
{
    uint16_t data = 0;
    uint8_t buf[2] = {0, 0};

    buf[0] = getchar();
    buf[1] = getchar();

    data = m_HexToDec(buf[0]) * 16 + m_HexToDec(buf[1]);

    return data;
}

/**
 * GetNumber one char 
 */
uint8_t m_GetNum_1()
{
    uint8_t buf1;

    buf1 = getchar();
    buf1 = m_HexToDec(buf1);

    return buf1;
}

/**
 * GetNumber two char 
 */
uint16_t m_GetNum_2()
{
    uint16_t data = 0;
    uint8_t buf2[2];

    buf2[0] = getchar();
    buf2[1] = getchar();
    data = m_HexToDec(buf2[0])*10 + m_HexToDec(buf2[1]);

    return data;
}

/**
 * GetNumber three char 
 */
uint16_t m_GetNum_3()
{
    uint16_t data = 0;
    uint8_t buf3[3];

    buf3[0] = getchar();
    buf3[1] = getchar();
    buf3[2] = getchar();
    data = m_HexToDec(buf3[0])*100 + m_HexToDec(buf3[1])*10 + m_HexToDec(buf3[2]);

    return data;
}

/** 
 * help menu.
 */
//void help_menu()
//{
//
//    printf("HELP\r\n");
//    printf("4 : Change operation mode\r\n");
//    printf("c : Check parameters\r\n");
//    printf("l : Debug mode on <-> off\r\n");
//    printf("o : Perform offset calibration at factory\r\n");
//    printf("x : Perform xtalk calibration at factory\r\n");
//    printf("f : Software revision\r\n");
//    printf("h : Help\r\n");
//    printf("r : Read from NVM\r\n");
//    printf("z : Write zero to NVM\r\n");
//    printf("b : Start measurement\r\n");
//    printf("s : Stop measurement\r\n");
//    printf("t : Set distance of offset calibration\r\n");
//}

/**
 * Process keyboard input on the terminal screen
 * Change operation mode
 */
//void kb_operation(GP2AP0XVT_Dev_t *dev)
//{
//    uint8_t kb_value;
//    uint8_t reg;
//
//    /* Keyboard input */
//    kb_value = kbhit();
//    if (kb_value == 1) {
//
//        reg = m_GetNum_1();
//
//        disable_interrupts(GLOBAL);
//
//        if (reg == KEYBOARD_h) { /* "h" Help */
//            help_menu();
//        } else if (reg == KEYBOARD_0) { /* I2C write mode */
//            kb_i2c_write(dev);
//        } else if (reg == KEYBOARD_1) { /* I2C read mode */
//            kb_i2c_read(dev);
//        } else if (reg == KEYBOARD_4) { /* Change operation mode */
//            ChangeOperationMode(dev);
//        } else if (reg == KEYBOARD_5) { /* for win app */
//            GetRawData(dev);
//        } else if (reg == KEYBOARD_6) { /* I2C sequential write */
//            kb_i2c_write_multi(dev);
//        } else if (reg == KEYBOARD_7) { /* I2C sequential read */
//            kb_i2c_read_multi(dev);
//        } else if (reg == KEYBOARD_8) { /* for win app */
//            ReadSRAM(dev);
//        } else if (reg == KEYBOARD_9) { /* Change i2c slave address */
//            reg = m_GetData_hex();
//            dev->slave_addr = reg;
//        } else if (reg == KEYBOARD_c) { /* Check params */
//            CheckParams(dev);
//        } else if (reg == KEYBOARD_l) { /* Debug mode */
//            if (dev->debug == 1)
//                dev->debug = 0;
//            else if (dev->debug == 0)
//                dev->debug = 1;
////            printf("debug: %d\r\n", dev->debug);
//        } else if (reg == KEYBOARD_f) {
////            printf("Software ver:%s\r\n", SOFT_REVISION);
//        } else if (reg == KEYBOARD_o) {
//            OffsetCalibration(dev);
//        } else if (reg == KEYBOARD_x) {
//            XTalkCalibration(dev);
//        } else if (reg == KEYBOARD_z) { /* reset NVM */
//            kb_write_0_NVM(dev);
//        } else if (reg == KEYBOARD_e) {
//            output_high(PIN_C1); /* en pin high for debug */
//        } else if (reg == KEYBOARD_d) {
//            output_low(PIN_C1); /* en pin low for debug */
//        } else if (reg == KEYBOARD_r) {
//            SetCalibrationDataFromStoreData(dev);
//            /* comment out to prevent mistakes due to incorrect operation
//            } else if (reg == KEYBOARD_t) {
//                if (PerformI2CSlaveAddressTrimming(dev, 0x31) == TRIMMING_PASSED) {
//                    printf("TRIMMING PASSED\r\n");
//                } else {
//                    printf("TRIMMING FAILED\r\n");
//                }
//             */
//        } else if (reg == KEYBOARD_b) {
//            func_start(dev, dev->measurement_mode);
//        } else if (reg == KEYBOARD_s) {
//            func_stop(dev);
//        } else if (reg == KEYBOARD_t) {
////            printf("Input target distance 3digit[mm]\r\n");
//            g_distance = m_GetNum_3();
////            printf("target distance:%ld\r\n", g_distance);
//        } else {
////            printf("Invalid number - Exit debug\r\n");
//        }
//        enable_interrupts(GLOBAL);
//    }
//}

/**
 * Change the operation mode
 * @param[in,out] dev structure of sensor
 * There are three modes.
 * "410"- Command mode
 * "411"- Output results to terminal screen
 * "414"- Output results to terminal screen
 * "413"- For Windows application
 */
//void ChangeOperationMode(GP2AP0XVT_Dev_t *dev)
//{
//    uint8_t reg;
//    uint8_t escape = 0;
//
//    while (!escape) {
////        printf(" 1:Select output mode\r\n");
////        printf(" e:Exit debug menu\r\n\r\n");
//
//        reg = m_GetNum_1();
//        switch (reg) {
//        case KEYBOARD_e: /* case e */
//            printf("Exit menu\r\n");
//            escape = 1;
//            break;
//
//        case KEYBOARD_1:
//            printf("* * * Select output mode * * *\r\n");
//            printf("0: Shutdown(Command mode)\r\n");
//            printf("1: Output to terminal(long mode)\r\n");
//            printf("3: Windows application\r\n");
//            printf("4: Output to terminal(short mode)\r\n");
//            printf("Current: %1u\r\n", output_mode);
//
//            reg = m_GetNum_1();
//
//            if ((reg == COMMANDMODE) || (reg == TERMINALMODE_LONG) ||
//                    (reg == TERMINALMODE_SHORT) || (reg == WINDOWS_APP_MODE)) {
//                output_mode = reg;
//            } else {
//                printf("%u is prohibited. Changed to 0\r\n", reg);
//                output_mode = COMMANDMODE;
//            }
//            if (output_mode == COMMANDMODE) {
//                func_stop(dev);
//                printf("\r\nCommand mode\r\n");
//            } else if ((output_mode == TERMINALMODE_LONG) || (output_mode == TERMINALMODE_SHORT)) {
//                if (output_mode == TERMINALMODE_LONG) {
//                    func_start(dev, LONG_RANGE_MODE);
//                } else {
//                    func_start(dev, SHORT_RANGE_MODE);
//                }
//                output_mode = TERMINALMODE;
//            } else if (output_mode == WINDOWS_APP_MODE) {
//                func_stop(dev);
//                disable_interrupts(INT_TIMER0);
//                disable_interrupts(INT_EXT_H2L);
//                disable_interrupts(GLOBAL);
//                printf("\r\nClose terminal\r\n");
//            }
//            escape = 1;
//            break;
//
//        default:
//            printf("\r\n");
//            printf("Invalid number - Please reType\r\n");
//
//            break;
//        }
//    }
//    return;
//}

/**
 * gets data form terminal 
 */
//int16_t GetDataManualInput()
//{
//    int8_t input_data[10];
//    int16_t data;
//
//    gets(input_data);
//    data = atol(input_data);
//
//    return data;
//}

/**
 * i2c write function from keyboard 
 * @param[in,out] dev structure of sensor
 */
//void kb_i2c_write(GP2AP0XVT_Dev_t *dev)
//{
//    uint8_t *tarray;
//    uint16_t tsize;
//    uint8_t reg;
//
//    tsize = 1;
//    tarray = (uint8_t *) malloc(sizeof (uint8_t) * tsize);
//    reg = m_GetData_hex();
//    tarray[0] = m_GetData_hex();
//    gp2ap_i2c_write(dev, reg, tarray, tsize);
////    if (output_mode == WINDOWS_APP_MODE) {
////
////        printf("SOP");
////    }
//    free(tarray);
//}

/**
 * i2c multi write function from keyboard 
 * @param[in,out] dev structure of sensor
 */
//void kb_i2c_write_multi(GP2AP0XVT_Dev_t *dev)
//{
//    uint8_t *tarray;
//    uint16_t tsize;
//    uint8_t reg;
//    uint8_t i;
//
//    reg = m_GetData_hex();
//    tsize = m_GetNum_2();
//    tarray = (uint8_t *) malloc(sizeof (uint8_t) * tsize);
//    for (i = 0; i < tsize; i++) {
//
//        tarray[i] = m_GetData_hex_woh();
//        DELAY_FUNC(50);
//    }
//    gp2ap_i2c_write(dev, reg, tarray, tsize);
//    free(tarray);
//}

/**
 * i2c read function from keyboard 
 * @param[in,out] dev structure of sensor
 */
//void kb_i2c_read(GP2AP0XVT_Dev_t *dev)
//{
//    uint8_t *tarray;
//    uint16_t tsize;
//    uint8_t reg;
//    reg = m_GetData_hex();
//    tsize = 1;
//    tarray = (uint8_t *) malloc(sizeof (uint8_t) * tsize);
//    gp2ap_i2c_read(dev, reg, tarray, tsize);
////    if (output_mode == WINDOWS_APP_MODE) {
////        printf("SC%02LXP", tarray[0]);
////    } else {
////
////        printf("%02LX", tarray[0]);
////    }
//    free(tarray);
//}

/**
 * i2c multi read function from keyboard 
 * @param[in,out] dev structure of sensor
 */
//void kb_i2c_read_multi(GP2AP0XVT_Dev_t *dev)
//{
//    uint8_t *tarray;
//    uint16_t tsize;
//    uint8_t reg;
//    uint8_t i;
//
//    reg = m_GetData_hex();
//    tsize = m_GetNum_2();
//    tarray = (uint8_t *) malloc(sizeof (uint8_t) * tsize);
//    gp2ap_i2c_read(dev, reg, tarray, tsize);
//
////    if (output_mode == WINDOWS_APP_MODE) {
////        printf("SC");
////    }
////    for (i = 0; i < tsize; i++) {
////        printf("%02LX", tarray[i]);
////        delay_us(50);
////    }
//    if (output_mode == WINDOWS_APP_MODE) {
//
//        printf("P");
//    }
//    free(tarray);
//}
//
///** 
// * Set the value of NVM to 0.
// */
//void kb_write_0_NVM(GP2AP0XVT_Dev_t *dev)
//{
//
////    printf("Write zero to NVM\r\n");
//    WRITE_EEPROM(NVM_ADDRESS1, 0); /* offset_short1 */
//    WRITE_EEPROM(NVM_ADDRESS2, 0); /* offset_short2 */
//
//    WRITE_EEPROM(NVM_ADDRESS3, 0); /* offset_long1 */
//    WRITE_EEPROM(NVM_ADDRESS4, 0); /* offset_long2 */
//
//    WRITE_EEPROM(NVM_ADDRESS5, 0); /* xtalk_data_lsb */
//    WRITE_EEPROM(NVM_ADDRESS6, 0); /* xtalk_data_msb */
//    WRITE_EEPROM(NVM_ADDRESS7, 0); /* factory_calibrated */
//}

/* @} */
