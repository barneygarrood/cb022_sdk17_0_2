#ifndef GP2AP02VT_I2C_H_
#define GP2AP02VT_I2C_H_

#define I2CREADMODE        0x01
#include "gp2ap02vt_api.h"

int32_t gp2ap_i2c_read(GP2AP0XVT_Dev_t *dev, uint8_t word_address, uint8_t *rdata, uint8_t num);
int32_t gp2ap_i2c_write(GP2AP0XVT_Dev_t *dev, uint8_t word_address, uint8_t *wdata, uint8_t num);


#endif /* GP2AP02VT_I2C_H_ */
