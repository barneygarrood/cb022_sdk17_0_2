#ifndef GP2AP02VT_COMMON_H_
#define GP2AP02VT_COMMON_H_
#include <stdint.h>
#include "gp2ap02vt_api.h"

/* output mode */
//uint8_t output_mode;
#define COMMANDMODE        0    /* command mode on terminal */
#define TERMINALMODE_LONG  1    /* output results to terminal for LONG_RANGE_MODE */
#define WINDOWS_APP_MODE   3    /* windows app */
#define TERMINALMODE_SHORT 4    /* output results to terminal for SHORT_RANGE_MODE */
#define TERMINALMODE       5

/* POLLING_MODE or INTERRUPT MODE */
//uint8_t get_data_mode;
#define INTERRUPT_MODE                0
#define POLLING_MODE                  1

#ifdef __KERNEL__ 
#define NVM_ADDRESS1          "/data/misc/gp2ap03vt_offset_short1" // offset_short1
#define NVM_ADDRESS2          "/data/misc/gp2ap03vt_offset_short2" // offset_short2
#define NVM_ADDRESS3          "/data/misc/gp2ap03vt_offset_long1" // offset_long1
#define NVM_ADDRESS4          "/data/misc/gp2ap03vt_offset_long2" // offset_long2
#define NVM_ADDRESS5          "/data/misc/gp2ap03vt_xtalk_lsb" // xtalk lsb
#define NVM_ADDRESS6          "/data/misc/gp2ap03vt_xtalk_msb1" // xtalk msb
#define NVM_ADDRESS7          "/data/misc/gp2ap03vt_factory_calibrated" // factory_calibrated

#define READ_EEPROM(value1) gp2ap_read_data_from_file(value1) 
#define WRITE_EEPROM(value1, value2) gp2ap_write_data_file(value1, value2)  
#endif

#ifndef __KERNEL__ 
#define NVM_ADDRESS1          0x01 // offset_short1
#define NVM_ADDRESS2          0x02 // offset_short2
#define NVM_ADDRESS3          0x03 // offset_long1
#define NVM_ADDRESS4          0x04 // offset_long2
#define NVM_ADDRESS5          0x05 // xtalk lsb
#define NVM_ADDRESS6          0x06 // xtalk msb
#define NVM_ADDRESS7          0x07 // factory_calibrated
#define READ_EEPROM(value1) gp2ap_read_data_from_file(value1) 
#define WRITE_EEPROM(value1, value2) gp2ap_write_data_file(value1, value2)  
#endif

#ifdef ARDUINO_UNO
#define READ_EEPROM(value1) EEPROM.read(value1)
#define WRITE_EEPROM(value1, value2) EEPROM.write(value1, value2)
#endif


void func_start(GP2AP0XVT_Dev_t *, uint8_t);
void func_stop(GP2AP0XVT_Dev_t *);
void OutputRangeTerminal(GP2AP0XVT_Dev_t *);
void OffsetCalibration(GP2AP0XVT_Dev_t *);
void XTalkCalibration(GP2AP0XVT_Dev_t *);
void SetCalibrationDataFromStoreData(GP2AP0XVT_Dev_t *);

void CheckParams(GP2AP0XVT_Dev_t *);
void OutputRangeTerminalDebug(GP2AP0XVT_Dev_t *);

void ReadSRAM(GP2AP0XVT_Dev_t *);
void GetRawData(GP2AP0XVT_Dev_t *);
uint16_t m_HexToDec(uint8_t);
uint16_t m_GetData_hex();
uint16_t m_GetData_hex_woh();
uint8_t m_GetNum_1();
uint16_t m_GetNum_2();
uint16_t m_GetNum_3();
void help_menu();
void kb_operation(GP2AP0XVT_Dev_t *);
void ChangeOperationMode(GP2AP0XVT_Dev_t *);
int16_t GetDataManualInput();
void kb_i2c_write(GP2AP0XVT_Dev_t *);
void kb_i2c_write_multi(GP2AP0XVT_Dev_t *);
void kb_i2c_read(GP2AP0XVT_Dev_t *);
void kb_i2c_read_multi(GP2AP0XVT_Dev_t *);
void kb_write_0_NVM(GP2AP0XVT_Dev_t *);

#endif