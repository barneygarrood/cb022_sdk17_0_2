/////////////////////////////////////////////////////////////////////////
//Copyright (C) 2019 SHARP All Rights Reserved.
//This program is for modeling purposes only, and is not guaranteed.
//This program is the control of the GP2AP02VT(TOF sensor).
/////////////////////////////////////////////////////////////////////////
/**
 * @file  gp2ap02vt.c
 * @brief example of main flow to use GP2AP02VT
 * @note modify this flow according to the hardware to be used\n
 * This exsample made as a program for PIC18F26K22
 */
/*** hardware setting *******************************************************/
#include <18F26K22.h>
#fuses HSH,PRIMARY,NOPLLEN,NOFCMEN,NOWDT,NOPROTECT,NOBROWNOUT,PUT,NOLVP,NOPBADEN,NOXINST,NOIESO,MCLR

#use fast_io(B)

/* Internal clock */
#use delay(CLOCK = 20000000) 

/* I2C definition */
#use I2C(master, sda = PIN_C4, scl = PIN_C3, FAST = 450000, FORCE_HW)

/* RS232C define */
#use RS232(BAUD = 115200, XMIT = PIN_C6, RCV = PIN_C7)

#use fixed_io(c_outputs = PIN_C1, PIN_C2) // for debug
#include <stdlib.h>
#include <STDLIBM.H>    /* malloc */
#include ".\Typedef.h"

/* polling timer */
uint16_t set_timer0_counts;
uint16_t timer0_polling_msec;
/* Subroutine control */
uint8_t get_data_handler = 0;
uint8_t get_data_mode;
int16_t g_distance= 100;
//#define SIMPLEMODE

#include ".\gp2ap02vt.h"
#include ".\api\gp2ap02vt_api.c"
#include ".\gp2ap02vt_i2c.c"
#include ".\gp2ap02vt_common.c"

/* polling handler */
#INT_TIMER0

void timer0_isr()
{
    set_timer0(set_timer0_counts);
    get_data_handler = 1;
    return;
}

/* interrupt handler */
#INT_EXT

void int_handler()
{
    get_data_handler = 1;
    return;
}

void main()
{
    GP2AP0XVT_Dev_t dev;
    uint8_t status;

    help_menu();

    /*** Hardware setting for MCU *******************************************/
    dev.slave_addr = 0x29; /* ( 0x29 << 1 ) | X, write_mode:0x52, read_mode:0x53 */

    /* Set up timer for polling mode*/
    setup_timer_0(RTCC_INTERNAL | RTCC_DIV_256);
    timer0_polling_msec = POLLING_100MSEC;
    set_timer0_counts = 65536 - 20000000 / 4 / 256 * timer0_polling_msec / 1000;

    /* Disable interrupt & polling timer for pic */
    get_data_handler = 0;
    disable_interrupts(INT_EXT_H2L);
    disable_interrupts(INT_TIMER0);
    disable_interrupts(GLOBAL);

    /* Output mode: COMMANDMODE, TERMINALMODE_SHORT,TERMINALMODE_LONG,WINDOWS_APP_MODE */
    output_mode = COMMANDMODE;
    // output_mode = TERMINALMODE;
    // output_mode = WINDOWS_APP_MODE;

    /* Debug mode */
    dev.debug = 0;

    /* check device id */
    dev.data.device_id = GetDeviceID(&dev);
#ifndef SIMPLEMODE
    printf("device_id:0x%x\r\n", dev.data.device_id);
#endif
    get_data_mode = POLLING_MODE; /* INTERRUPT_MODE or POLLING_MODE */

    /* read factory calibration data from NVM */
    if (READ_EEPROM(NVM_ADDRESS7) == FACTORY_CALIBRATION_DONE) {
        /* If the calibration is completed, 
         * the host must read the calibration value from NVM and 
         * set it to the variable. */
#ifndef SIMPLEMODE
        printf("Factory calibrated\r\n");
#endif
        SetCalibrationDataFromStoreData(&dev);
    } else {
        /* Set NVM to 0 */
        WRITE_EEPROM(NVM_ADDRESS1, 0); /* offset_short1 8bit */
        WRITE_EEPROM(NVM_ADDRESS2, 0); /* offset_short2 8bit */

        WRITE_EEPROM(NVM_ADDRESS3, 0); /* offset_long1 8bit */
        WRITE_EEPROM(NVM_ADDRESS4, 0); /* offset_long2 8bit */

        WRITE_EEPROM(NVM_ADDRESS5, 0); /* data_xtalk lsb 8bit */
        WRITE_EEPROM(NVM_ADDRESS6, 0); /* data_xtalk msb 8bit */
        WRITE_EEPROM(NVM_ADDRESS7, 0); /* factory_calibrated 8bit */
        SetCalibrationDataFromStoreData(&dev);
    }

    /* Initialize */
    /* gp2ap02vt must be used with SHORT_RANGE_MODE or LONG_RANGE_MODE setting */
    if (InitData(&dev, LONG_RANGE_MODE) == INITIALIZATION_FAILED) {
#ifndef SIMPLEMODE
        printf("INITIALIZATION FAILED!!!. \r\n");
        printf("CHECK GPIO PIN VOLTATGE!!!. \r\n");
#endif
    }

#ifndef SIMPLEMODE
    /* Terminal output mode */
    if (output_mode == TERMINALMODE) {
        /* hardware setting */
        if (get_data_mode == POLLING_MODE) { /* Polling timer */
            set_timer0(set_timer0_counts);
            enable_interrupts(INT_TIMER0);
        } else if (get_data_mode == INTERRUPT_MODE) { /* External intrrupt */
            enable_interrupts(INT_EXT_H2L);
        }
        enable_interrupts(GLOBAL); /* enable interrupt */

        StartMeasurement(&dev); /* start measurement */
        DELAY_FUNC(MEASUREMENT_TIME33M);
    }
#endif
#ifdef SIMPLEMODE
    func_start(&dev, LONG_RANGE_MODE);
#endif    
    /* Main loop */
    for (;;) {

#ifndef SIMPLEMODE
        kb_operation(&dev); /* Keyboard operation for setting */
#endif
        /* Polling timer expiration or interrupt signal */
        if (get_data_handler) {

            status = CheckMeasurementEndFlag(&dev);

            if (status == MEASUREMENT_ENDED) {

                GetMeasurementData(&dev);

                ClearMeasurementEndFlag(&dev);
                status = MEASUREMENT_NOT_END;

                OutputRangeTerminal(&dev); /* output */

                StartMeasurement(&dev); /* restart */
            }
            get_data_handler = 0;
        }

    }
    return;
}
