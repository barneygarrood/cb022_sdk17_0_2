//char:1byte
typedef signed char int8_t;
typedef unsigned char uint8_t;
//int16:2byte
typedef signed int16 int16_t;
typedef unsigned int16 uint16_t;
//int32:4byte
typedef signed int32 int32_t;
typedef unsigned int32 uint32_t;
