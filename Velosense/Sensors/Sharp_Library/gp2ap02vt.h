#ifndef _GP2AP02VT_H_
#define _GP2AP02VT_H_

#define PIC18F26K22
//#define ARDUINO_UNO

#define POLLING_100MSEC    100

#define KEYBOARD_0         0 
#define KEYBOARD_1         1  
#define KEYBOARD_4         4 
#define KEYBOARD_5         5 
#define KEYBOARD_6         6 
#define KEYBOARD_7         7 
#define KEYBOARD_8         8 
#define KEYBOARD_9         9 
#define KEYBOARD_b         11
#define KEYBOARD_c         12
#define KEYBOARD_d         13
#define KEYBOARD_e         14
#define KEYBOARD_f         15
#define KEYBOARD_h         17
#define KEYBOARD_l         21
#define KEYBOARD_o         24
#define KEYBOARD_r         27
#define KEYBOARD_s         28
#define KEYBOARD_t         29
#define KEYBOARD_x         33
#define KEYBOARD_z         35


#endif //_GP2AP02VT_H_
