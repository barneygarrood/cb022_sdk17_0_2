/**
 * @file  gp2ap02vt_i2c.c
 * @brief Example functions for I2C
 * @note modify these functions according to the hardware to be used
 */
#include "gp2ap02vt_i2c.h"
#include "velo_twi.h"

/**
 * @brief Sequential I2C read 
 * @param [in] *dev pointer to structure of sensor
 * @param [in] word_address word_address 
 * @param [in] *rdata pointer to array for read data
 * @param [in] num bytes to read
 */
int32_t gp2ap_i2c_read(GP2AP0XVT_Dev_t *dev, uint8_t word_address, uint8_t *rdata, uint8_t num)
{
  ret_code_t ret;
  // Two transfers here - first to set register to read, second to read from it.
  nrf_twi_mngr_transfer_t trans[2];
  velo_twi_read_register_transfer(dev->slave_addr, &word_address, rdata, num, trans);

  // perform transfers.  This is blocking..
  ret = velo_twi_perform_transfers(trans,2);
  return ret;
}

/**
 * @brief Sequential I2C write 
 * @param [in] *dev pointer to structure of sensor
 * @param [in] word_address word_address 
 * @param [in] *wdata pointer to array for write data
 * @param [in] num bytes to write
 */
int32_t gp2ap_i2c_write(GP2AP0XVT_Dev_t *dev, uint8_t word_address, uint8_t *wdata, uint8_t num)
{
  ret_code_t ret;
  uint8_t buffer[16];
  buffer[0] = word_address;
  for (int i=0;i<num;i++)
  {
    buffer[i+1] = wdata[i];
  }
  nrf_twi_mngr_transfer_t trans[1];
  velo_twi_write_transfer(dev->slave_addr,buffer,num+1,trans);
  ret = velo_twi_perform_transfers(trans,1);

  return ret;

}
