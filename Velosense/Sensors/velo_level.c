#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_delay.h"
#include "bat_level.h"
#include "velo_us_timer.h"

#define TIMEOUT 2000   //2s timeout..

static uint8_t soc_register = 0x1C;
static uint8_t voltage_register = 0x04;

void bat_init(velo_bat_t battery)
{
  battery.last_poll_ms=0;
  battery.soc=0;
  battery.voltage=0;
  bat_gauge_init();
}

void bat_gauge_get_soc_transfer(uint8_t * p_data, nrf_twi_mngr_transfer_t * p_trans)
{
  twi_read_register_transfer(BAT_GAUGE_ADDRESS, &soc_register, p_data, 2, p_trans);
}


void bat_gauge_get_voltage_transfer(uint8_t * p_data, nrf_twi_mngr_transfer_t * p_trans)
{
  twi_read_register_transfer(BAT_GAUGE_ADDRESS, &voltage_register, p_data, 2, p_trans);
}


void bat_gauge_enter_shutdown_transfer(nrf_twi_mngr_transfer_t * p_trans)
{
  // enable shutdown mode.
   uint8_t reg[3]={0x00,0x1B,0x00};
   twi_write_transfer(BAT_GAUGE_ADDRESS,reg,3,&p_trans[0]);
  // enter shutdown mode.
   reg[1] = 0x1C;
   twi_write_transfer(BAT_GAUGE_ADDRESS,reg,3,&p_trans[1]);
}

int32_t bat_gauge_read(velo_bat_t * battery)
{
  // Setup 4 transfers to read two registers.
  // each read requires writing the register address, then reading the bytes
  nrf_twi_mngr_transfer_t trans[4];
  uint8_t buffer[4];  // two bytes each for soc and voltage:
  // Create transfers:
  bat_gauge_get_soc_transfer(&buffer[0],&trans[0]);
  bat_gauge_get_voltage_transfer(&buffer[2],&trans[2]);
  ret_code_t ret = twi_perform_transfers(trans,4);
  battery->soc = (uint8_t)uint16_decode(&buffer[0]);
  battery->voltage = uint16_decode(&buffer[2]);
  return ret;
}


static void write_cmd(uint8_t * p_cmd, uint8_t count)
{
  nrf_twi_mngr_transfer_t trans[1];
  twi_write_transfer(BAT_GAUGE_ADDRESS,p_cmd,count,trans);
  twi_perform_transfers(trans,1);
  // Note that we always require a little delay here to let sensor sort itself out.
}

static void enter_cfg_update()
{
  nrf_twi_mngr_transfer_t trans[1];
  uint8_t cmd[3]; uint8_t dat[1];
  cmd[0] = 0x00; cmd[1] = 0x13; cmd[2] = 0x00;
  write_cmd(&cmd[0],3);

  // wait until flag changes to show that we are in config update mode:
  uint32_t start = time_ms();
  cmd[0] = 0x06;
  twi_read_register_transfer(BAT_GAUGE_ADDRESS, &cmd[0], &dat[0], 1, &trans[0]);
  while(!(dat[0] & 0x10))
  { 
    twi_perform_transfers(&trans[0], 2);
    nrf_delay_ms(10);
  } 
}

static void reset()
{
  nrf_twi_mngr_transfer_t trans[1];
  uint8_t cmd[3]; uint8_t dat[1];
  cmd[0] = 0x00; cmd[1] = 0x42; cmd[2] = 0x00;
  write_cmd(&cmd[0],3);

  // wait until flag changes to show that we are in config update mode:
  uint32_t start = time_ms();
  cmd[0] = 0x06;
  twi_read_register_transfer(BAT_GAUGE_ADDRESS, &cmd[0], &dat[0], 1, &trans[0]);
  while(dat[0] & 0x10)
  { 
    twi_perform_transfers(&trans[0], 2);
    nrf_delay_ms(10);
  } 
}

static void read_register(uint8_t *p_data, uint8_t data_class, uint8_t offset, uint8_t data_len)
{
  nrf_twi_mngr_transfer_t trans[1];
  uint8_t cmd[2];
  cmd[0]=0x61; cmd[1] = 0x00;
  write_cmd(cmd,2);
  cmd[0]=0x3E; cmd[1] = data_class;
  write_cmd(cmd,2);
  uint8_t data_offset = offset<32 ? 0 : 1;
  cmd[0]=0x3F; cmd[1] = data_offset;
  write_cmd(cmd,2);
  //read register:
  cmd[0] = 0x40 + offset;
  twi_read_register_transfer(BAT_GAUGE_ADDRESS,&cmd[0],p_data, data_len,&trans[0]);
  twi_perform_transfers(trans, 2);
}

static void write_register(uint8_t *p_old_data, uint8_t *p_new_data, uint8_t data_class, uint8_t offset, uint8_t data_len)
{
  nrf_twi_mngr_transfer_t trans[1];
  uint8_t cmd[8];
  cmd[0]=0x61; cmd[1] = 0x00;
  write_cmd(cmd,2);
  cmd[0]=0x3E; cmd[1] = data_class;
  write_cmd(cmd,2);
  uint8_t data_offset = offset<32 ? 0 : 1;
  cmd[0]=0x3F; cmd[1] = data_offset;
  write_cmd(cmd,2);
  // Need a short delay here:
  // read old checksum:
  cmd[0] = 0x60;
  uint8_t old_chksum;
  twi_read_register_transfer(BAT_GAUGE_ADDRESS,&cmd[0],&old_chksum,1,&trans[0]);
  twi_perform_transfers(trans, 2);

  // Compute new checksum.
  uint8_t temp = 255-old_chksum;
  for (int i=0;i<data_len;i++)
  {
    temp-=p_old_data[i];
  }
  uint8_t new_chksum = 255 - temp;
  for (int i=0;i<data_len;i++)
  {
    new_chksum-=p_new_data[i];
  }

  // Write new values:
  cmd[0]=0X40 + offset;
  for (int i=0;i<data_len;i++)
  {
    cmd[i+1]=p_new_data[i];
  }
  write_cmd(cmd,data_len+1);

  // Write new checksum to register 0x60
  cmd[0] = 0x60;
  cmd[1] = new_chksum;
  write_cmd(cmd,2);
}


// Function to initialise battery gauge.
// This sets GPOUT interrupt to fire when SOC drops below a threshold (8%) and
// Clear when it goes back above another threshold(1%).
void bat_gauge_init()
{
  nrf_twi_mngr_transfer_t trans[2];

  uint8_t cmd[8];
  uint8_t dat[8];

  // Enter update mode:
  enter_cfg_update();
  // get old op config register values:
  uint8_t old_opconfig[2];
  read_register(old_opconfig,0x40,0x00,2);

  // Modify OpConfig flags: high byte bit 3->0 low byte bit 2->1:
  uint8_t new_opconfig[2];
  new_opconfig[0] = old_opconfig[0] & ~(1UL<<3);
  new_opconfig[1] = old_opconfig[1] | (1UL<<2);
  // Write new values:
  write_register(old_opconfig,new_opconfig,0x40,0x00,2);
  
  // get old SOC1 threashold data:
  uint8_t old_SOC1_threshold[2];
  read_register(old_SOC1_threshold, 0x31, 0x00, 2);
  // Write new values (8% set, 12% clear thresholds):
  uint8_t new_SOC1_threshold[2] = {8, 12};
  // Write new values:
  write_register(old_SOC1_threshold, new_SOC1_threshold, 0x31, 0x00, 2);

  // reset unit - gets out of update mode.
  reset();

}