#ifndef VELO_CONFIG_H__
#define VELO_CONFIG_H__

#define VELO_SW_REVISION_MAJ     4
#define VELO_SW_REVISION_MIN      8

#define VELO_HW_REVISION          0
#define VELO_MANUF_ID             296
#define VELO_MODEL_NO             0

// Sensor measurement loop runs on a timer based on poll_period.
// If time since last sensor measuerement is greater than MS values below, then measurement taken in that loop.
#define POLL_PERIOD 1024             // Measure data once every 240/32768=7.3ms = 136Hz.
#define LIDAR_POLL_PERIOD_MS  30     //NOTE Now only polling every 1/4s, need to make sure lidar poll period a little less than 1/4s so it happens always.
#define BAT_POLL_PERIOD_MS  1000      //Measure battery level once every 1s = 1000ms.

#define REG_PIN_ON       0       // If true, reg pin is high to turn on regulator (e.g. CB22), otherwise its low (e.g. CB25).

// LOG messages:
#define VELOSENSE_LOG_ENABLED 0
#define VELO_INSTALLED_FLASH_MEMORY 0

// ANT Parameters
#define VELO_CHANNEL_NUM 0
#define ANT_VELO_ACK_TIME  65540           //Amount of time in ticks (/32768) for which we should send acknowledge message.

#define VELO_DEFAULT_ZENITH_MODE          0x00  // 

#define ZENITH_MAX_BURST_ATTEMPT_COUNT    10 //

#define ADXL343_SAMPLE_COUNT 32

// Lidar addresses - they start off the same, then we change itl.
#define LIDAR_ORIG_ADDRESS 0x52
#define LIDAR_1_ADDRESS 0x66
#define LIDAR_2_ADDRESS 0x68
#define LIDAR_3_ADDRESS 0x70

//Define pins to use:
#define PIN_SCL		26
#define PIN_SDA		27

#define PIN_VL_1_XSHUT    23

#define PIN_VL_2_XSHUT    35

#define PIN_VL_3_XSHUT    20

#define PIN_REG         15
#define PIN_SW		30

#define PIN_LED1	5 // Red
#define PIN_LED2  7 // Blue
#define PIN_LED3	6 // Green

#define PIN_RESET 18

#define PIN_STAT_1  11
#define PIN_PG  8
#define PIN_GPOUT 4

#define VELO_BAT_SOC_OFF 15 // Device turns off when battery state of charge rises above this value (%)
#define VELO_BAT_SOC_ON 20 // Device turns on when battery state of charge rises above this value (%)
//#define VELO_BAT_VOLTAGE_OFF  3500
//#define VELO_BAT_VOLTAGE_ON  3600

#define ACCEL_INACTIVITY_THRESHOLD 0.5

#define ANT_COMMON_PAGE_70_LOG_ENABLED 0
#define ANT_COMMON_PAGE_80_LOG_ENABLED 0
#define ANT_COMMON_PAGE_81_LOG_ENABLED 0
#define ANT_AERO_PAGE_33_LOG_ENABLED 0
#define ANT_AERO_PAGE_224_LOG_ENABLED 0
#define ANT_AERO_PAGE_227_LOG_ENABLED 0
#define ANT_AERO_PAGE_228_LOG_ENABLED 0
#endif //VELO_CONFIG_H__
