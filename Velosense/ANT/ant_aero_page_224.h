#ifndef ANT_AERO_PAGE_224_H__
#define ANT_AERO_PAGE_224_H__

#include <stdint.h>
#include "sdk_common.h"
#define ANT_AERO_PAGE_224 (224)

// Packet structure
typedef struct 
{
  uint8_t new_file_count;
  uint8_t installed_memory;
  uint8_t used_memory[3];
  uint8_t fractional_voltage;
  struct
  {
    uint8_t coarse_voltage        :4;
    uint8_t status                :3;
    uint8_t reserved              :1;
  }descriptive_bit_field;
} ant_aero_page_224_data_layout_t;

// Page data structure
typedef struct
{
  uint8_t new_file_count;
  // Installed memory as a power of 2, in bytes.
  uint8_t installed_memory;
  uint32_t used_memory;
  // Voltage in 1/1000 V
  uint16_t voltage;
  // SOC in %
  uint8_t soc;
} ant_aero_page_224_data_t;

//Initialiser - start with zeros as these values will be replaced by real data once first measurements are taken.
#define ANT_AERO_PAGE_224_INIT()       \
    (ant_aero_page_224_data_t)       \
  {                                   \
    .new_file_count = 0,          \
    .used_memory = 0,                \
    .installed_memory = VELO_INSTALLED_FLASH_MEMORY,                \
    .soc=0,                           \
    .voltage=0,                       \
  }

    

// Functions
void ant_aero_page_224_encode(uint8_t * p_page_buffer, ant_aero_page_224_data_t * p_page_data);

void ant_aero_page_224_data_log(ant_aero_page_224_data_t const * p_page_data);

void ant_aero_page_224_update_battery(ant_aero_page_224_data_t * p_page_data, uint16_t voltage, uint8_t soc);
void ant_aero_page_224_update_memory(ant_aero_page_224_data_t * p_page_data, uint8_t new_file_count, uint32_t used_memory);

#endif // ANT_AERO_PAGE_224_H__