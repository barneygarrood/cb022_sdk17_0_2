#include "ant_aero_page_33.h"
#include "velo_config.h"

#define NRF_LOG_MODULE_NAME ant_aero_page_33
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
NRF_LOG_MODULE_REGISTER();

void ant_aero_page_33_decode(uint8_t * p_page_buffer, ant_aero_page_33_data_t * p_page_data)
{
  ant_aero_page_33_data_layout_t * p_page_in = (ant_aero_page_33_data_layout_t * ) p_page_buffer;

  p_page_data->cid = (p_page_in->reserved >> 5) & 0x07;
  p_page_data->command_type = p_page_in->command_type;
  p_page_data->command_data = uint24_decode(p_page_in->command_data);
  p_page_data->time_offset = uint16_decode(p_page_in->time_offset);
  if (ANT_AERO_PAGE_33_LOG_ENABLED)
  {
    ant_aero_page_33_data_log(p_page_data);
  }
}

void ant_aero_page_33_data_log(ant_aero_page_33_data_t * p_page_data)
{
    NRF_LOG_INFO("****************************** PAGE 33 START ******************************");
    NRF_LOG_INFO("cid:                                                  %u", p_page_data->cid);
    NRF_LOG_INFO("command_type:                                         %u", p_page_data->command_type);
    NRF_LOG_INFO("command_data:                                         %u", p_page_data->command_data);
    NRF_LOG_INFO("time_offset:                                          %u", p_page_data->time_offset);
}

