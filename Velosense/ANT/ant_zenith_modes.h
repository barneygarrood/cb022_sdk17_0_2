#ifndef ANT_ZENITH_MODES_H__
#define ANT_ZENITH_MODES_H__

#include <stdint.h>

typedef struct
{
  uint8_t id;
  uint8_t * p_foreground_page_list;
  uint8_t * p_background_page_list;
  uint8_t foreground_page_count;
  uint8_t background_page_count;
  uint8_t background_page_frequency;    //Number of cycles of foreground pages between which to put a background page.
  uint8_t background_replacement_page;  // index of foreground page sequence with which to overwrite with background page.
  
  uint8_t dev_channels    :1;
  uint8_t cda_calc    :1;
} ant_zenith_mode_t;

void ant_zenith_modes_update(uint8_t mode_number, ant_zenith_mode_t * p_mode);



#endif //ANT_AERO_MODES_H__