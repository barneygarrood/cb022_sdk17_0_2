#include "velo_ant_zenith.h"
#include "ant_interface.h"
#include "velo_status.h"
#include "velo_sensors.h"
#include "velo_settings.h"
#include "ant_zenith_modes.h"
#include "nrf_pwr_mgmt.h"

#define NRF_LOG_MODULE_NAME velo_ant_zenith
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

// ANT Velosense object declarations:
static void _ant_zenith_freeze_handler(ant_zenith_profile_t * p_profile, uint8_t packet_count);
static void _ant_zenith_evt_handler(ant_zenith_profile_t * p_profile, ant_zenith_page_t page);

VELO_CHANNEL_CONFIG_DEF(m_ant_zenith,VELO_CHANNEL_NUM,CHAN_ID_TRANS_TYPE,ANT_PUBLIC_NETWORK_NUM);
VELO_PROFILE_CONFIG_DEF(m_ant_zenith,_ant_zenith_freeze_handler, _ant_zenith_evt_handler);
static ant_zenith_profile_t m_ant_zenith;
NRF_SDH_ANT_OBSERVER(m_ant_observer, ANT_AERO_ANT_OBSERVER_PRIO,ant_zenith_stack_evt_handler, &m_ant_zenith);



static void _velo_send_req_page_loop(ant_zenith_profile_t * p_profile)
{
  p_profile->p_sens_cb->send_requested_page = 1;
  p_profile->p_sens_cb->response_count = 0;
}


// NOTE Set up profile and open ANT channel:
void velo_ant_zenith_profile_setup(uint16_t device_id)
{
  ret_code_t err_code;
  if (!velo_status.ant_zenith_init)
  {
    // set device number
    m_ant_zenith_channel_config.device_number=device_id;
    // Set ANT frequency
    m_ant_zenith_channel_config.rf_freq = VELO_ANTPLUS_RF_FREQ; 

    // Set mode:
    ant_zenith_modes_update(VELO_DEFAULT_ZENITH_MODE, &m_ant_zenith.ant_zenith_mode);

    err_code = ant_zenith_init(&m_ant_zenith,VELO_CHANNEL_CONFIG(m_ant_zenith),VELO_PROFILE_CONFIG(m_ant_zenith));
    APP_ERROR_CHECK(err_code);
    velo_status.ant_zenith_init = true;
    err_code = ant_zenith_open(&m_ant_zenith);
    APP_ERROR_CHECK(err_code);
  }
}

// NOTE Get current data values (takes averages where necessary) and put into ANT pages.
// NOTE Called as callback from ANT_ZENITH.
static void _ant_zenith_freeze_handler(ant_zenith_profile_t * p_profile, uint8_t packet_count)
{
  velo_lidar_calc(&velo_sensor_data.lidar_1);
  velo_lidar_calc(&velo_sensor_data.lidar_2);
  velo_lidar_calc(&velo_sensor_data.lidar_3);
  ant_aero_page_228_freeze(&p_profile->page_228, packet_count, velo_sensor_data.lidar_1.range_av, velo_sensor_data.lidar_2.range_av,
                        velo_sensor_data.lidar_3.range_av);
  
  velo_accel_calc_stats(&velo_sensor_data.accel);
  ant_aero_page_227_freeze(&p_profile->page_227, packet_count, velo_sensor_data.accel.accel_x_mean, velo_sensor_data.accel.accel_y_mean,
                        velo_sensor_data.accel.accel_z_mean, velo_sensor_data.accel.accel_mag_max);

  ant_aero_page_232_freeze(&p_profile->page_232, velo_settings.range_ref_1, velo_settings.range_ref_2, velo_settings.range_ref_3);
  ant_aero_page_224_update_battery(&p_profile->page_224,velo_sensor_data.battery.voltage,velo_sensor_data.battery.soc);
}

void _ant_zenith_ref_udpate_loop(ant_zenith_profile_t * p_profile)
{
  // Have settings changed:
  bool update = false;
  if (p_profile->page_232.range_ref_1 != velo_settings.range_ref_1) update = true;
  if (p_profile->page_232.range_ref_2 != velo_settings.range_ref_2) update = true;
  if (p_profile->page_232.range_ref_3 != velo_settings.range_ref_3) update = true;

  if (update)
  {
    NRF_LOG_INFO("Updating range reference values.");
    velo_settings.range_ref_1 = p_profile->page_232.range_ref_1; 
    velo_settings.range_ref_2 = p_profile->page_232.range_ref_2; 
    velo_settings.range_ref_3 = p_profile->page_232.range_ref_3; 
    velo_settings_save();
  }
}

// NOTE ANT event handler - callback from ANT_ZENITH
// NOTE For now, nothing to do..
static void _ant_zenith_evt_handler(ant_zenith_profile_t * p_profile, ant_zenith_page_t page)
{
  nrf_pwr_mgmt_feed();
  switch (page)
  {
    case ANT_AERO_PAGE_224:
      //fall through
    case ANT_AERO_PAGE_227:
      //fall through
    case ANT_AERO_PAGE_228:
      //fall through
      break;
    case ANT_AERO_PAGE_33:
      // New command page - could be new lap, section, session, or mode change (currently..)
      // First look at what the command is:
      switch (p_profile->page_33.command_type)
      {
        case 128:
          // mode change
          ant_zenith_modes_update(p_profile->page_33.command_data, &m_ant_zenith.ant_zenith_mode);
          break;
        case 131:
          // tare accelerometers
          velo_accel_start_taring();
          break;
        case 132:
          // tare accelerometers
          velo_accel_clear_tares();
          break;
      }
      break;
      
    case ANT_AERO_PAGE_232:
      // If page was received, then may need to change period:
      if (!p_profile->p_sens_cb->event_is_tx)
      {
        _ant_zenith_ref_udpate_loop(p_profile);
      }
      break;
    case ANT_COMMON_PAGE_70:
      // Data page request, only on specific page requests.
      // Note that device only ever receives this page, never sends.
      switch (p_profile->page_70.command_type)
      {
        case ANT_PAGE70_COMMAND_PAGE_DATA_REQUEST:
          // Set transmission flags - this is done separately from above select, as most pages require the same thing..
          switch (p_profile->page_70.page_number)
          {
            case ANT_COMMON_PAGE_80:
            case ANT_COMMON_PAGE_81:
            case ANT_AERO_PAGE_224:
            case ANT_AERO_PAGE_227:
            case ANT_AERO_PAGE_228:
            case ANT_AERO_PAGE_232:
              if (p_profile->page_70.transmission_response.items.ack_resposne)
              {
                p_profile->p_sens_cb->response_max_count = ZENITH_MAX_BURST_ATTEMPT_COUNT;
                p_profile->p_sens_cb->ack_requested = 1;
                p_profile->p_sens_cb->requested_frequency = p_profile->page_70.radio_frequency;
              }
              else
              {
                p_profile->p_sens_cb->ack_requested = 0;
                p_profile->p_sens_cb->response_max_count = p_profile->page_70.transmission_response.items.transmit_count;
                p_profile->p_sens_cb->requested_frequency = p_profile->page_70.radio_frequency;
              }
              p_profile->p_sens_cb->requested_page_no = p_profile->page_70.page_number;
              p_profile->p_sens_cb->requested_page_descriptor = p_profile->page_70.descriptor;
              _velo_send_req_page_loop(p_profile);
              break;
            default:
              // Do nothing, only want to send supported pages.
              break;
          }
          break;
      }
    default:
        break;
  }
}

// Close ANT channels.
void velo_ant_zenith_close()
{

  uint8_t status;
  ret_code_t err_code = sd_ant_channel_status_get(VELO_CHANNEL_NUM, &status);
  if (status & 0x02)  // Status 0x02 is searching, 0x03 is tracking.  
  {
    err_code = ant_zenith_close(&m_ant_zenith);
    APP_ERROR_CHECK(err_code);
  }
  velo_status.ant_zenith_init = false;
}

// Send ANT messasge:
void velo_ant_zenith_send()
{
  if (velo_status.ant_zenith_init)
  {
    ant_message_send(&m_ant_zenith);
  }
}