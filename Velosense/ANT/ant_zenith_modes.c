#include "ant_zenith_modes.h"

#define NRF_LOG_MODULE_NAME ant_zenith_modes
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

// Mode 0: range sensor data only.
static uint8_t m_mode_0_foreground_pages[] = {228};
static uint8_t m_mode_0_background_pages[] = {224, 232};
// Mode 1: Accelerometer data only.
static uint8_t m_mode_1_foreground_pages[] = {227};
static uint8_t m_mode_1_background_pages[] = {224};

void ant_zenith_modes_update(uint8_t mode_number, ant_zenith_mode_t * p_mode)
{
NRF_LOG_INFO("Changing mode to %u", mode_number);
  switch (mode_number)
  {
    case (0):
      p_mode->p_foreground_page_list = m_mode_0_foreground_pages;
      p_mode->foreground_page_count = 1;
      p_mode->p_background_page_list = m_mode_0_background_pages;
      p_mode->background_page_count = 2;
      p_mode->background_page_frequency = 64;
      p_mode->background_replacement_page = 0;
      p_mode->id = mode_number;
      break;
    case (1):
      p_mode->p_foreground_page_list = m_mode_1_foreground_pages;
      p_mode->foreground_page_count = 1;
      p_mode->p_background_page_list = m_mode_1_background_pages;
      p_mode->background_page_count = 1;
      p_mode->background_page_frequency = 64;
      p_mode->background_replacement_page = 0;
      p_mode->id = mode_number;
      break;
    default:
      // Do nothing, i.e. ignore spurious mode numbers.
      break;
  }
}