
#include "sdk_common.h"
#include "ant_aero_page_228.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "velo_config.h"

static void page_228_data_log(ant_aero_page_228_data_t const * p_page_data)
{
  NRF_LOG_INFO("****************************** PAGE 228 START ******************************");
  NRF_LOG_INFO("packet count:			%u", p_page_data->packet_count);
  NRF_LOG_INFO("range 1:                        %u", p_page_data->range_1);
  NRF_LOG_INFO("range 2:                        %u", p_page_data->range_2);
  NRF_LOG_INFO("range 3:                        %u", p_page_data->range_3);
  NRF_LOG_FLUSH();
}

void ant_aero_page_228_freeze(ant_aero_page_228_data_t * p_page_data, uint8_t packet_count, uint16_t range_1, uint16_t range_2, uint16_t range_3)
{
	p_page_data->packet_count=packet_count;
  p_page_data->range_1 = range_1;
  p_page_data->range_2 = range_2;
  p_page_data->range_3 = range_3;
}

void ant_aero_page_228_encode(uint8_t * p_page_buffer, ant_aero_page_228_data_t  * p_page_data)
{
  ant_aero_page_228_data_layout_t * p_outcoming_data = (ant_aero_page_228_data_layout_t *)p_page_buffer;
  p_outcoming_data->packet_count=p_page_data->packet_count;
  UNUSED_PARAMETER(uint16_encode(p_page_data->range_1,p_outcoming_data->range_1));
  UNUSED_PARAMETER(uint16_encode(p_page_data->range_2,p_outcoming_data->range_2));
  UNUSED_PARAMETER(uint16_encode(p_page_data->range_3,p_outcoming_data->range_3));
  if (VELOSENSE_LOG_ENABLED)
  {
          page_228_data_log(p_page_data);
  }
}
