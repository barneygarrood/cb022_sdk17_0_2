
#ifndef ANT_AERO_PAGE_228_H__
#define ANT_AERO_PAGE_228_H__

#include <stdint.h>
#define ANT_AERO_PAGE_228 (228)

/**@brief Data packet struct.
*/
typedef struct
{
	uint8_t packet_count;
	uint8_t range_1[2];
	uint8_t range_2[2];
	uint8_t range_3[2];
	
}ant_aero_page_228_data_layout_t;

/**@brief Page data struct.
*/
typedef struct
{
	uint8_t count;
	uint8_t error_code;
	
	uint8_t packet_count;
	uint16_t range_1;
	uint16_t range_2;
	uint16_t range_3;
	
} ant_aero_page_228_data_t;


//Initialiser
#define ANT_AERO_PAGE_228_INIT(count_)  \
    (ant_aero_page_228_data_t)                      	\
    {                                               \
        .count     = (count_),        \
    }
		


void ant_aero_page_228_freeze(ant_aero_page_228_data_t * p_page_data, uint8_t packet_count, uint16_t range_1, uint16_t range_2, uint16_t range_3);

/**@brief Function for encoding page.
 *
 * @param[in]  p_page_data      Pointer to the page data.
 * @param[out] p_page_buffer    Pointer to the data buffer.
 */
void ant_aero_page_228_encode(uint8_t * p_page_buffer, ant_aero_page_228_data_t * p_page_data);	
		
		
	
#endif // ANT_AERO_PAGE_228_H__
