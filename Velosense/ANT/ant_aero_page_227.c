#include "sdk_common.h"
#include "ant_aero_page_227.h"
#include "nrf_log.h"
#include "velo_config.h"

#define INT12_MIN   -2047
#define INT12_MAX   2047
#define UINT12_MAX  4095

		
/**@brief Function for logging page.
 *
 * @param[in]  p_page_data      Pointer to the page data.
 */
static void page_227_data_log(ant_aero_page_227_data_t const * p_page_data)
{
    NRF_LOG_INFO("****************************** PAGE 14 START ******************************");
    NRF_LOG_INFO("packet count:			%u", p_page_data->packet_count);
    NRF_LOG_INFO("accel_x;			%i", p_page_data->accel_x);
    NRF_LOG_INFO("accel_y:			%i", p_page_data->accel_y);
    NRF_LOG_INFO("accel_z:			%i", p_page_data->accel_z);
    NRF_LOG_INFO("accel_sd:			%u", p_page_data->accel_mag_max);
}

void ant_aero_page_227_freeze(ant_aero_page_227_data_t * p_page_data, uint8_t packet_count_, float mean_accel_x, float mean_accel_y, 
                                    float mean_accel_z, float accel_mag_max)
{
  //Average values and store:
  // Note since we are storing as 12 bit min/max we need to cap:
  p_page_data->accel_x = mean_accel_x < INT12_MIN ? INT12_MIN : 
                      mean_accel_x > INT12_MAX ? INT12_MAX :
                      (int16_t) mean_accel_x;

  p_page_data->accel_y = mean_accel_y < INT12_MIN ? INT12_MIN : 
                      mean_accel_y > INT12_MAX ? INT12_MAX :
                      (int16_t) mean_accel_y;

  p_page_data->accel_z = mean_accel_z < INT12_MIN ? INT12_MIN : 
                      mean_accel_z > INT12_MAX ? INT12_MAX :
                      (int16_t) mean_accel_z;

  p_page_data->accel_mag_max = accel_mag_max > UINT12_MAX ? UINT12_MAX : (uint16_t) accel_mag_max; 

  p_page_data->packet_count=packet_count_;

  
  if (VELOSENSE_LOG_ENABLED)
  {
          page_227_data_log(p_page_data);
  }
}

void ant_aero_page_227_encode(uint8_t * p_page_buffer, ant_aero_page_227_data_t  * p_page_data)
{
  ant_aero_page_227_data_layout_t * p_outcoming_data = (ant_aero_page_227_data_layout_t *)p_page_buffer;
  p_outcoming_data->packet_count = p_page_data->packet_count;
  // Offset:
  uint16_t x = p_page_data->accel_x - INT12_MIN; // NOTE INT12_MIN is a negative number..
  uint16_t y = p_page_data->accel_y - INT12_MIN;
  uint16_t z = p_page_data->accel_z - INT12_MIN;

  p_outcoming_data->data[0] = x & 0xFF;
  p_outcoming_data->data[1] = ((x >> 8) & 0x0F) | ((y<<4) & 0xF0);  // X MSB is bits 0:3, Y LSB is bits 4:7.  0 = LSB.
  p_outcoming_data->data[2] = ((y >> 4) & 0xFF);
  p_outcoming_data->data[3] = z & 0xFF;
  p_outcoming_data->data[4] = ((z >> 8) & 0x0F) | ((p_page_data->accel_mag_max << 4) & 0xF0);
  p_outcoming_data->data[5] = ((p_page_data->accel_mag_max >> 4) & 0xFF);
  NRF_LOG_INFO("X,Y,Z = %u, %u, %u", x,y,z);
  if (p_page_data->accel_z>0.1)
  {
    int x=0;
  }
}