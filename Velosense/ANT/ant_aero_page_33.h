#ifndef ANT_AERO_PAGE_33_H__
#define ANT_AERO_PAGE_33_H__

#include <stdint.h>
#include "sdk_common.h"
#define ANT_AERO_PAGE_33  (33)

// Packet structure
typedef struct
{
  uint8_t reserved;
  uint8_t command_type;
  uint8_t command_data[3];
  uint8_t time_offset[2];

} ant_aero_page_33_data_layout_t;

// Page data structure:
typedef struct
{
  uint8_t cid;
  uint8_t command_type;
  uint32_t command_data;
  uint16_t time_offset;

} ant_aero_page_33_data_t;

// Initialiser:
#define ANT_AERO_PAGE_33_INIT(cid_, command_type_, command_data_, time_offset_) \
   (ant_aero_page_33_data_t)                \
{                                           \
   .cid = (cid_),                           \
   .command_type = (command_type_),         \
   .command_data = (command_data_),         \
   .time_offset = (time_offset_),           \
}

// Functions:
void ant_aero_page_33_decode(uint8_t * p_page_buffer, ant_aero_page_33_data_t * p_page_data);

void ant_aero_page_33_data_log(ant_aero_page_33_data_t * p_page_data);

#endif // ANT_AERO_PAGE_32_H__
