
#ifndef ANT_AERO_PAGE_232_H__
#define ANT_AERO_PAGE_232_H__

#include <stdint.h>
#define ANT_AERO_PAGE_232 (232)

/**@brief Data packet struct.
*/
typedef struct
{
	uint8_t unused;
	uint8_t range_ref_1[2];
	uint8_t range_ref_2[2];
	uint8_t range_ref_3[2];
	
}ant_aero_page_232_data_layout_t;

/**@brief Page data struct.
*/
typedef struct
{
        uint8_t unused;
	uint16_t range_ref_1;
	uint16_t range_ref_2;
	uint16_t range_ref_3;
	
} ant_aero_page_232_data_t;


//Initialiser
#define ANT_AERO_PAGE_232_INIT()  \
    (ant_aero_page_232_data_t)                      	\
    {                                               \
      .unused = 0xFF,                                \
    }
		


void ant_aero_page_232_freeze(ant_aero_page_232_data_t * p_page_data, uint16_t range_ref_1, uint16_t range_ref_2, uint16_t range_ref_3);

/**@brief Function for encoding page.
 *
 * @param[in]  p_page_data      Pointer to the page data.
 * @param[out] p_page_buffer    Pointer to the data buffer.
 */
void ant_aero_page_232_encode(uint8_t * p_page_buffer, ant_aero_page_232_data_t * p_page_data);	
void ant_aero_page_232_decode(uint8_t * p_page_buffer, ant_aero_page_232_data_t  * p_page_data);
		
		
	
#endif // ANT_AERO_PAGE_232_H__
