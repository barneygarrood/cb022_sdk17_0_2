/**
 * Copyright (c) 2015 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
 
#ifndef ANT_ZENITH_H__
#define ANT_ZENITH_H__

#include <stdint.h>
#include <stdbool.h>
#include "ant_parameters.h"
#include "nrf_sdh_ant.h"
#include "ant_channel_config.h"
#include "ant_zenith_pages.h"
#include "sdk_errors.h"
#include "velo_driver.h"
#include "ant_zenith_modes.h"

#define VELO_DEVICE_TYPE            112u                ///< Device type
#define VELO_ANTPLUS_RF_FREQ        67u                 ///< Frequency, decimal 57 (2457 MHz).
#define VELO_MSG_PERIOD             3895u       //2800u//8192u //512u ///< /32768s
#define VELO_EXT_ASSIGN             0x00u               ///< ANT ext assign (see Ext. Assign Channel Parameters in ant_parameters.h: @ref ant_parameters).
#define VELO_CHANNEL_TYPE           CHANNEL_TYPE_MASTER //< device is master, i.e. broadcasting.

#define MAX_DEVICE_ID 	0xFFF


/** @brief Control block. */
typedef struct
{
  uint8_t regular_page_index;
  uint8_t regular_page_cycle_index;
  uint8_t background_page_index;

  uint16_t acknowledge_messsage_counter;
  uint16_t acknowledge_message_max_count;

  uint8_t response_max_count;
  uint8_t response_count;

  uint8_t requested_page_no;
  uint16_t requested_page_descriptor;

  bool acknowledge_message;
  uint8_t requested_frequency;

  uint16_t ant_period;  
  
  uint8_t ack_requested :1;
  uint8_t send_requested_page     :1;
  uint8_t event_is_tx             :1;

} ant_zenith_cb_t;

/**@brief Initialize an ANT channel configuration structure.
 *
 * @param[in]  NAME                 Name of related instance.
 * @param[in]  CHANNEL_NUMBER       Number of the channel assigned to the profile instance.
 * @param[in]  TRANSMISSION_TYPE    Type of transmission assigned to the profile instance.
 * @param[in]  DEVICE_NUMBER        Number of the device assigned to the profile instance.
 * @param[in]  NETWORK_NUMBER       Number of the network assigned to the profile instance.
 */
#define VELO_CHANNEL_CONFIG_DEF(NAME,                                      	\
                                     CHANNEL_NUMBER,                            \
                                     TRANSMISSION_TYPE,                         \
                                     NETWORK_NUMBER)                          \
static ant_channel_config_t   CONCAT_2(NAME, _channel_config) = 		\
    {                                                                           \
        .channel_number    = (CHANNEL_NUMBER),                                  \
        .channel_type      = VELO_CHANNEL_TYPE,                            	\
        .ext_assign        = VELO_EXT_ASSIGN,                                   \
        .rf_freq           = VELO_ANTPLUS_RF_FREQ ,                                                \
        .transmission_type = (TRANSMISSION_TYPE),                               \
        .device_type       = VELO_DEVICE_TYPE, 					\
        .channel_period    = VELO_MSG_PERIOD,                                   \
        .network_number    = (NETWORK_NUMBER),                                  \
    }

#define VELO_CHANNEL_CONFIG(NAME) &CONCAT_2(NAME, _channel_config)

/**@brief Initialize an ANT profile configuration structure
 *
 * @param[in]  NAME                 Name of related instance.
 * @param[in]  EVT_HANDLER          Event handler to be called for handling events in the BPWR profile.
 */
#define VELO_PROFILE_CONFIG_DEF(NAME, FREEZE_HANDLER, EVT_HANDLER)   \
static ant_zenith_cb_t            CONCAT_2(NAME, _cb);                 \
static const ant_zenith_config_t  CONCAT_2(NAME, _profile_config) =    \
    {                                                                \
        .p_cb               = &CONCAT_2(NAME, _cb),                  \
        .freeze_handler        = (FREEZE_HANDLER),                   \
        .evt_handler        = (EVT_HANDLER),                         \
    }
#define VELO_PROFILE_CONFIG(NAME) &NAME##_profile_config
		
 /**@brief Page number type. */
typedef uint8_t ant_zenith_page_t;

// Forward declaration of the ant_zenith_profile_t type.
typedef struct ant_zenith_profile_s ant_zenith_profile_t;

/**@brief Event handler type - this defines pointer to the function. */
typedef void (* ant_zenith_freeze_handler_t) (ant_zenith_profile_t *, uint8_t packet_count);

/**@brief Event handler type - this defines pointer to the function. */
typedef void (* ant_zenith_evt_handler_t) (ant_zenith_profile_t *, ant_zenith_page_t page);

/**@brief Configuration structure. */
typedef struct
{
    ant_zenith_cb_t     * p_cb;          ///< Pointer to the data buffer for internal use.
    ant_zenith_evt_handler_t   evt_handler;   ///< Event handler to be called for handling events .
    ant_zenith_freeze_handler_t   freeze_handler;
	
} ant_zenith_config_t;

 /**@brief Profile structure. */
struct ant_zenith_profile_s
{
    uint8_t                  	channel_number; ///< Channel number assigned to the profile.
    ant_zenith_cb_t * 			p_sens_cb;			///< Pointer to internal control block
    ant_zenith_freeze_handler_t   freeze_handler;
    ant_zenith_evt_handler_t   	evt_handler;    ///< Event handler       
          
    ant_zenith_mode_t   ant_zenith_mode;

    ant_aero_page_33_data_t     page_33;
    ant_aero_page_224_data_t    page_224;        
    ant_aero_page_227_data_t    page_227; 
    ant_aero_page_228_data_t    page_228;  
    ant_aero_page_232_data_t    page_232;  

    ant_common_page_70_data_t   page_70;  
    ant_common_page80_data_t   page_80;
    ant_common_page81_data_t   page_81;
};

ret_code_t ant_zenith_init(ant_zenith_profile_t           * p_profile,
                              ant_channel_config_t const   * p_channel_config,
                              ant_zenith_config_t const * p_sens_config);
ret_code_t ant_zenith_open(ant_zenith_profile_t * p_profile);
ret_code_t ant_zenith_close(ant_zenith_profile_t * p_profile);
void ant_zenith_stack_evt_handler(ant_evt_t * p_ant_evt, void * p_context);

void velo_start_pairing(void);
void velo_pairing_off(void * p_context);
void ant_zenith_set_channel_id(uint8_t ant_channel_id,ant_channel_config_t ant_chan_cfg);
void ant_zenith_period_set(uint8_t ant_channel_id, uint16_t ant_channel_period);
void ant_message_send(ant_zenith_profile_t * p_profile);
#endif // ANT_ZENITH_H__
