#include "sdk_common.h"
 
#include "nrf_assert.h"
#include "app_error.h"
#include "ant_interface.h"
#include "ant_zenith.h"
#include "ant_zenith_modes.h"

#include "nrf_log.h"
#include "app_timer.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_delay.h"

#include "velo_config.h"
#include "velo_status.h"
#include "velo_settings.h"


uint8_t packet_count=0;

/**@brief Message data layout structure. */
typedef struct
{
    uint8_t page_number;
    uint8_t page_payload[7];
} ant_zenith_message_layout_t;

static uint8_t page_pattern[] = {228};

// Profile intialisation:
ret_code_t ant_zenith_init(ant_zenith_profile_t           * p_profile,
                              ant_channel_config_t const   * p_channel_config,
                              ant_zenith_config_t const * p_sens_config)
{
  ASSERT(p_profile != NULL);
  ASSERT(p_channel_config != NULL);
  ASSERT(p_sens_config != NULL);

  p_profile->p_sens_cb->regular_page_index = 0;
  p_profile->p_sens_cb->regular_page_cycle_index = 0;
  p_profile->p_sens_cb->background_page_index = 0;
	
  p_profile->evt_handler   = p_sens_config->evt_handler;
  p_profile->freeze_handler = p_sens_config->freeze_handler;
  p_profile->p_sens_cb = p_sens_config->p_cb;

  p_profile->p_sens_cb->acknowledge_message = false;
  p_profile->p_sens_cb->acknowledge_messsage_counter=0;
  p_profile->p_sens_cb->ant_period=VELO_MSG_PERIOD;

  p_profile->channel_number = p_channel_config->channel_number;

  p_profile->page_33 = ANT_AERO_PAGE_33_INIT(0,0,0,0);
  p_profile->page_224  = ANT_AERO_PAGE_224_INIT();
  p_profile->page_227  = ANT_AERO_PAGE_227_INIT();
  p_profile->page_228  = ANT_AERO_PAGE_228_INIT(0);
  p_profile->page_232  = ANT_AERO_PAGE_232_INIT();

  p_profile->page_70 = DEFAULT_ANT_COMMON_PAGE70();
  p_profile->page_80 = ANT_COMMON_page80(VELO_HW_REVISION,VELO_MANUF_ID,VELO_MODEL_NO);
  p_profile->page_81 = ANT_COMMON_page81(VELO_SW_REVISION_MAJ,VELO_SW_REVISION_MIN,velo_get_serialNo());
  p_profile->p_sens_cb->send_requested_page=0;

  return ant_channel_init(p_channel_config);
	
}

/**@brief Function for getting next page number to send.
 *
 * @param[in]  p_profile        Pointer to the profile instance.
 *
 * @return     Next page number.
 */
static ant_zenith_page_t next_page_number_get(ant_zenith_profile_t * p_profile)
{
  ant_zenith_page_t page_number;

    // Foreground or background page?
  if ((p_profile->p_sens_cb->regular_page_cycle_index == 0) && 
              (p_profile->p_sens_cb->regular_page_index == p_profile->ant_zenith_mode.background_replacement_page) && 
              (p_profile->ant_zenith_mode.background_page_count>0))
  {
    page_number = p_profile->ant_zenith_mode.p_background_page_list[p_profile->p_sens_cb->background_page_index];
    p_profile->p_sens_cb->background_page_index++;
    if (p_profile->p_sens_cb->background_page_index >= p_profile->ant_zenith_mode.background_page_count)
    {
      p_profile->p_sens_cb->background_page_index = 0;
    }
  }
  else
  {
    page_number = p_profile->ant_zenith_mode.p_foreground_page_list[p_profile->p_sens_cb->regular_page_index];
  }
  p_profile->p_sens_cb->regular_page_index++;
  if (p_profile->p_sens_cb->regular_page_index >= p_profile->ant_zenith_mode.foreground_page_count)
  {
    p_profile->p_sens_cb->regular_page_index = 0;
    p_profile->p_sens_cb->regular_page_cycle_index++;
    if (p_profile->p_sens_cb->regular_page_cycle_index >= p_profile->ant_zenith_mode.background_page_frequency)
    {
      p_profile->p_sens_cb->regular_page_cycle_index=0;
    }
  }

 if (p_profile->p_sens_cb->send_requested_page)
  {
    page_number = (ant_zenith_page_t) p_profile->p_sens_cb->requested_page_no;
  }
  return page_number;
}

/**@brief Function for encoding message.
 *
 * @note Assume to be call each time when Tx window will occur.
 */
static void sens_message_encode(ant_zenith_profile_t * p_profile, uint8_t * p_message_payload)
{
    ant_zenith_message_layout_t * p_velo_message_payload = (ant_zenith_message_layout_t *)p_message_payload;

    p_velo_message_payload->page_number = next_page_number_get(p_profile);

		//Note we encode data always on the 0th page so that all 3 pages have the same timestamp.
    switch (p_velo_message_payload->page_number)
    {
      case ANT_COMMON_PAGE_81:
        packet_count++;
        ant_common_page_81_encode(p_velo_message_payload->page_payload,&(p_profile->page_81));
        break;
      case ANT_AERO_PAGE_224:
        // Battery page:
        //Calculage accelerometer averages etc:
        p_profile->freeze_handler(p_profile,packet_count);
        packet_count++;
        ant_aero_page_224_encode(p_velo_message_payload->page_payload,&(p_profile->page_224));
        break;
      case ANT_AERO_PAGE_227:
        // Accelerometer page.
        p_profile->freeze_handler(p_profile,packet_count);
        packet_count++;
        ant_aero_page_227_encode(p_velo_message_payload->page_payload,&(p_profile->page_227));
        break;
      case ANT_AERO_PAGE_228:
        // Range sensor page.
        p_profile->freeze_handler(p_profile,packet_count);
        packet_count++;
        ant_aero_page_228_encode(p_velo_message_payload->page_payload,&(p_profile->page_228));
        break;
      case ANT_AERO_PAGE_232:
        // Range sensor reference values.
        p_profile->freeze_handler(p_profile,packet_count);
        packet_count++;
        ant_aero_page_232_encode(p_velo_message_payload->page_payload,&(p_profile->page_232));
        break;
      default:
        return;
    }
    p_profile->evt_handler(p_profile, (ant_zenith_page_t)p_velo_message_payload->page_number);
}


static void sens_message_decode(ant_zenith_profile_t * p_profile, uint8_t * p_message_payload)
{
    ant_zenith_message_layout_t * p_zenith_message_payload = (ant_zenith_message_layout_t *)p_message_payload;

    switch (p_zenith_message_payload->page_number)
    {
      case ANT_AERO_PAGE_33:       
        ant_aero_page_33_decode(p_zenith_message_payload->page_payload, &p_profile->page_33);
        break;
      case ANT_COMMON_PAGE_70:       
        ant_common_page_70_decode(p_zenith_message_payload->page_payload, &p_profile->page_70);
        break;
      case ANT_AERO_PAGE_232:       
        ant_aero_page_232_decode(p_zenith_message_payload->page_payload, &p_profile->page_232);
        break;
      default:
        return;
    }
    p_profile->evt_handler(p_profile, (ant_zenith_page_t)p_zenith_message_payload->page_number);
}

// Sends ant message:
void ant_message_send(ant_zenith_profile_t * p_profile)
{
    uint32_t err_code;
    uint8_t  p_message_payload[ANT_STANDARD_DATA_PAYLOAD_SIZE];
		//OVerwrite second byte with packet counter.
    sens_message_encode(p_profile, p_message_payload);
    err_code = sd_ant_broadcast_message_tx(p_profile->channel_number,
                                    sizeof (p_message_payload),
                                    p_message_payload);
    APP_ERROR_CHECK(err_code);
}

// NOTE Open ANT channel
ret_code_t ant_zenith_open(ant_zenith_profile_t * p_profile)
{
    // Fill tx buffer for the first frame
    ant_message_send(p_profile);

    return sd_ant_channel_open(p_profile->channel_number);
}

// NOTE Close ANT channel
ret_code_t ant_zenith_close(ant_zenith_profile_t * p_profile)
{

    return sd_ant_channel_close(p_profile->channel_number);
}

// Ant event handler
void ant_zenith_stack_evt_handler(ant_evt_t * p_ant_event, void * p_context)
{
  //Update status flag to show ant context:
  velo_status.error_status.ant_context = true;
  ant_zenith_profile_t * p_profile = ( ant_zenith_profile_t *)p_context;
  if (p_ant_event->channel == p_profile->channel_number)
  {
    switch (p_ant_event->event)
    {
      case EVENT_TX:
        //NRF_LOG_INFO("TX");
        velo_status.last_ant_msg_tx_time = NRF_RTC0->COUNTER & 0xFFFFFF;
        p_profile->p_sens_cb->event_is_tx = true;
        if (p_profile->p_sens_cb->send_requested_page)
        {
          p_profile->p_sens_cb->response_count++;

          if (p_profile->p_sens_cb->response_count > p_profile->p_sens_cb->response_max_count)
          {
            p_profile->p_sens_cb->send_requested_page = 0;
            p_profile->p_sens_cb->requested_frequency = 255;
          }
        }
        //ant_message_send(p_profile);
        break;

      case EVENT_RX:
        p_profile->p_sens_cb->event_is_tx = false;
        //if (p_ant_event->message.ANT_MESSAGE_ucMesgID == MESG_BROADCAST_DATA_ID)MESG_ACKNOWLEDGED_DATA_ID
        if (p_ant_event->message.ANT_MESSAGE_ucMesgID == MESG_ACKNOWLEDGED_DATA_ID)
        {
/* NEED TO DEAL WITH INCOMING MESSAGE!! */								
          sens_message_decode(p_profile, p_ant_event->message.ANT_MESSAGE_aucPayload);
        }
        break;

      default:
          // No implementation needed
          break;
    }
  }
  velo_status.error_status.ant_context = false;
}

void ant_zenith_set_channel_id(uint8_t ant_channel_id,ant_channel_config_t ant_chan_cfg)
{
	uint8_t err_code = sd_ant_channel_id_set(ant_channel_id,ant_chan_cfg.device_number,ant_chan_cfg.device_type,
									ant_chan_cfg.transmission_type);
	APP_ERROR_CHECK(err_code);	
	
}

void ant_zenith_period_set(uint8_t ant_channel_id, uint16_t ant_channel_period)
{
  sd_ant_channel_period_set (ant_channel_id, ant_channel_period);
}


