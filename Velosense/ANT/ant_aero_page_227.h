
#ifndef ANT_AERO_PAGE_227_H__
#define ANT_AERO_PAGE_227_H__

#include <stdint.h>

#define ANT_AERO_PAGE_227  (227)

//Initialiser
#define ANT_AERO_PAGE_227_INIT(count_)  \
    (ant_aero_page_227_data_t)                      	\
    {                                               \
    }

/**@brief Data packet struct.
*/
typedef struct
{
	uint8_t packet_count;
	uint8_t data[6];
	
}ant_aero_page_227_data_layout_t;

/**@brief Page data struct.
*/
typedef struct
{
	uint8_t packet_count;
  int16_t accel_x;
  int16_t accel_y;
  int16_t accel_z;
  uint16_t accel_mag_max;
} ant_aero_page_227_data_t;


/**@brief Function for averaging register data".
 *
 * @param[in]  p_page_data      Pointer to the page data.
 */
void ant_aero_page_227_freeze(ant_aero_page_227_data_t * p_page_data, uint8_t packet_count_, float mean_accel_x, float mean_accel_y, 
                                    float mean_accel_z, float accel_mag_max);	

/**@brief Function for encoding page.
 *
 * @param[in]  p_page_data      Pointer to the page data.
 * @param[out] p_page_buffer    Pointer to the data buffer.
 */
void ant_aero_page_227_encode(uint8_t * p_page_buffer, ant_aero_page_227_data_t * p_page_data);	
		
#endif // ANT_VELO_RANGE_PAGE_12_H__