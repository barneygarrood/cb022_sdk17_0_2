#ifndef VELO_ANT_ZENITH_H__
#define VELO_ANT_ZENITH_H__
#include "ant_zenith.h"

void velo_ant_zenith_profile_setup(uint16_t device_id);
// Set error messsage:
//void velo_ant_zenith_set_error(velo_error_status_t error_status);

void velo_ant_zenith_close();
void velo_ant_zenith_send();

#endif //VELO_ANT_ZENITH_H__