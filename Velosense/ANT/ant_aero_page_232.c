
#include "sdk_common.h"
#include "ant_aero_page_232.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "velo_config.h"

static void page_232_data_log(ant_aero_page_232_data_t const * p_page_data)
{
  NRF_LOG_INFO("****************************** PAGE 232 START ******************************");
  NRF_LOG_INFO("range ref 1:                        %u", p_page_data->range_ref_1);
  NRF_LOG_INFO("range ref 2:                        %u", p_page_data->range_ref_2);
  NRF_LOG_INFO("range ref 3:                        %u", p_page_data->range_ref_3);
  NRF_LOG_FLUSH();
}

void ant_aero_page_232_freeze(ant_aero_page_232_data_t * p_page_data, uint16_t range_ref_1, uint16_t range_ref_2, uint16_t range_ref_3)
{
  p_page_data->range_ref_1 = range_ref_1;
  p_page_data->range_ref_2 = range_ref_2;
  p_page_data->range_ref_3 = range_ref_3;
}

void ant_aero_page_232_encode(uint8_t * p_page_buffer, ant_aero_page_232_data_t  * p_page_data)
{
  ant_aero_page_232_data_layout_t * p_outcoming_data = (ant_aero_page_232_data_layout_t *)p_page_buffer;;
  p_outcoming_data->unused = p_page_data->unused;
  UNUSED_PARAMETER(uint16_encode(p_page_data->range_ref_1,p_outcoming_data->range_ref_1));
  UNUSED_PARAMETER(uint16_encode(p_page_data->range_ref_2,p_outcoming_data->range_ref_2));
  UNUSED_PARAMETER(uint16_encode(p_page_data->range_ref_3,p_outcoming_data->range_ref_3));
  //if (VELOSENSE_LOG_ENABLED)
  {
    page_232_data_log(p_page_data);
  }
}

void ant_aero_page_232_decode(uint8_t * p_page_buffer, ant_aero_page_232_data_t  * p_page_data)
{

  ant_aero_page_232_data_layout_t * p_page_in = (ant_aero_page_232_data_layout_t * ) p_page_buffer;
  p_page_data->unused = p_page_in->unused;
  p_page_data->range_ref_1 = uint16_decode(p_page_in->range_ref_1);
  p_page_data->range_ref_2 = uint16_decode(p_page_in->range_ref_2);
  p_page_data->range_ref_3 = uint16_decode(p_page_in->range_ref_3);
 // if (VELOSENSE_LOG_ENABLED)
  {
    page_232_data_log(p_page_data);
  }
}