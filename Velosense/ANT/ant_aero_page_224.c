#include "ant_aero_page_224.h"
#include "velo_config.h"
#define NRF_LOG_MODULE_NAME ant_aero_page_224
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
NRF_LOG_MODULE_REGISTER();

void ant_aero_page_224_encode(uint8_t * p_page_buffer, ant_aero_page_224_data_t * p_page_data)
{
  ant_aero_page_224_data_layout_t * p_page_out = (ant_aero_page_224_data_layout_t * ) p_page_buffer;

  p_page_out->new_file_count = p_page_data->new_file_count;
  p_page_out->installed_memory = p_page_data->installed_memory;
  uint24_encode(p_page_data->used_memory,p_page_out->used_memory);


  // Note we can put these into an intialiser!
  uint8_t coarse_voltage=p_page_data->voltage/1000;
  float fractional_voltage = (p_page_data->voltage/1000.0f-coarse_voltage)*256;
  p_page_out->fractional_voltage = fractional_voltage;
  p_page_out->descriptive_bit_field.coarse_voltage = coarse_voltage;

  // Battery status based on SOC:
  if (p_page_data->soc >80)
  {
    p_page_out->descriptive_bit_field.status = 1;
  }
  else if (p_page_data->soc>60)
  {
    p_page_out->descriptive_bit_field.status = 2;
  }
  else if (p_page_data->soc>40)
  {
    p_page_out->descriptive_bit_field.status = 3;
  }
  else if (p_page_data->soc>20)
  {
    p_page_out->descriptive_bit_field.status = 4;
  }
  else
  {
    p_page_out->descriptive_bit_field.status = 5;
  }

  p_page_out->descriptive_bit_field.reserved=0;

  if (ANT_AERO_PAGE_224_LOG_ENABLED)
  {
    ant_aero_page_224_data_log(p_page_data);
  }

}

void ant_aero_page_224_data_log(ant_aero_page_224_data_t const * p_page_data)
{
    NRF_LOG_INFO("****************************** PAGE 224 START ******************************");
    NRF_LOG_INFO("new_files_count:                              %u", p_page_data->new_file_count);
    NRF_LOG_INFO("installed_memory:                             %u", p_page_data->installed_memory);
    NRF_LOG_INFO("used_memory:                                  %u", p_page_data->used_memory);
    NRF_LOG_INFO("voltage:                                      %u", p_page_data->voltage);
    NRF_LOG_INFO("soc :                                         %u", p_page_data->soc);
}

void ant_aero_page_224_update_battery(ant_aero_page_224_data_t * p_page_data, uint16_t voltage, uint8_t soc)
{
  p_page_data->voltage = voltage;
  p_page_data->soc = soc;
}
void ant_aero_page_224_update_memory(ant_aero_page_224_data_t * p_page_data, uint8_t new_file_count, uint32_t used_memory)
{
  p_page_data->new_file_count = new_file_count;
  p_page_data->used_memory = used_memory;
}

