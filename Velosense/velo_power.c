#include "velo_power.h"
#include "app_scheduler.h"
#include "velo_led_pwm.h"
#include "velo_status.h"
#include "velo_config.h"
#include "velo_driver.h"
#include "velo_sensors.h"

#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "app_button.h"
#include "app_timer.h"
#include "nrf_drv_gpiote.h"
#include "nrf_power.h"

#define NRF_LOG_MODULE_NAME velo_power
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

static uint8_t tap_count=0;
static uint32_t tap_start_time=0;
static velo_power_callback_t velo_power_callback;

//Timer object to run button timer off. 
//Enables us to have different actions depending on press duration.
APP_TIMER_DEF(m_button_tick_timer);

//Handler for GPOUT status change:
static void _bat_gauge_interrupt_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
  if (!velo_status.enable_gpio_events) return;
}

//NOTE Charger interrupt handler (PIN_PG)
static void _charger_interrupt_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
  //Either we are plugging in or unplugging.
  velo_status.pending_actions |= VELO_PENDING_ACTION_CHECK_CHARGE_STATUS;
  velo_power_check_update_status();
}


// NOTE Shutdown handler - called from scheduler to allow shutdown from main context.
static void _shutdown_handler(void * p_event_data, uint16_t event_size)
{
  // NOTE Why did we have this originally?  I think we want to wake on plugin!
  //Uninitialise plugin and charge status detection
  //nrf_drv_gpiote_in_uninit(PIN_PG);
  // Flush messages:
  NRF_LOG_FLUSH();
  sd_power_system_off();
}

// NOTE Check charge status - just checks if charging or not and sets flags accordinhly.
// NOTE THis should ONLY be called from velo_power_check_update_status
static void _check_charge_status(void)
{
  // Get status based on battery voltage:
  velo_bat_status_enum_t status = velo_bat_get_status(&velo_sensor_data.battery);
  //Pin is low on plugged in or high otherwise.
  if (nrf_gpio_pin_read(PIN_PG))
  {
    // Unpluged.
    // If in standby mode and was charging, then switch off:
    if (velo_status.current_state == VELO_PWR_STATE_STANDBY)
    {
      // Pause 10ms to allow for debounce.
      nrf_delay_ms(100);
      velo_status.new_state = VELO_PWR_STATE_OFF;
      velo_power_callback();
    }
    else
    {
      // If this has gone low, then need to shutdown unit.
      if (status == VELO_BAT_STATE_SUB_CRITICAL)
      {
        velo_status.new_state = VELO_PWR_STATE_OFF;
        // NOTE ALways change power state through master:
        velo_power_callback();
        return;
      }
      else if (status == VELO_BAT_STATE_CRITICAL)
      {
        velo_status.charge_state = VELO_CHARGE_STATE_LOW;
        velo_status.pending_actions |= VELO_PENDING_ACTION_UPDATE_LED;
      }
      // Unplugged and on, so just reset the charging status to none.
      else if (velo_status.charge_state != VELO_CHARGE_STATE_NONE)
      {
        velo_status.charge_state = VELO_CHARGE_STATE_NONE;
        velo_status.pending_actions |= VELO_PENDING_ACTION_UPDATE_LED;
      }
    }
  }
  else  // Plugged in
  {
    // Reset charge status, retaining low bat setting.
    if (!nrf_gpio_pin_read(PIN_STAT_1))
    {
      NRF_LOG_INFO("Pin stat = 0");
      velo_status.charge_state = VELO_CHARGE_STATE_CHARGING;
      velo_status.pending_actions |= VELO_PENDING_ACTION_UPDATE_LED;
    }
    // If fully charged then change light colour:
    else if(!(velo_status.charge_state == VELO_CHARGE_STATE_CHARGED))
    {
      NRF_LOG_INFO("Pin stat = 1");
      velo_status.charge_state = VELO_CHARGE_STATE_CHARGED;
      velo_status.pending_actions |= VELO_PENDING_ACTION_UPDATE_LED;
    }
  }
}


// NOTE Power button handler
static void _button_interrupt_handler(uint8_t button, uint8_t action)
{
  uint32_t err_code;
  if (!velo_status.enable_gpio_events) return;
  switch(action) 
  {
    case APP_BUTTON_PUSH:
      // Change status to on if not already on:
      if (velo_status.current_state != VELO_PWR_STATE_ON)
      {
        velo_status.new_state = VELO_PWR_STATE_ON;
       velo_power_callback();
      }
      app_timer_start(m_button_tick_timer, APP_TIMER_TICKS(BUTTON_OFF_MS), NULL);
      velo_status.button_state = VELO_BUTTON_STATE_BUTHOLD;
      velo_status.pending_actions |= VELO_PENDING_ACTION_UPDATE_LED;
      velo_power_check_update_status();
      break;
  
    case APP_BUTTON_RELEASE:
      if (velo_status.button_state & VELO_BUTTON_STATE_BUTHOLD)
      {
        err_code = app_timer_stop(m_button_tick_timer);
        APP_ERROR_CHECK(err_code);
        if (velo_status.button_state & VELO_BUTTON_STATE_OFF)
        {
          velo_status.button_state = VELO_BUTTON_STATE_NONE;
          // If charging go to standby, otherwise turn off:
          if (velo_status.charge_state == VELO_CHARGE_STATE_CHARGING)
          {
            velo_status.new_state = VELO_PWR_STATE_STANDBY;
          }
          else
          {
            velo_status.new_state = VELO_PWR_STATE_OFF;
          }
          velo_power_callback();
          break;
        } 
        else
        {
          velo_status.button_state = VELO_BUTTON_STATE_NONE;
          velo_status.pending_actions |= VELO_PENDING_ACTION_UPDATE_LED;
          velo_power_check_update_status();
        }
      }
      //Deal with button count:
      uint32_t time=time_us();
      if ((time-tap_start_time)>BOOTLOADER_TAP_TIME_MS*1000)
      {
        tap_count=0;
        tap_start_time=time;
      }
      tap_count++;
      if (tap_count == BOOTLOADER_TAP_COUNT)
      {
        velo_status.new_state = VELO_PWR_STATE_DFU;
        velo_power_callback();
      }
      NRF_LOG_INFO("Button taps = %u",tap_count);
    break;
  }
}

// NOTE Button timer handler - after period changes button effect.
static void _button_timer_handler(void * p_context)
{
  if (velo_status.button_state & VELO_BUTTON_STATE_BUTHOLD)
  {
    velo_status.button_state |= VELO_BUTTON_STATE_OFF;
  }
  // Update:
  velo_status.pending_actions |= VELO_PENDING_ACTION_UPDATE_LED;
  velo_power_check_update_status();
}

// NOTE Reset all GPIOs
static void _power_reset_all_gpios(bool reset_gpout)
{
  nrfx_gpiote_in_uninit(PIN_SW);
  nrfx_gpiote_in_uninit(PIN_PG);
  if (reset_gpout)
  {
    nrfx_gpiote_in_uninit(PIN_GPOUT);
  }
}

// NOTE Reset latches - means we can tell which gpio woke it up
static void _reset_latches()
{
  nrf_gpio_pin_latch_clear(PIN_SW);
  nrf_gpio_pin_latch_clear(PIN_PG);
  nrf_gpio_pin_latch_clear(PIN_GPOUT);
}

// NOTE Check power status if required, and update LED status if required.
void velo_power_check_update_status()
{
  if (velo_status.pending_actions & VELO_PENDING_ACTION_CHECK_CHARGE_STATUS)
  {
    _check_charge_status();
    // Reset flag:
    velo_status.pending_actions &= ~VELO_PENDING_ACTION_CHECK_CHARGE_STATUS;
  }
  if (velo_status.pending_actions & VELO_PENDING_ACTION_UPDATE_LED)
  {
    velo_led_pwm_set_pattern();
    velo_status.pending_actions &= ~VELO_PENDING_ACTION_UPDATE_LED;
  }
}

// Set GPIO to wake system up.
static void _set_gpio_for_wake(uint8_t pin_number, nrf_gpio_pin_sense_t sense_type, nrf_gpio_pin_pull_t pull_type)
{
  NRF_P0->PIN_CNF[pin_number] = ((uint32_t)NRF_GPIO_PIN_DIR_INPUT << GPIO_PIN_CNF_DIR_Pos)
                             | ((uint32_t)NRF_GPIO_PIN_INPUT_CONNECT << GPIO_PIN_CNF_INPUT_Pos)
                             | ((uint32_t)pull_type << GPIO_PIN_CNF_PULL_Pos)
                             | ((uint32_t)NRF_GPIO_PIN_S0S1 << GPIO_PIN_CNF_DRIVE_Pos)
                             | ((uint32_t)sense_type << GPIO_PIN_CNF_SENSE_Pos);
}

// NOTE Shutdown everything.
void velo_power_shutdown_all(void)
{
  //Unninitialise PWM (which controls LEDs - otherwise if shutdown exactly during a flash they can stay on).
  velo_led_pwm_uninit();

  // Reset gpios:
  _power_reset_all_gpios(false);

  //Turn off regulator:
  nrf_gpio_pin_write(PIN_REG, !REG_PIN_ON);

  // Set pins to wake:
  // Button - needs to detect low signal, and doesn't require pullup/pulldown.
  _set_gpio_for_wake(PIN_SW, NRF_GPIO_PIN_SENSE_LOW, NRF_GPIO_PIN_NOPULL);
  // Battery charger PG - detect on plug-in (PG goes low), requies pullup.
  _set_gpio_for_wake(PIN_PG, NRF_GPIO_PIN_SENSE_LOW, NRF_GPIO_PIN_PULLUP);

  // Put cpu to sleep
  velo_power_sleep();
}

// NOTE Put CPU to sleep
void velo_power_sleep(void)
{
  //Clear latches:
  _reset_latches();

  // Schedule shutdown of CPU.
  app_sched_event_put(NULL,0,_shutdown_handler);
  
}

// NOTE Turn everything off and reset into DFU mode.
void velo_power_enter_dfu()
{ 
  // Reset gpios:
  _power_reset_all_gpios(false);

  //Turn off regulator:
  nrf_gpio_pin_write(PIN_REG, !REG_PIN_ON);

  //Clear latches:
  _reset_latches();

  sd_power_gpregret_set(0,BOOTLOADER_DFU_START);
  sd_nvic_SystemReset();   
}

// NOTE Get startup reason, and decide what mode to boot into.
static void _get_starting_condition()
{  
  uint32_t latches =  NRF_P0->LATCH;
  //Are we charging, and is battery level critical?
  nrf_gpio_cfg_input(PIN_PG,NRF_GPIO_PIN_PULLUP);
  nrf_gpio_cfg_input(PIN_GPOUT,NRF_GPIO_PIN_PULLUP);
  nrf_delay_ms(10);
  bool is_charging = !nrf_gpio_pin_read(PIN_PG);
 // bool is_bat_critical = !nrf_gpio_pin_read(PIN_GPOUT);

 // Read battery:
  velo_bat_gauge_read(&velo_sensor_data.battery);
  velo_bat_status_enum_t bat_status = velo_bat_get_status(&velo_sensor_data.battery);

  // If battery is critical and NOT charging then we off.
  if (bat_status == VELO_BAT_STATE_SUB_CRITICAL) 
  {
    if (!is_charging)
    {
      NRF_LOG_INFO("Critical battery state.  Turning off.");
      velo_status.new_state = VELO_PWR_STATE_OFF;
      return;
    }
    // else put into standby:
    else
    {
      NRF_LOG_INFO("Critical battery state and charging.  Going to standby.");
      velo_status.new_state = VELO_PWR_STATE_STANDBY;
      return;
    }
  }
  // If due to button then switch on:
  if (latches & (1<<PIN_SW))
  {
    velo_status.new_state = VELO_PWR_STATE_ON;
  }
  // If due to PG pin then put into standby:
  else if (latches & (1<<PIN_PG))
  {
    velo_status.new_state = VELO_PWR_STATE_STANDBY;
  }
  else
  {
    velo_status.new_state = VELO_PWR_STATE_ON;
  }
  // Set flag depending on whether power connected (is charging).
  if (is_charging) 
  {
    velo_status.charge_state = VELO_CHARGE_STATE_CHARGING;
  }
}


// NOTE: Power up devices:
static void _power_up(void)
{
  // Turn off regulator:
  nrf_gpio_cfg_output(PIN_REG);
  if (REG_PIN_ON)
  {
    nrf_gpio_cfg(
          PIN_REG,
          NRF_GPIO_PIN_DIR_OUTPUT,
          NRF_GPIO_PIN_INPUT_DISCONNECT,
          NRF_GPIO_PIN_NOPULL,
          NRF_GPIO_PIN_S0S1,
          NRF_GPIO_PIN_NOSENSE);
  }
  else
  {
    nrf_gpio_cfg(
          PIN_REG,
          NRF_GPIO_PIN_DIR_OUTPUT,
          NRF_GPIO_PIN_INPUT_DISCONNECT,
          NRF_GPIO_PIN_NOPULL,
          NRF_GPIO_PIN_S0D1,
          NRF_GPIO_PIN_NOSENSE);
  }



  nrf_gpio_pin_write(PIN_REG, !REG_PIN_ON);

  // Start with all Lidars turned off:
  nrf_gpio_cfg_output(PIN_VL_1_XSHUT);
  nrf_gpio_pin_write(PIN_VL_1_XSHUT, 0);
  nrf_gpio_cfg_output(PIN_VL_2_XSHUT);
  nrf_gpio_pin_write(PIN_VL_2_XSHUT, 0);
  nrf_gpio_cfg_output(PIN_VL_3_XSHUT);
  nrf_gpio_pin_write(PIN_VL_3_XSHUT, 0);
  // wait 10ms - I dont know for sure if necesary but seems good to ensure everything off so reset properly.
  nrf_delay_ms(10);
  // Turn on regulators.
  nrf_gpio_pin_write(PIN_REG, REG_PIN_ON);
  //Wait for 10ms to give i2c sensors a change to boot up.
  nrf_delay_ms(10);
}

// NOTE: Single function to set an interrupt pin:
static void _set_gpio_interrupt(uint8_t pin, nrf_gpio_pin_pull_t pull_direction, nrf_gpiote_polarity_t polarity, 
                              nrfx_gpiote_evt_handler_t handler)
{
  nrf_drv_gpiote_in_config_t config = 
  {
    .is_watcher = false,
    .hi_accuracy = false,
    .pull = pull_direction,
    .sense = polarity
  };
  APP_ERROR_CHECK(nrf_drv_gpiote_in_init(pin, &config, handler));
  nrf_drv_gpiote_in_event_enable(pin, true);
}


// NOTE: Initialise GPIOs:
static void _set_gpios()
{
  uint32_t err_code;

  // ***************************************************************************************************************
  // Initialise gpiote module.
  if (!nrf_drv_gpiote_is_init())
  {
      err_code = nrf_drv_gpiote_init();
      APP_ERROR_CHECK(err_code);
  }

  // ***************************************************************************************************************
  //Setup the power button:
  static app_button_cfg_t pwr_buttons[] = {{PIN_SW, APP_BUTTON_ACTIVE_LOW, NRF_GPIO_PIN_NOPULL, _button_interrupt_handler}};
  err_code = app_button_init((app_button_cfg_t *)pwr_buttons,1,BUTTON_DETECTION_DELAY);
  APP_ERROR_CHECK(err_code);
  err_code = app_button_enable();
  APP_ERROR_CHECK(err_code);

  //Start timer to handle the power button, (power down if hold for more than x seconds)
  err_code = app_timer_create(&m_button_tick_timer, APP_TIMER_MODE_SINGLE_SHOT, _button_timer_handler);
  APP_ERROR_CHECK(err_code);

  // ***************************************************************************************************************
  //Setup gpio config for charger status pins:
  // NOTE Although stat_1 and stat_2 are available, actually these haven't been very useful, so just use PG
  //      to sense if charging or not.
  _set_gpio_interrupt(PIN_PG, NRF_GPIO_PIN_PULLUP, NRF_GPIOTE_POLARITY_TOGGLE, _charger_interrupt_handler);

  
  // ***************************************************************************************************************
  // Pin for battery gauge handling:
  // NOTE Removing this for now.  Think better off monitoring voltage.
  //_set_gpio_interrupt(PIN_GPOUT, NRF_GPIO_PIN_PULLUP, NRF_GPIOTE_POLARITY_TOGGLE, _bat_gauge_interrupt_handler);

  // Set STAT1 pin for reading.
  nrf_gpio_cfg_input(PIN_STAT_1,NRF_GPIO_PIN_PULLUP);
  
  // ***************************************************************************************************************
  // NOTE Set flag to disable gpio handlers for now..
  velo_status.gpios_enable = false;
}

void velo_power_up()
{
    // Power up
    _power_up();
}

// NOTE: Power initialisation - deals with power, LEDs, GPIOS etc.
void velo_power_init(velo_power_callback_t callback_function)
{
  // Set local reference to callback function in calling function (velo_driver.c).
  velo_power_callback = callback_function; 

  // Get starting condition, i.e. what caused system to wake up:
  _get_starting_condition();
  
  // if requested to turn off, skip the following steps:
  if (velo_status.new_state != VELO_PWR_STATE_OFF)
  {
    // Initialise LEDs:
    velo_led_pwm_init();
  
    // Set GPIOs
    _set_gpios();
  }

  // Callback to velo_driver to change state if necessary.
  velo_power_callback();
  


}
