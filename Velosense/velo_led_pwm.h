#ifndef VELO_LED_PWM_H__
#define VELO_LED_PWM_H__


void velo_led_pwm_set_pattern(void);
void velo_led_pwm_init(void);
void velo_led_pwm_uninit(void);
#endif //VELO_LED_PWM_H__