#include "velo_flash.h"
#include "nrf_delay.h"

#include "nrf_fstorage_sd.h"

#include "nrf_soc.h"
#include "nrf_sdh.h"

#include "app_scheduler.h"
#include "velo_config.h"

#define NRF_LOG_MODULE_NAME velo_flash
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
NRF_LOG_MODULE_REGISTER();

static void fstorage_evt_handler(nrf_fstorage_evt_t * p_evt);

NRF_FSTORAGE_DEF(nrf_fstorage_t fstorage) =
{
    .evt_handler = fstorage_evt_handler,
    .start_addr = VELO_FLASH_START_ADDR,
    .end_addr   = VELO_FLASH_END_ADDR,
};


static void fstorage_evt_handler(nrf_fstorage_evt_t * p_evt)
{
    if (p_evt->result != NRF_SUCCESS)
    {
        NRF_LOG_INFO("--> Event received: ERROR while executing an fstorage operation.");
        return;
    }

    switch (p_evt->id)
    {
        case NRF_FSTORAGE_EVT_WRITE_RESULT:
        {
            NRF_LOG_INFO("--> Event received: wrote %d bytes at address 0x%x.",
                         p_evt->len, p_evt->addr);
        } break;

        case NRF_FSTORAGE_EVT_ERASE_RESULT:
        {
            NRF_LOG_INFO("--> Event received: erased %d page from address 0x%x.",
                         p_evt->len, p_evt->addr);
        } break;

        default:
            break;
    }
}

void wait_for_flash_ready(nrf_fstorage_t const * p_fstorage)
{
    /* While fstorage is busy, sleep and wait for an event. */
    while (nrf_fstorage_is_busy(p_fstorage))
    {
    (void) sd_app_evt_wait();
    }
}

void velo_flash_init()
{
  nrf_fstorage_init(&fstorage, &nrf_fstorage_sd, NULL);
  wait_for_flash_ready(&fstorage);
  NRF_LOG_INFO("Flash memory initialised, address range: 0x%x to 0x%x.",fstorage.start_addr, fstorage.end_addr);
}

void velo_flash_read(uint32_t first_byte_number, uint8_t * p_data, uint16_t byte_count)
{
  uint32_t address = first_byte_number;

  ret_code_t err_code;

  err_code = nrf_fstorage_read(&fstorage,address,p_data,byte_count);
  NRF_LOG_INFO("Read in flash_read %u bytes starting address 0x%x",byte_count, address);

  NRF_LOG_FLUSH();
  if (err_code !=0)
  {
    int x=0;
  }
  APP_ERROR_CHECK(err_code);
}

//static object to hold write data:
//static velo_flash_write_data_t write_data;

void velo_flash_write_cb(void *  p_data, uint16_t unused)
{
  velo_flash_data_t * p_data_to_write = *(velo_flash_data_t **) p_data;
  // Write callback so we can use scheduler.  This avoids running from same context as soft device interrupt handler which I think causes issues.

  NRF_LOG_INFO("Writing %u bytes to address 0x%x in velo_flash_write_cb",p_data_to_write->byte_count, p_data_to_write->address);
  ret_code_t err_code = nrf_fstorage_write(&fstorage, p_data_to_write->address, p_data_to_write->p_data, p_data_to_write->byte_count, NULL);
  // Wait for write operation to complete.
  wait_for_flash_ready(&fstorage);
  APP_ERROR_CHECK(err_code);
  NRF_LOG_INFO("Data written");
}

void velo_flash_write(velo_flash_data_t * p_data, bool schedule)
{
  ret_code_t err_code;
  //Schedule transfer:
  // Note normally we are here in context of an ANT event, so need to run in main context.  For this
  // reason we have option to schedule.
  if (schedule)
  {
    err_code = app_sched_event_put(&p_data, sizeof(velo_flash_data_t *), velo_flash_write_cb);
    APP_ERROR_CHECK(err_code);
  }
  else
  {
    velo_flash_write_cb(&p_data, 0);
  }

}


void velo_flash_erase_cb(void *  p_data, uint16_t unused)
{
  // Callback to allow us to call from scheduler, so it runs in main context.
  // Don't require any input parameters.

  // We are putting all settings in the one page for now, so we just erase the lot.
  // NOTE: in flash this sets all bytes to 0xFF.  Subsequent writes can only change bits to '0'.
  // NOTE: Can only erase entire pages (4096 bytes, or 0x1000 bytes). 
  uint8_t * p_block_id = p_data;
  ret_code_t err_code = nrf_fstorage_erase(&fstorage, VELO_FLASH_START_ADDR,1, NULL);
  
  wait_for_flash_ready(&fstorage);
  APP_ERROR_CHECK(err_code);
  NRF_LOG_INFO("Flash config memory erased for block 0x%x", &p_data);
}

void velo_flash_erase(bool schedule)
{
  if (schedule)
  {
    app_sched_event_put(NULL,0,velo_flash_erase_cb);
  }
  else
  {
    velo_flash_erase_cb(NULL,0);
  }

}
